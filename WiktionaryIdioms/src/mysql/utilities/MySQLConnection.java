/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.utilities;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;


import org.apache.commons.lang3.StringUtils;


import classifier.model.DataPoint;
import classifier.model.Sense;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import detector.model.Example;

public class MySQLConnection {
	private Connection conn;
	private static Map<String, MySQLConnection> records;
	
	private MySQLConnection(String host, String database, String user, String password) {
		try {
			String connStr = "jdbc:mysql://" + host + "/" + database + "?useUnicode=true&characterEncoding=UTF-8";
			conn = DriverManager.getConnection(connStr, user, password);
		} catch (SQLException ex) {
		    // handle any errors
			System.out.println("Error in creating connection.");
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		    System.exit(0);
		}
	}
	
	public static MySQLConnection getInstance(String host, String database,
			String user, String password) {
		if (records == null) {
			records = new HashMap<String, MySQLConnection>();
		}
		String key = host + database + user + password;
		if (!records.containsKey(key)) {
			records.put(key, new MySQLConnection(host, database, user, password));
		}
		return records.get(key);
	}
	
	public String selectQuerySingle(String whatToSelect, String whereFrom, String endingClause) {
		List<String> select = new ArrayList<String>();
		select.add(whatToSelect);
		List<String[]> results = selectQuery(select, whereFrom, endingClause);
		if (results.size() > 0) {
			return results.get(0)[0];
		}
		return null;
	}
	
	public List<String[]> selectQueryWhereRestricted(List<String> whatToSelect, String table,
			String field, String value) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<String[]> results = new ArrayList<String[]>();
		try {
			String selectStr = StringUtils.join(whatToSelect, ",");
		    List<String> qs = new ArrayList<String>();
		    for (int i = 0; i < whatToSelect.size(); i++) {
		    	qs.add("?");
		    }
		    String qStr = StringUtils.join(qs, ",");
			stmt = conn.prepareStatement("SELECT " + selectStr + " FROM " + table + " WHERE " + field + " = ? ");
			stmt.setString(1, value);
			try {
				rs = stmt.executeQuery();
			    // process result set
				while(rs.next()) {
					String[] entry = new String[whatToSelect.size()];
					for (String select : whatToSelect) {
						try {
							entry[whatToSelect.indexOf(select)] = rs.getString(select);
			    		} catch (Exception e) {
			    			System.out.println(rs.getString(select));
			    		}
			    	}
					results.add(entry);
			    }		    	
			} catch (SQLException ex) {
				System.out.println(stmt);
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
		} catch (SQLException ex){
			// handle any errors
			System.out.println(stmt);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
		return results;
	}
	
	public List<String[]> selectQuery(String whatToSelect, String whereFrom,
			String endingClause) {
		List<String> select = new ArrayList<String>();
		select.add(whatToSelect);
		List<String[]> results = selectQuery(select, whereFrom, endingClause);
		return results;
	}
	
	public List<String[]> selectQuery(List<String> whatToSelect, String whereFrom,
			String endingClause) {
		Statement stmt = null;
		ResultSet rs = null;
		List<String[]> results = new ArrayList<String[]>();
		try {
		    stmt = conn.createStatement();
		    String selectStr = StringUtils.join(whatToSelect, ",");
		    boolean hadResults = stmt.execute("SELECT " + selectStr + " FROM " + 
		    		whereFrom + endingClause);
		    while (hadResults) {
		    	rs = stmt.getResultSet();
		    	// process result set
		    	while(rs.next()) {
		    		String[] entry = new String[whatToSelect.size()];
		    		for (String field : whatToSelect) {
		    			try {
		    				entry[whatToSelect.indexOf(field)] = rs.getString(field);
		    			} catch (Exception e) {
		    				System.out.println(rs.getString(field));
		    			}
		    		}
	    			results.add(entry);
		    	}		    	
		    	hadResults = stmt.getMoreResults();
		    }
		    // Do things with the ResultSet.
		     
		} catch (SQLException ex){
		    // handle any errors
			System.out.println(endingClause);
		    System.out.println("SQLException: " + ex.getMessage());
		    System.out.println("SQLState: " + ex.getSQLState());
		    System.out.println("VendorError: " + ex.getErrorCode());
		} finally {
		    // it is a good idea to release
		    // resources in a finally{} block
		    // in reverse-order of their creation
		    // if they are no-longer needed

		    if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException sqlEx) { } // ignore

		        rs = null;
		    }

		    if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException sqlEx) { } // ignore
		        stmt = null;
		    }
		}
		return results;
	}
	
	public Double selectFromDistanceTable(String table, String from, String to) {
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = conn.prepareStatement("SELECT distance FROM " + table + " WHERE from_word = ? AND to_word = ? ");
			stmt.setString(1, from);
			stmt.setString(2, to);
			try {
				rs = stmt.executeQuery();
				while (rs.next()) {
					return Double.parseDouble(rs.getString("distance"));
				}
			} catch (SQLException ex) {
				System.out.println(stmt);
				System.out.println("SQLException: " + ex.getMessage());
				System.out.println("SQLState: " + ex.getSQLState());
				System.out.println("VendorError: " + ex.getErrorCode());
			}
		} catch (SQLException ex){
			// handle any errors
			System.out.println(stmt);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
		return null;
	}
	
	public void updateFolds(String table, String idToSelect, int numFolds) {
		// We will need to insert them into two different tables:
		// 1) the idioms table
		// 2) the idiom definitions table
				
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement upStmt = null;
		try {
			// Prepare the statements.
			stmt = conn.prepareStatement("SELECT " + idToSelect + " FROM " + table);
		    upStmt = conn.prepareStatement("UPDATE " + table + " SET fold_num = ? WHERE " + idToSelect +
		    		" = ?");

			stmt.execute();
			rs = stmt.getResultSet();
			List<Long> rowIds = new ArrayList<Long>();
			while (rs.next()) {
				rowIds.add(rs.getLong(idToSelect));
			}

			List<Integer> folds = new ArrayList<Integer>();
			int foldSize = rowIds.size() / numFolds;
			int remainder = rowIds.size() % numFolds;
			// For each fold, add foldSize of that fold id to the list.
			// For the remainder, add one of each fold id, starting from zero
			// until there is no more remainder.
			for (int i = 0; i < numFolds; i++) {
				for (int j = 0; j < foldSize; j++) {
					folds.add(i);
				}
			}
			
			for (int i = 0; i < remainder; i++) {
				folds.add(i);
			}
			
			// Sanity check to make sure that the folds list is the same size as the rowIds.
			if (rowIds.size() != folds.size()) {
				System.out.println("Error in folds calculation");
				return;
			}
			System.out.println("Shuffling folds...");
			Collections.shuffle(folds);
			// Now, the entry in folds at index i is the fold that the rowId at index
			// i should be in!
			System.out.println("Updating...");
			for (int i = 0; i < rowIds.size(); i++) {
				if (i % 100000 == 0) {
					System.out.println(i);
				}
				upStmt.setInt(1, folds.get(i));
				upStmt.setLong(2, rowIds.get(i));
				upStmt.addBatch();
				if (i % 100000 == 0) {
					upStmt.executeBatch();
				}
			}
			upStmt.executeBatch();
			
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (SQLException sqlEx) { } // ignore
				rs = null;
			}
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
			if (upStmt != null) {
				try {
					upStmt.close();
				} catch (SQLException sqlEx) { } // ignore
			    upStmt = null;
			}
		}
	}
	
	public void updateColumn(String table, String columnName, String idName, Map<String, Double> values) {
		PreparedStatement stmt = null;
		try {
			stmt = conn.prepareStatement("UPDATE " + table + " SET " + columnName + " = ? WHERE " + idName + " = ? ");
			
			int i = 0;
			for (String key : values.keySet()) {
				// Set the first value.
				stmt.setString(1, Double.toString(values.get(key)));
				// Set the id.
				stmt.setString(2, key);
				stmt.addBatch();
				i++;
				if (i % 100000 == 0) {
					System.out.println(i);
					stmt.executeBatch();
				}
			}
			stmt.executeBatch();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void updateCaseColumn(String table, String columnName, String idName, Map<String, Double> values) {
		PreparedStatement stmt = null;
		try {
			String stmtStr = "UPDATE " + table + " SET " + columnName + " = CASE " + idName;
			for (String key : values.keySet()) {
				stmtStr += " WHEN '" + key + "' THEN '" + Double.toString(values.get(key)) + "'";
			}
			stmtStr += " ELSE " + columnName + " END";
			stmt = conn.prepareStatement(stmtStr);
			stmt.execute();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void updateCaseColumnString(String table, String columnName, String idName, Map<String, String> values) {
		PreparedStatement stmt = null;
		try {
			String stmtStr = "UPDATE " + table + " SET " + columnName + " = CASE " + idName;
			for (String key : values.keySet()) {
				stmtStr += " WHEN '" + key + "' THEN ? ";
			}
			stmtStr += " ELSE " + columnName + " END";
			stmt = conn.prepareStatement(stmtStr);
			// Set the values
			int i = 1;
			for (String key : values.keySet()) {
				stmt.setString(i, values.get(key));
				i++;
			}
			stmt.execute();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void updateCaseColumnInteger(String table, String columnName, String idName, Map<String, Integer> values) {
		PreparedStatement stmt = null;
		try {
			String stmtStr = "UPDATE " + table + " SET " + columnName + " = CASE " + idName;
			for (String key : values.keySet()) {
				stmtStr += " WHEN '" + key + "' THEN ? ";
			}
			stmtStr += " ELSE " + columnName + " END";
			stmt = conn.prepareStatement(stmtStr);
			// Set the values
			int i = 1;
			for (String key : values.keySet()) {
				stmt.setInt(i, values.get(key));
				i++;
			}
			stmt.execute();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void insertDataPointsInto(List<String> whatToSelect, List<DataPoint> data, String table) {
		PreparedStatement stmt = null;
		try {
		    String selectStr = StringUtils.join(whatToSelect, ",");
		    List<String> qs = new ArrayList<String>();
		    for (int i = 0; i < whatToSelect.size(); i++) {
		    	qs.add("?");
		    }
		    String qStr = StringUtils.join(qs, ",");
			stmt = conn.prepareStatement("INSERT INTO " + table + " (" + selectStr + ") values (" + qStr + ")");
			for (int i = 0; i < data.size(); i++) {
				stmt.setLong(1, data.get(i).getId());
				stmt.setString(2, data.get(i).getTitle());
				stmt.setInt(3, data.get(i).getLabel());
				for (int j = 4; j <= qs.size(); j++) {
					stmt.setDouble(j, data.get(i).getFeature(j - 4));
				}
				//stmt.addBatch();
				try {
					stmt.execute();
				} catch (SQLException ex) {
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				}
			}
			//stmt.executeBatch();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void insertSensesInto(List<String> whatToSelect, List<Sense> data, String table) {
		PreparedStatement stmt = null;
		try {
		    String selectStr = StringUtils.join(whatToSelect, ",");
		    List<String> qs = new ArrayList<String>();
		    for (int i = 0; i < whatToSelect.size(); i++) {
		    	qs.add("?");
		    }
		    String qStr = StringUtils.join(qs, ",");
			stmt = conn.prepareStatement("INSERT INTO " + table + " (" + selectStr + ") values (" + qStr + ")");
			for (int i = 0; i < data.size(); i++) {

				stmt.setString(1, data.get(i).getKey());
				stmt.setString(2, data.get(i).getTitle());
				stmt.setString(3, data.get(i).getGloss());
				stmt.setString(4, data.get(i).getUncleanedGloss());
				stmt.setInt(5, data.get(i).getLabel());
				
				for (int j = 6; j <= qs.size(); j++) {
					stmt.setDouble(j, data.get(i).getFeature(j - 6));
				}
				try {
					stmt.execute();
				} catch (SQLException ex) {
					System.out.println(stmt);
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				}
			}
		} catch (SQLException ex){
			// handle any errors
			System.out.println(stmt);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void insertDistanceInfo(String table, Map<String, Map<String, Double>> values) {
		PreparedStatement stmt = null;
		try {
		    String selectStr = "from_word,to_word,distance";
		    String qStr = "?,?,?";
			stmt = conn.prepareStatement("INSERT INTO " + table + " (" + selectStr + ") values (" + qStr + ")");
			for (String from : values.keySet()) {

				for (String to : values.get(from).keySet()) {
					stmt.setString(1, from);
					stmt.setString(2, to);
					stmt.setDouble(3, values.get(from).get(to));
				
					try {
						stmt.execute();
					} catch (SQLException ex) {
						System.out.println(stmt);
						System.out.println("SQLException: " + ex.getMessage());
						System.out.println("SQLState: " + ex.getSQLState());
						System.out.println("VendorError: " + ex.getErrorCode());
					}
				}
			}
		} catch (SQLException ex){
			// handle any errors
			System.out.println(stmt);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	/**
	 * data = [senseKey, title, label, language, gloss, uncleaned_gloss]
	 * @param whatToSelect
	 * @param data
	 * @param table
	 */
	public void insertDataCodesInto(List<String> whatToSelect, List<String[]> data, String table, boolean verbose) {
		PreparedStatement stmt = null;
		try {
		    String selectStr = StringUtils.join(whatToSelect, ",");
		    List<String> qs = new ArrayList<String>();
		    for (int i = 0; i < whatToSelect.size(); i++) {
		    	qs.add("?");
		    }
		    String qStr = StringUtils.join(qs, ",");
			stmt = conn.prepareStatement("INSERT INTO " + table + " (" + selectStr + ") values (" + qStr + ")");
			for (int i = 0; i < data.size(); i++) {
				stmt.setString(1, data.get(i)[0]);
				stmt.setString(2, data.get(i)[1]);
				stmt.setInt(3, Integer.parseInt(data.get(i)[2]));
				stmt.setString(4, data.get(i)[3]);
				stmt.setString(5, data.get(i)[4]);
				stmt.setString(6, data.get(i)[5]);
				try {
					stmt.execute();
				} catch (SQLException ex) {
					if (verbose) {
						System.out.println("SQLException: " + ex.getMessage());
						System.out.println("SQLState: " + ex.getSQLState());
						System.out.println("VendorError: " + ex.getErrorCode());
					}
				}
			}
			//stmt.executeBatch();
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
	
	public void insertIdioms(List<JsonObject> idioms, String idiomsTable,
							String idiomDefsTable) {
		// We will need to insert them into two different tables:
		// 1) the idioms table
		// 2) the idiom definitions table
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		PreparedStatement selStmt = null;
		PreparedStatement defStmt = null;
		PreparedStatement selDefStmt = null;
		try {
			// Prepare the statements.
		    stmt = conn.prepareStatement("INSERT INTO " + idiomsTable + " (title) values (?)");
	    	selStmt = conn.prepareStatement("SELECT * FROM " + idiomsTable + " WHERE title = ?");
		    defStmt = conn.prepareStatement(
		    		"INSERT INTO " + idiomDefsTable + 
		    				" (idiom_id, language, type, definition) values (?, ?, ?, ?)");
		    selDefStmt = conn.prepareStatement(
		    		"SELECT * FROM " + idiomDefsTable + " WHERE idiom_id = ? AND definition = ?");
		    
		    for (JsonObject idiom : idioms) {
		    	
		   		String title = idiom.get("title").getAsString();
		   		selStmt.setString(1, title);
			    selStmt.execute();
			    rs = selStmt.getResultSet();
			    
			    // If it isn't in the table, put it there.
			    if (!rs.next()) {
			    	stmt.setString(1, title);
			    	stmt.executeUpdate();
			    	selStmt.execute();
				    rs = selStmt.getResultSet();
				   	rs.next();
				}
			    
			    // Get the idiom_id from the result set.
			    int idiom_id = rs.getInt("idiom_id");
			    defStmt.setInt(1, idiom_id);
			    selDefStmt.setInt(1, idiom_id);
			    
			    // now go and put all of the definitions into the definitions table.
			    JsonArray langs = idiom.get("languages").getAsJsonArray();
			    
			    for (JsonElement entry : langs) {
			    	// Each language corresponds to a subsections array and definitions array.
			    	// We will only use the definitions array for now.
			    	JsonObject obj = entry.getAsJsonObject();
			    	defStmt.setString(2, obj.get("language").getAsString());
			    	JsonArray defs = obj.get("definitions").getAsJsonArray();
			    	
			    	for (JsonElement def : defs) {
			    		// Each definition has a type and then (possible) multiple "actual"
			    		// definitions.
			    		JsonObject defObj = def.getAsJsonObject();
			    		defStmt.setString(3, defObj.get("type").getAsString());
			    		JsonArray data = defObj.get("data").getAsJsonArray();
			    		
			    		// These are the actual definitions.
			    		for (JsonElement datum : data) {
			    			// Execute the statement for each individual actual definition
			    			defStmt.setString(4, datum.getAsString());
			    			
			    			// Check to make sure that the definition isn't already in the table first.
			    			selDefStmt.setString(2, datum.getAsString());
			    			selDefStmt.execute();
			    			rs = selDefStmt.getResultSet();
			    			if (!rs.next()) {
			    				defStmt.executeUpdate();
			    			}
			    		}
			    	}
			    }
		    }
		} catch (SQLException ex){
			// handle any errors
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
	    	
		} finally {
			if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException sqlEx) { } // ignore

		        rs = null;
		    }
			if (defStmt != null) {
		        try {
		            defStmt.close();
		        } catch (SQLException sqlEx) { } // ignore

		        defStmt = null;
		    }
			if (stmt != null) {
		        try {
		            stmt.close();
		        } catch (SQLException sqlEx) { } // ignore

		        stmt = null;
		    }
			if (selStmt != null) {
		        try {
		            selStmt.close();
		        } catch (SQLException sqlEx) { } // ignore

		        selStmt = null;
		    }
			if (selDefStmt != null) {
		        try {
		            selDefStmt.close();
		        } catch (SQLException sqlEx) { } // ignore

		        selDefStmt = null;
		    }
	   	}
	}
	
	public void close() {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException sqlEx) { } // ignore
		}
		
	}
	
	public void insertExamplesInto(List<String> whatToSelect, List<Example> data, String table) {
		PreparedStatement stmt = null;
		try {
		    String selectStr = StringUtils.join(whatToSelect, ",");
		    List<String> qs = new ArrayList<String>();
		    for (int i = 0; i < whatToSelect.size(); i++) {
		    	qs.add("?");
		    }
		    String qStr = StringUtils.join(qs, ",");
			stmt = conn.prepareStatement("INSERT INTO " + table + " (" + selectStr + ") values (" + qStr + ")");
			for (int i = 0; i < data.size(); i++) {

				stmt.setString(1, data.get(i).getKey());
				stmt.setString(2, data.get(i).getTitle());
				stmt.setString(3, data.get(i).getText());
				stmt.setInt(4, data.get(i).getLabel());
				
				for (int j = 5; j <= qs.size(); j++) {
					stmt.setDouble(j, data.get(i).getFeature(j - 5));
				}
				try {
					stmt.execute();
				} catch (SQLException ex) {
					System.out.println(stmt);
					System.out.println("SQLException: " + ex.getMessage());
					System.out.println("SQLState: " + ex.getSQLState());
					System.out.println("VendorError: " + ex.getErrorCode());
				}
			}
		} catch (SQLException ex){
			// handle any errors
			System.out.println(stmt);
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
			    	
		} finally {
			if (stmt != null) {
				try {
					stmt.close();
				} catch (SQLException sqlEx) { } // ignore
				stmt = null;
			}
		}
	}
}
