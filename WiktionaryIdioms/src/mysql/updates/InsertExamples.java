/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import mysql.utilities.MySQLConnection;

import de.tudarmstadt.ukp.wiktionary.api.IWikiString;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import detector.model.Example;

public class InsertExamples {
	public static int STEP_SIZE = 100000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 5) {
			throw new IllegalArgumentException("Usage: <data_database> <data_codes_table> <where_clause> " +
					" <data_table> <wiktionary_processed_dir>");
		}
		String dataDatabase = args[0];
		String dataCodesTable = args[1];
		String whereClause = args[2];
		String dataTable = args[3];
		File outputDirectory = new File(args[4]);
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data codes table: " + dataCodesTable);
		System.out.println("Where clause: " + whereClause);
		System.out.println("Data table: " + dataTable);

		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");

		// First, we're going to select the proper points from the data_codes
		// database.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("label");
		
		List<String> insertSelect = new ArrayList<String>();
		insertSelect.add("sense_key");
		insertSelect.add("title");
		insertSelect.add("text");
		insertSelect.add("label");
		
		List<Example> data = new ArrayList<Example>();
		
		// Now, we actually do it incrementally
		int current = 0;
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = whereClause + " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataCodesTable, limitClause);
			
			// For each result, construct a sense
			System.out.println("Gathering examples...");
			int i = 0;
			for (String[] r : results) {
				String title = r[select.indexOf("title")];
				String senseKey = r[select.indexOf("sense_key")];
				int label = Integer.parseInt(r[select.indexOf("label")]);
				
				// Get all the example sentences
				IWiktionarySense s = wkt.getSenseForKey(senseKey);
				List<IWikiString> examples = s.getExamples();
					
				if (examples != null && examples.size() > 0) {
					
					for (IWikiString ex : examples) {
						if (ex.getPlainText().length() > 0) {
							Example e = new Example(label, title, ex.getPlainText(), senseKey);
							data.add(e);
						}
					}
				}		

				i++;
				if (i % 1000 == 0) {
					System.out.println(i);
					// Then insert them into the sense_data data base
					System.out.print("Inserting sense data...");
					moby.insertExamplesInto(insertSelect, data, dataTable);
					System.out.println("done");
					data = new ArrayList<Example>();
				}
			}
			
			// Then insert them into the sense_data data base
			System.out.println("Inserting sense data...");
			moby.insertExamplesInto(insertSelect, data, dataTable);
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				System.out.println("Done with inserting: " + results.size() + " is less than " + STEP_SIZE);
				break;
			}
			current += results.size();
		}
	}
}
