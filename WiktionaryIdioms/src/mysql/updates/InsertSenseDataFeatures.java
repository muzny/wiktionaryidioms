/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import classifier.model.Sense;
import classifier.features.numeric.Feature;

import mysql.utilities.MySQLConnection;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;

public class InsertSenseDataFeatures {
	public static int STEP_SIZE = 100000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 5) {
			throw new IllegalArgumentException("Usage: <data_database> <data_codes_table> <where_clause> " +
					" <data_table> <wiktionary_processed_dir>");
		}
		String dataDatabase = args[0];
		String dataCodesTable = args[1];
		String whereClause = args[2];
		String dataTable = args[3];
		File outputDirectory = new File(args[4]);
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data codes table: " + dataCodesTable);
		System.out.println("Where clause: " + whereClause);
		System.out.println("Data table: " + dataTable);

		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		
		// First, we're going to select the proper points from the data_codes
		// database.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("gloss");
		select.add("uncleaned_gloss");
		select.add("label");
		
		List<String> insertSelect = new ArrayList<String>();
		insertSelect.add("sense_key");
		insertSelect.add("title");
		insertSelect.add("gloss");
		insertSelect.add("uncleaned_gloss");
		insertSelect.add("label");
		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			insertSelect.add(Feature.FEATURE_INDEXES.get(key).getDBName());
		}
		
		List<Sense> data = new ArrayList<Sense>();
		
		// Now, we actually do it incrementally
		int current = 0;
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = whereClause + " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataCodesTable, limitClause);
			
			// For each result, construct a sense
			System.out.println("Constructing senses...");
			int i = 0;
			for (String[] r : results) {
				Sense datum = new Sense(r[0], r[1], r[2], r[3], Integer.parseInt(r[4]));

				// TODO: Then calculate and add the features to the sense
				for (int key : Feature.FEATURE_INDEXES.keySet()) {
					double value = Feature.calculate(datum, key, wkt);
					datum.addFeature(key, value);
				}
				i++;
				if (i % 1000 == 0) {
					System.out.println(i);
					// Then insert them into the sense_data data base
					System.out.print("Inserting sense data...");
					moby.insertSensesInto(insertSelect, data, dataTable);
					System.out.println("done");
					data = new ArrayList<Sense>();
				}
				data.add(datum);
			}
			
			// Then insert them into the sense_data data base
			System.out.println("Inserting sense data...");
			moby.insertSensesInto(insertSelect, data, dataTable);
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				System.out.println("Done with inserting: " + results.size() + " is less than " + STEP_SIZE);
				break;
			}
			current += results.size();
		}
	}
}
