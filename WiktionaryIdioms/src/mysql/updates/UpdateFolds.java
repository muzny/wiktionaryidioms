/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import mysql.utilities.MySQLConnection;

public class UpdateFolds {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("Usage: <data_database> <folds_table> <id_name> <num_folds>");
		}
		System.out.println("Updating folds in table: " + args[1]);
		System.out.println("After this operation there will be " + args[3] + " folds.");
		MySQLConnection moby = MySQLConnection.getInstance("localhost", args[0], "root", "");
		moby.updateFolds(args[1], args[2], Integer.parseInt(args[3]));
		System.out.println("Done!");
	}

}
