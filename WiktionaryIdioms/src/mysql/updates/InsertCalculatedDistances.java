/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mysql.utilities.MySQLConnection;

import classifier.distances.Distance;
import classifier.utilities.StemUtils;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;

public class InsertCalculatedDistances {
	public static int STEP_SIZE = 100000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 6) {
			throw new IllegalArgumentException("Usage: <data_database> " +
					" <data_table> <wiktionary_processed_dir> <distance_table> <max_dist> <def_column>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		File outputDirectory = new File(args[2]);
		String distanceTable = args[3];
		int maxDist = Integer.parseInt(args[4]);
		String def = args[5];
		
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		System.out.println("Inserting into: " + distanceTable);
		System.out.println("Max dist: " + maxDist);
		System.out.println("Using distance calculator: " + Distance.TYPES.get(distanceTable));

		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		
		// First, we're going to select the proper points from the
		// database.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add(def);

		Map<String, Map<String, Double>> values = new HashMap<String, Map<String, Double>>();
		Map<String, Map<String, Double>> archive = new HashMap<String, Map<String, Double>>();
		
		// First, go and get all the mappings that the database already has
		List<String> toSelect = new ArrayList<String>();
		toSelect.add("from_word");
		toSelect.add("to_word");
		toSelect.add("distance");
		List<String[]> alreadyDone = moby.selectQuery(toSelect, distanceTable, "");
		
		// Add these to the archive
		int sum = 0;
		for (String[] result : alreadyDone) {
			if (!archive.containsKey(result[0])) {
				archive.put(result[0], new HashMap<String, Double>());
			}
			archive.get(result[0]).put(result[1], Double.parseDouble(result[2]));
			sum++;
		}
		System.out.println("The table already contains " + sum + " entries.");

		// Now, we actually do it incrementally
		int current = 0;
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataTable, limitClause);
			System.out.println("I have " + results.size() + " results");
			
			// For each result, construct a sense
			System.out.println("Calculating values...");
			int i = 0;
			for (String[] r : results) {
				i++;
				if (i % 1000 == 0) {
					System.out.println("On result: " + i);
					// Then insert them into the sense_data data base
					System.out.print("Inserting data...");
					moby.insertDistanceInfo(distanceTable, values);
					System.out.println("done.");
					// put these in the archive
					putInArchive(archive, values);
					values = new HashMap<String, Map<String, Double>>();
				}
				String senseKey = r[0];
				String title = r[1];
				String definition = r[2];
				
				// for each word in the title, calculate the distance between it and each word in the definition
				IWiktionarySense wikiSense = wkt.getSenseForKey(senseKey);
				String[] titleWords = StemUtils.cleanSentence(title).split(" ");
				String cleanGloss = StemUtils.cleanSentence(definition);
				String[] glossWords = cleanGloss.split(" ");
				
				for (String tW : titleWords) {
					if (tW.trim().length() == 0) {
						continue;
					}
					
					for (String gW : glossWords) {
						if (gW.trim().length() == 0) {
							continue;
						}
						if (!values.containsKey(tW)) {
							values.put(tW, new HashMap<String, Double>());
						}
						if (!values.get(tW).containsKey(gW) && 
								(!archive.containsKey(tW) || !archive.get(tW).containsKey(gW))) {
							int dist = Distance.TYPES.get(distanceTable).getDistance(tW, gW, maxDist);
							if (dist == -1) {
								values.get(tW).put(gW, 0.0);
							} else {
								dist = dist + 1;
								values.get(tW).put(gW, ((dist * 1.0) / (maxDist + 1)));
							}
						}
					}
				}
			}
			// Then insert them into the sense_data data base
			System.out.print("Inserting data...");
			moby.insertDistanceInfo(distanceTable, values);
			System.out.println("done.");
			// put these in the archive
			putInArchive(archive, values);
			values = new HashMap<String, Map<String, Double>>();
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				System.out.println("Done with inserting: " + results.size() + " is less than " + STEP_SIZE);
				break;
			}
			current += results.size();
		}
	}
	
	public static void putInArchive(Map<String, Map<String, Double>> archive, 
			Map<String, Map<String, Double>> values) {
		for (String from : values.keySet()) {
			for (String to : values.get(from).keySet()) {
				if (! archive.containsKey(from)) {
					archive.put(from, new HashMap<String, Double>());
				}
				archive.get(from).put(to, values.get(from).get(to));
			}
		}
	}

}
