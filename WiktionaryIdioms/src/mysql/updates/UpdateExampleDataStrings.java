/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mysql.utilities.MySQLConnection;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import classifier.features.strings.StringFeature;

public class UpdateExampleDataStrings {
	public static int STEP_SIZE = 100000;


	public static void main(String[] args) {
		if (args.length != 5) {
			throw new IllegalArgumentException("Usage: <data_database> " +
					" <data_table> <wiktionary_processed_dir> <featuresToUpdate(comma separated)> <where_clause>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		File outputDirectory = new File(args[2]);
		String whereClause = args[4];
		
		// Now analyze which features we should be using
		List<Integer> featuresToUpdate = new ArrayList<Integer>();
		String[] nums = args[3].split(",");
		for (String num : nums) {
			featuresToUpdate.add(Integer.parseInt(num));
		}
		
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		System.out.println("Updating features: " + featuresToUpdate + ", corresponding to: ");
		for (int feat : featuresToUpdate) {
			System.out.println(feat + ":" + StringFeature.FEATURE_INDEXES.get(feat));
		}

		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		
		// First, we're going to select the proper points from the
		// database.
		List<String> select = new ArrayList<String>();
		select.add("id");
		select.add("sense_key");
		select.add("title");
		select.add("label");
		select.add("text");
		
		Map<String, String> values = new HashMap<String, String>();		
		// Now, we actually do it incrementally
		int current = 0;
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = whereClause + " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataTable, limitClause);
			System.out.println("I have " + results.size() + " results");
			
			// For each result, construct a sense
			System.out.println("Constructing senses...");
			int i = 0;
			for (int key : featuresToUpdate) {
				for (String[] r : results) {
					String senseKey = r[0]; // id
					values.put(senseKey, StringFeature.FEATURE_INDEXES.get(key).calculate(senseKey, r[4], wkt));
					i++;
					if (i % 10 == 0) {
						System.out.println(i);
						// Then insert them into the sense_data data base
						System.out.print("Updating sense data for feature " + key + "...");
						moby.updateCaseColumnString(dataTable, StringFeature.FEATURE_INDEXES.get(key).getDBName(),
									"id", values);
						System.out.println("done.");
					
						values = new HashMap<String, String>();
					}
				}
				// Then insert them into the sense_data data base
				System.out.println("Updating sense data for feature " +  key + "...");
				moby.updateCaseColumnString(dataTable, StringFeature.FEATURE_INDEXES.get(key).getDBName(),
							"id", values);
			
				values = new HashMap<String, String>();
			}
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				System.out.println("Done with inserting: " + results.size() + " is less than " + STEP_SIZE);
				break;
			}
			current += results.size();
		}
	}
	
}
