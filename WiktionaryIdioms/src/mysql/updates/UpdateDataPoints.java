/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import classifier.model.DataPoint;
import classifier.utilities.DataUtils;

import mysql.utilities.MySQLConnection;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import de.tudarmstadt.ukp.wiktionary.api.entry.WiktionaryIterator;

public class UpdateDataPoints {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 5) {
			throw new IllegalArgumentException("Usage: <idioms_database> <idioms_table> <data_database>" +
					" <data_table> <wiktionary_processed_dir>");
		}
		String dataDatabase = args[2];
		String dataTable = args[3];
		File outputDirectory = new File(args[4]);
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", args[0], "root", "");
		List<String> select = new ArrayList<String>();
		select.add("title");
		List<String[]> results = moby.selectQuery(select, args[1], "");
		
		// Put the titles into a set.
		Set<String> titles = new HashSet<String>();
		for (String[] strs : results) {
			titles.add(strs[select.indexOf("title")]);
		}
		System.out.println("We have " + titles.size() + " titles!");
		
		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		WiktionaryIterator<IWiktionaryPage> wit = wkt.getAllPages();
		
		// Reset select list for inserts.
		select = new ArrayList<String>();
		select.add("page_id");
		select.add("title");
		select.add("label");
		/*for (int key : Feature.FEATURE_INDEXES.keySet()) {
			select.add(Feature.FEATURE_INDEXES.get(key).getDBName());
		}*/
		
		System.out.println("Making data points...");
		List<DataPoint> data = new ArrayList<DataPoint>();
		int i = 0;
		while (wit.hasNext()) {
			i++;
			if (i % 100000 == 0) {
				System.out.println(i);
				System.out.println("Inserting data");
				DataUtils.insertData(dataDatabase, dataTable, select, data);
				data = new ArrayList<DataPoint>();
				System.out.println("on to brighter pastures");
			}
			IWiktionaryPage page = wit.next();
			int label = 0;
			if (titles.contains(page.getTitle())) {
				label = 1;
			}
			DataPoint dp = new DataPoint(page.getId(), page.getTitle(), label);
			/*for (int key : Feature.FEATURE_INDEXES.keySet()) {
				dp.addFeature(key, Feature.FEATURE_INDEXES.get(key).calculate(dp.getId(), wkt));
			}*/
			data.add(dp);
		}
		
		System.out.println("Inserting data: " + data.size());
		DataUtils.insertData(dataDatabase, dataTable, select, data);
	}

}
