/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import mysql.utilities.MySQLConnection;

import de.tudarmstadt.ukp.wiktionary.api.IWikiString;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import detector.model.Example;

public class UpdateExampleDataSets {
	public static int STEP_SIZE = 100000;


	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("Usage: <data_database> " +
					" <data_table> <wiktionary_processed_dir> <where_clause>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		File outputDirectory = new File(args[2]);
		String whereClause = args[3];
		
		
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		
		// First, we're going to select the proper points from the
		// database.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("label");
		
		// Now, we actually do it incrementally
		List<Example> exs = new ArrayList<Example>();
		List<String> idTitles = new ArrayList<String>();
		List<String> litTitles = new ArrayList<String>();
		int current = 0;
		
		Map<String, Map<String, Integer>> counts = new HashMap<String, Map<String, Integer>>();
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = whereClause + " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataTable, limitClause);
			System.out.println("I have " + results.size() + " results");
			
			// For each result, add it to the list of examples if it has examples
			System.out.println("Constructing examples...");
			for (String[] r : results) {
				String senseKey = r[0];
				String phrase = r[1];
				int label = Integer.parseInt(r[2]);
				IWiktionarySense s = wkt.getSenseForKey(senseKey);
				List<IWikiString> examples = s.getExamples();
					
				if (examples != null && examples.size() > 0) {
					if (!counts.containsKey(phrase)) {
						counts.put(phrase, new HashMap<String, Integer>());
					}
					counts.get(phrase).put(senseKey, examples.size());
					for (IWikiString ex : examples) {
						Example e = new Example(label, phrase, ex.getPlainText(), senseKey);
						exs.add(e);
					}
						
					if (label == 0) {
						litTitles.add(phrase);
					} else {
						idTitles.add(phrase);
					}
				}		
			}
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				break;
			}
			current += results.size();
		}
		
		// Now, figure out which examples should be in train/dev/test
		
		List<Example> train = new ArrayList<Example>();
		List<Example> dev = new ArrayList<Example>();
		List<Example> test = new ArrayList<Example>();
		
		Set<String> ambiguous = new HashSet<String>();
		Map<String, List<Example>> tempAmbiguous = new HashMap<String, List<Example>>();

		for (String t : idTitles) {
			if (litTitles.contains(t)) {
				ambiguous.add(t);
			}
		}
		
		for (Example e : exs) {
			if (ambiguous.contains(e.getTitle())) {
				if (!tempAmbiguous.containsKey(e.key)) {
					tempAmbiguous.put(e.key, new ArrayList<Example>());
				}
				tempAmbiguous.get(e.key).add(e);
			} else {
				train.add(e);
			}
		}
		
		// put half of the dev into the test
		// we need to calculate from counts to determine half-ness
		int sum = 0;
		for (String phrase : counts.keySet()) {
			if (ambiguous.contains(phrase)) {
				for (String key : counts.get(phrase).keySet()) {
					sum += counts.get(phrase).get(key);
				}
			}
		}
		System.out.println("Total number of sentences paired with ambiguous phrases: " + sum);
		
		int target = sum / 2;
		System.out.println("Number of sentences paired with ambiguous phrases that should be in dev: " + target);

		
		int numDevSenses = 0;
		int numTestSenses = 0;
		for (String key : tempAmbiguous.keySet()) {
			List<Example> sentences = tempAmbiguous.get(key);
			if (target - sentences.size() >= 0) {
				dev.addAll(sentences);
				target = target - sentences.size();
				numDevSenses++;
			} else {
				test.addAll(sentences);
				numTestSenses++;
			}
		}
		
		System.out.println("Size of train: " + train.size());
		System.out.println("Size of dev: " + dev.size() + ", corresponding to " + numDevSenses + " senses.");
		System.out.println("Size of test: " + test.size() + ", corresponding to " + numTestSenses + " senses.");
		
		
		update(moby, dataTable, "train", train);
		update(moby, dataTable, "dev", dev);
		update(moby, dataTable, "test", test);
	}
	
	private static void update(MySQLConnection moby, String dataTable, String name, List<Example> exs) {
		System.out.print("Updating example data set marking: " + name + "...");
		
		Map<String, String> values = new HashMap<String, String>();
		for (Example e : exs) {
			values.put(e.key, name);
		}
		
		moby.updateCaseColumnString(dataTable, "example_data_set",
				"sense_key", values);
		System.out.println("done.");
	}
}
