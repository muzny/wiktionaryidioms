/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This runnable class updates the feature values for the specified features (one or more).
 * 
 * To run it, it takes 5 command line arguments: <database> <table> 
 * <path_to_wiktionary_processed_directory> <features_to_update> <where_clause>
 * where path_to_wiktioanry_processed_directory is the path to the directory that hold the 
 * wiktionary output that is the result of parsing wiktionary with JWKTL 
 * (http://code.google.com/p/jwktl/) and <features_to_update> is a comma separated list.
 */


package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mysql.utilities.MySQLConnection;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import detector.model.Example;

import classifier.features.numeric.Feature;

public class UpdateExampleDataFeatures {
	public static int STEP_SIZE = 100000;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length != 5) {
			throw new IllegalArgumentException("Usage: <data_database> " +
					" <data_table> <wiktionary_processed_dir> <featuresToUpdate(comma separated)> <where_clause>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		File outputDirectory = new File(args[2]);
		String whereClause = args[4];
		
		// Now analyze which features we should be using
		List<Integer> featuresToUpdate = new ArrayList<Integer>();
		String[] nums = args[3].split(",");
		for (String num : nums) {
			featuresToUpdate.add(Integer.parseInt(num));
		}
		
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		System.out.println("Updating features: " + featuresToUpdate + ", corresponding to: ");
		for (int feat : featuresToUpdate) {
			System.out.println(feat + ":" + Feature.FEATURE_INDEXES.get(feat));
		}

		
		// Set up JWKTL tools
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		
		// Set up the database connection.
		MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		
		// First, we're going to select the proper points from the
		// database.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("text");
		select.add("label");
		select.add("parse");
		
		Map<String, Double> values = new HashMap<String, Double>();		
		// Now, we actually do it incrementally
		int current = 0;
		while (true) {
			System.out.println("Working from: " + current);
			String limitClause = whereClause + " LIMIT " + current + ", " + STEP_SIZE;
			List<String[]> results = moby.selectQuery(select, dataTable, limitClause);
			System.out.println("I have " + results.size() + " results");
			
			// For each result, construct a sense
			System.out.println("Constructing senses...");
			int i = 0;
			for (int key : featuresToUpdate) {
				for (String[] r : results) {
					String senseKey = r[0];
					String title = r[1];
					String text = r[2];
					int label = Integer.parseInt(r[3]);
					String parse = r[4];
					Example e = new Example(label, title, text, senseKey);
					e.setParse(parse); // set the parse
					
					values.put(senseKey, Feature.calculate(e, key, wkt));

					i++;
					if (i % 100 == 0) {
						System.out.println(i);
						// Then insert them into the sense_data data base
						System.out.print("Updating sense data for feature " + key + "...");
						moby.updateCaseColumn(dataTable, Feature.FEATURE_INDEXES.get(key).getDBName(),
									"sense_key", values);
						System.out.println("done.");
					
						values = new HashMap<String, Double>();
					}
				}
				// Then insert them into the sense_data data base
				System.out.println("Updating sense data for feature " +  key + "...");
				moby.updateCaseColumn(dataTable, Feature.FEATURE_INDEXES.get(key).getDBName(),
							"sense_key", values);
			
				values = new HashMap<String, Double>();
			}
			
			// If there are fewer results than STEP_SIZE, we're done!
			if (results.size() < STEP_SIZE) {
				System.out.println("Done with inserting: " + results.size() + " is less than " + STEP_SIZE);
				break;
			}
			current += results.size();
		}
	}
}
