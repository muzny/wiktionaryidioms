/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package mysql.updates;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import classifier.utilities.DataUtils;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import de.tudarmstadt.ukp.wiktionary.api.entry.WiktionaryIterator;
import de.tudarmstadt.ukp.wiktionary.api.util.ILanguage;

public class InsertDataCodes {
	public static final String IDIOMATIC = "idiomatic";
	public static final String FIGURATIVELY = "figuratively";
	public static final String SIMILE = "simile";
	
	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("Usage: <data_database>" +
					" <data_table> <wiktionary_processed_dir> <loose_constraints>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		File outputDirectory = new File(args[2]);
		boolean loose = args[3].equalsIgnoreCase("true");
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		System.out.println("Using loose constraints: " + loose);		
		
		// Set up JWKTL tools		
		// Create new IWiktionaryEdition for our parsed data.
		IWiktionaryEdition wkt = Wiktionary.open(outputDirectory);
		WiktionaryIterator<IWiktionaryPage> wit = wkt.getAllPages();
		
		// Reset select list for inserts.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("label");
		select.add("language");
		select.add("gloss");
		select.add("uncleaned_gloss");

		int i = 0;
		List<String[]> data = new ArrayList<String[]>();
		while (wit.hasNext()) {
			i++;
			if (i % 100000 == 0) {
				System.out.println(i);
				System.out.println("Inserting data");
				DataUtils.insertDataCodes(dataDatabase, dataTable, select, data, false);
				data = new ArrayList<String[]>();
				System.out.println("on to brighter pastures");
			}
			IWiktionaryPage page = wit.next();
			String title = page.getTitle();
			
			for (IWiktionaryEntry entry : page.getEntries()) {
				// This will be null for things that are categories
				ILanguage language = entry.getWordLanguage();
				
				for (IWiktionarySense sense : entry.getSenses()) {					
					
					if (sense.getGloss() != null && language != null) {
						String gloss = sense.getGloss().getPlainText();
						String uncleaned = sense.getGloss().getTextIncludingWikiMarkup();
						
						if (!uncleaned.startsWith("#")) {
							// Now, I just have to discover the labels
							// figure out the label
							String label = "";
							if (loose) {
								label = getLabelLooseConstraints(uncleaned);
							} else {
								label = getLabel(uncleaned);
							}
							String senseKey = sense.getKey();
							String[] datum = {senseKey, title, label,
											  language.getName(), gloss, uncleaned};
							data.add(datum);
						}
					}
				}
			}
		}
		
		System.out.println("Inserting data: " + data.size());
		DataUtils.insertDataCodes(dataDatabase, dataTable, select, data, false);
	}
	
	private static String getLabelLooseConstraints(String uncleaned) {
		String[] parts = uncleaned.split("}}");
		if (parts[0].contains(IDIOMATIC) || parts[0].contains(FIGURATIVELY) ||
				parts[0].contains(SIMILE)) {
			return "1";
		}
		return "0";
	}
	
	private static String getLabel(String uncleaned) {
		String[] parts = uncleaned.split("}}");
		if (parts[0].contains(IDIOMATIC)) {
			return "1";
		}
		return "0";
	}
}
