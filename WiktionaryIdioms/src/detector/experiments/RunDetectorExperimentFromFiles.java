/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The main class to run detector experiments from that draws only from
 * text files.
 * 
 * Works with the DummyExperiment - "dummy" for type of detectore_experiment in
 * command line args.
 */

package detector.experiments;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import classifier.classifiers.Classifier;
import classifier.classifiers.Perceptron;
import classifier.config.ClassifierConfigs;
import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.utilities.ClassifierDataUtils;

import detector.methods.DetectorMethod;
import detector.methods.ExtendedLesk;
import detector.utilities.ExampleUtilities;

public class RunDetectorExperimentFromFiles {
	
	public static void main(String[] args){
		if (args.length < 4) {
			System.err.println("Usage: RunDetectorExperimentFromFiles <detector_experiment> " +
					"<detector_method> <config_path> <classifier_model_path> [<times>]");
			System.exit(1);
		}
	
		DetectorExperiment experiment = DetectorExperiment.EXPERIMENTS.get(args[0]); 
		System.out.println("Using experiment: "+ experiment);
		
		DetectorMethod d = DetectorMethod.METHODS.get(args[1]);
		System.out.println("Using method: " + d);
		
		String configPath = args[2];
		System.out.println("Using config file: " + configPath);
		
		String modelPath = args[3];
		System.out.println("Using model: " + modelPath);
		
		int times = 1;
		if (args.length == 5) {
			times = Integer.parseInt(args[4]);
		}
		
		ClassifierConfigs configs = ClassifierConfigs.getInstance(configPath);
				
		String testPath = configs.getSString(ClassifierConfigs.TEST_PATH);
		String sensesPath = configs.getSString("sensesPath");
		String allSensesPath = configs.getSString("allSensesPath");

		System.out.print("Loading test data from: " + testPath + " ...");
		List<ClassifierData> testData = ExampleUtilities.getExamplesFromFile(new File(testPath));
		System.out.println("(" + testData.size() + ") done.");
		
		System.out.print("Loading sense data from: " + sensesPath + " ...");
		List<ClassifierData> senses = ClassifierDataUtils.getSensesFromFile(new File(sensesPath));
		System.out.println("(" + senses.size() + ") done.");

		System.out.print("Loading all sense data from: " + allSensesPath + " ...");
		List<ClassifierData> allSenses = ClassifierDataUtils.getSensesFromFile(new File(allSensesPath));
		System.out.println("(" + allSenses.size() + ") done.");

		// Gather some statistics
		int ids = 0;
		int lits = 0;
		for (ClassifierData cd : testData) {
			if (cd.getLabel() == 0) {
				lits++;
			}
			if (cd.getLabel() == 1) {
				ids++;
			}
		}
	
		System.out.println("Number of examples in test: " + testData.size());
		System.out.println("Number of literals in test: " + lits);
		System.out.println("Number of idioms in test: " + ids);
		
		// Process our all senses
		System.out.print("Processing senses...");
		Map<String, List<ClassifierData>> titleSenseMap = new HashMap<String, List<ClassifierData>>();
		for (ClassifierData cd : allSenses) {
			String title = cd.getTitle();
			if (!titleSenseMap.containsKey(title)) {
				titleSenseMap.put(title, new ArrayList<ClassifierData>());
			}
			titleSenseMap.get(title).add(cd);
		}
		System.out.println("done.");
		
		// then we need to set them as the map for the detector method to use.
		d.setSensesByTitleMap(titleSenseMap);

		for (int i = 0; i < times; i++) {
			DetectorExperimentResult result = experiment.runExperiment(d, testData);
		
			System.out.println("# of sentences tested: " + testData.size());
			System.out.println("Results:");
			System.out.println(result);

			if (d instanceof ExtendedLesk) {
				System.out.println("Phrase Synset Counts: " + ExtendedLesk.phraseSynsetCount);
			}
		
			//System.out.println("Test results for default labels classifier model: ");
			//testDefaultClassifierModel(senses, testData, result);
		
			System.out.println("Test results for corrected classifier model: ");
			testClassifierModel(modelPath, senses, testData, result, false);
			
			//System.out.println("Test results for augmented corrected classifier model: ");
			//testClassifierModel(modelPath, senses, testData, result, true);
		}

	}
	
	public static void testClassifierModel(String modelPath, List<ClassifierData> senses,
			List<ClassifierData> testData, DetectorExperimentResult result, boolean augment) {
		// Now, go and use the model to classify them (as idiomatic or not)!
		// First, go and classify these using the identification model.
		Classifier classy = new Perceptron(new ClassifierModel(
				new File(modelPath)));

		System.out.println("Gathered senses: " + senses.size());
		System.out.println("Classifying...");
		TreeMap<Integer, List<ClassifierData>> idiomaticity = new TreeMap<Integer, List<ClassifierData>>();
		for (String key : result.results.keySet()) {
			
			// These will be examples
			for (ClassifierData cd : result.results.get(key)) {
				//classify the definition of the sense associated with the key
				for (ClassifierData sense : senses) {
					if (sense.getKey().equals(key)) {
						// classify this one!
						int label = classy.predict(sense);
						int defLabel = sense.getLabel();
						if (augment) {
							if (defLabel == 1) {
								label = 1;
							}
						}
						if (!idiomaticity.containsKey(label)) {
							idiomaticity.put(label, new ArrayList<ClassifierData>());
						}
						idiomaticity.get(label).add(cd);
					}
				}
			}
		}
		
		// Print out classification results
		System.out.println("Classification results:");
		
		int correctIds = 0;
		int incorrectIds = 0;
		int correctLits = 0;
		int incorrectLits = 0;
		
		for (int label : idiomaticity.keySet()) {
			
			for (ClassifierData cd : idiomaticity.get(label)) {
				if (label == 0) {
					if (cd.getLabel() == 0) {
						correctLits++;
					} else {
						incorrectIds++;
					}
				} else if (label == 1) {
					if (cd.getLabel() == 1) {
						correctIds++;
					} else {
						incorrectLits++;
					}
				}
			}
		}
		double precision = (correctIds * 1.0) / (correctIds + incorrectLits);
		double recall = (correctIds * 1.0) / (correctIds + incorrectIds + result.unclassified.size());
		double fMeasure = (2.0 * precision * recall) / (precision + recall);
		double accuracy = ((correctIds + correctLits) * 1.0) / (correctIds + incorrectIds + correctLits
				+ incorrectLits + result.unclassified.size());

				
		System.out.println("[p, r]: " + precision + ", " + recall);
		System.out.println("F-measure: " + fMeasure);
		System.out.println("Accuracy: " + accuracy);
	}
}
