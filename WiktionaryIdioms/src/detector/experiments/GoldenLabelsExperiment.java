/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * An experiment that displays statistics about the examples according to 
 * the labelings of idioms already in Wiktionary - the "golden labels".
 */

package detector.experiments;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


import classifier.config.GeneralConfigs;
import classifier.features.dependency.Pair;
import classifier.model.ClassifierData;
import classifier.model.Sense;
import classifier.utilities.ClassifierDataUtils;

import detector.methods.DetectorMethod;
import detector.model.Example;

public class GoldenLabelsExperiment implements DetectorExperiment {

	@Override
	public DetectorExperimentResult runExperiment(DetectorMethod d, List<ClassifierData> test) {

		System.out.println("There are "+ test.size() + " test examples.");
		DetectorExperimentResult result = new DetectorExperimentResult();
		
		// Go and get the right senses
		String database = GeneralConfigs.getString(
				GeneralConfigs.DETECTOR_CONFIGS, "GoldenLabels.senseDb");
		String table = GeneralConfigs.getString(
				GeneralConfigs.DETECTOR_CONFIGS, "GoldenLabels.senseTable");
		List<ClassifierData> senses = ClassifierDataUtils.getSensesFromDbBasedOnExamples(database, table, test);
		System.out.println("These examples map to " + senses.size() + " senses.");
		
		List<Pair<Sense, Example>> pairs = new ArrayList<Pair<Sense, Example>>();
		for (ClassifierData sense : senses) {
			// Find all examples that go with it
			for (ClassifierData example : test) {
				if (example.getKey().equals(sense.getKey())) {
					pairs.add(new Pair<Sense, Example>((Sense) sense, (Example) example));
				}
			}
		}
		System.out.println("Which makes a total of " + pairs.size() + " pairs.");
		
		// For each classifier data, see if the corresponding sense "should" be literal or idiomatic
		Set<String> predictedIdiomaticTitles = new HashSet<String>();
		for (Pair<Sense, Example> pair : pairs) {
			
			if (pair.getFirst().getLabel() == 1) {
				predictedIdiomaticTitles.add(pair.getFirst().getTitle());
			}
		}
		System.out.println("There are " + predictedIdiomaticTitles.size() + " titles labeled idiomatic in Wiktionary.");
		
		// Now gather all examples with those titles
		List<Example> predictedIdiomatic = new ArrayList<Example>();
		for (Pair<Sense, Example> pair : pairs) {
			if (predictedIdiomaticTitles.contains(pair.getSecond().getTitle())) {
				predictedIdiomatic.add(pair.getSecond());
			}
		}
		System.out.println("There are " + predictedIdiomatic.size() + " examples corresponding to these titles.");
		
		int count = 0;
		for (Example ex : predictedIdiomatic) {
										
			String best = d.predict(ex);

			if (best != null) {
				result.addResult(best, ex);
			} else {
				result.addUnclassified(ex);
			}
			count++;
			if (count % 100 == 0) {
				System.out.println(count + "/" + test.size());
			}
		}
		return result;
	}

}
