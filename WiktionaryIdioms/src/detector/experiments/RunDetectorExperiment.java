/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The main class to run a detector experiment from that
 * draws from MySQL databases.
 */

package detector.experiments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


import classifier.classifiers.Classifier;
import classifier.classifiers.Perceptron;
import classifier.config.GeneralConfigs;
import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.utilities.ClassifierDataUtils;


import detector.methods.DetectorMethod;
import detector.methods.ExtendedLesk;
import detector.utilities.ExampleUtilities;

public class RunDetectorExperiment {
	
	public static void main(String[] args){
		if (args.length < 2) {
			System.err.println("Usage: RunDetectorExperiment <detector_experiment> <detector_method> [<times>]");
			System.exit(1);
		}
	
		DetectorExperiment experiment = DetectorExperiment.EXPERIMENTS.get(args[0]); 
		System.out.println("Using experiment: "+ experiment);
		
		DetectorMethod d = DetectorMethod.METHODS.get(args[1]);
		System.out.println("Using method: " + d);
		
		int times = Integer.parseInt(args[2]);
				
		String database = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.database");
		String table = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.table");
		String classifierTable = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.classifierTable");
		String column = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.column");
		String testData = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.testData");
		String labelKind = GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.label");


		List<ClassifierData> test = null;
		if (testData.length() > 0) {
			test = ExampleUtilities.getExamplesFromDb(
					database, table, " WHERE " + column + " = \"" + testData + "\" AND corrected_label >= 0 ", labelKind);	
		} else {
			test = ExampleUtilities.getExamplesFromDb(
					database, table, "", labelKind);	
		}
		
		int ids = 0;
		int lits = 0;
		for (ClassifierData cd : test) {
			if (cd.getLabel() == 0) {
				lits++;
			}
			if (cd.getLabel() == 1) {
				ids++;
			}
		}
	
		System.out.println("Number of examples in test: " + test.size());
		System.out.println("Number of literals in test: " + lits);
		System.out.println("Number of idioms in test: " + ids);

		for (int i = 0; i < times; i++) {
			DetectorExperimentResult result = experiment.runExperiment(d, test);
		
			System.out.println("# of sentences tested: " + test.size());
			System.out.println("Results:");
			System.out.println(result);

			if (d instanceof ExtendedLesk) {
				System.out.println("Phrase Synset Counts: " + ExtendedLesk.phraseSynsetCount);
			}
		
			System.out.println("Test results for default labels classifier model: ");
			testDefaultClassifierModel(database, classifierTable, testData, result);
		
			System.out.println("Test results for corrected classifier model: ");
			testClassifierModel(GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.classifierModelCorrected"),
					database, classifierTable, testData, result, false);
			
			System.out.println("Test results for augmented corrected classifier model: ");
			testClassifierModel(GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.classifierModelCorrected"),
					database, classifierTable, testData, result, true);
		
			System.out.println("Test results for cleaned classifier model: ");
			testClassifierModel(GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.classifierModelCleaned"),
					database, classifierTable, testData, result, false);
			
			System.out.println("Test results for augmented cleaned classifier model: ");
			testClassifierModel(GeneralConfigs.getString(GeneralConfigs.DETECTOR_CONFIGS, "default.classifierModelCleaned"),
					database, classifierTable, testData, result, true);
		}

	}
	
	public static void testDefaultClassifierModel(String database, String classifierTable,
			String testData, DetectorExperimentResult result) {
		// Now, go and use the model to classify them (as idiomatic or not)!
		// First, go and classify these using the identification model.
				
		List<ClassifierData> senses = ClassifierDataUtils.getSensesFromDb(database, classifierTable,
				" WHERE data_set = \"" + testData + "\" ", "label");
		
		System.out.println(ClassifierDataUtils.getGeneralDataAnalysis(senses));

		System.out.println("Gathered senses: " + senses.size());
		System.out.println("Classifying...");
		TreeMap<Integer, List<ClassifierData>> idiomaticity = new TreeMap<Integer, List<ClassifierData>>();
		for (String key : result.results.keySet()) {
					
			// These will be examples
			for (ClassifierData cd : result.results.get(key)) {
				//classify the definition of the sense associated with the key
				for (ClassifierData sense : senses) {
					if (sense.getKey().equals(key)) {
						// get the label that is default associated in Wiktionary
						int label = sense.getLabel();
						
						if (!idiomaticity.containsKey(label)) {
							idiomaticity.put(label, new ArrayList<ClassifierData>());
						}
						idiomaticity.get(label).add(cd);
					}
				}
			}
		}
		
		// Print out classification results
		System.out.println("Classification results:");
		
		int correctIds = 0;
		int incorrectIds = 0;
		int correctLits = 0;
		int incorrectLits = 0;
		
		for (int label : idiomaticity.keySet()) {
					
			for (ClassifierData cd : idiomaticity.get(label)) {
				if (label == 0) {
					if (cd.getLabel() == 0) {
						correctLits++;
					} else {
						incorrectIds++;
					}
				} else if (label == 1) {
					if (cd.getLabel() == 1) {
						correctIds++;
					} else {
						incorrectLits++;
					}
				}
			}
		}
		double precision = (correctIds * 1.0) / (correctIds + incorrectLits);
		double recall = (correctIds * 1.0) / (correctIds + incorrectIds + result.unclassified.size());
		double fMeasure = (2.0 * precision * recall) / (precision + recall);
		double accuracy = ((correctIds + correctLits) * 1.0) / (correctIds + incorrectIds + correctLits
				+ incorrectLits + result.unclassified.size());
		
		System.out.println("[p, r]: " + precision + ", " + recall);
		System.out.println("F-measure: " + fMeasure);
		System.out.println("Accuracy: " + accuracy);
	}	
	
	public static void testClassifierModel(String modelPath, String database, String classifierTable,
			String testData, DetectorExperimentResult result, boolean augment) {
		// Now, go and use the model to classify them (as idiomatic or not)!
		// First, go and classify these using the identification model.
		Classifier classy = new Perceptron(new ClassifierModel(
				new File(modelPath)));
				
		List<ClassifierData> senses = ClassifierDataUtils.getSensesFromDb(database, classifierTable,
				" WHERE data_set = \"" + testData + "\"", "label");

		System.out.println("Gathered senses: " + senses.size());
		System.out.println("Classifying...");
		TreeMap<Integer, List<ClassifierData>> idiomaticity = new TreeMap<Integer, List<ClassifierData>>();
		for (String key : result.results.keySet()) {
			
			// These will be examples
			for (ClassifierData cd : result.results.get(key)) {
				//classify the definition of the sense associated with the key
				for (ClassifierData sense : senses) {
					if (sense.getKey().equals(key)) {
						// classify this one!
						int label = classy.predict(sense);
						int defLabel = sense.getLabel();
						if (augment) {
							if (defLabel == 1) {
								label = 1;
							}
						}
						if (!idiomaticity.containsKey(label)) {
							idiomaticity.put(label, new ArrayList<ClassifierData>());
						}
						idiomaticity.get(label).add(cd);
					}
				}
			}
		}
		
		// Print out classification results
		System.out.println("Classification results:");
		
		int correctIds = 0;
		int incorrectIds = 0;
		int correctLits = 0;
		int incorrectLits = 0;
		
		for (int label : idiomaticity.keySet()) {
			
			for (ClassifierData cd : idiomaticity.get(label)) {
				if (label == 0) {
					if (cd.getLabel() == 0) {
						correctLits++;
					} else {
						incorrectIds++;
					}
				} else if (label == 1) {
					if (cd.getLabel() == 1) {
						correctIds++;
					} else {
						incorrectLits++;
					}
				}
			}
		}
		double precision = (correctIds * 1.0) / (correctIds + incorrectLits);
		double recall = (correctIds * 1.0) / (correctIds + incorrectIds + result.unclassified.size());
		double fMeasure = (2.0 * precision * recall) / (precision + recall);
		double accuracy = ((correctIds + correctLits) * 1.0) / (correctIds + incorrectIds + correctLits
				+ incorrectLits + result.unclassified.size());

				
		System.out.println("[p, r]: " + precision + ", " + recall);
		System.out.println("F-measure: " + fMeasure);
		System.out.println("Accuracy: " + accuracy);
	}
}
