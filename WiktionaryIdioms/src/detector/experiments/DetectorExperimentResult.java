/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package detector.experiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;


import classifier.model.ClassifierData;
import classifier.model.Result;

public class DetectorExperimentResult implements Comparable<DetectorExperimentResult>{

	public TreeMap<String, List<ClassifierData>> results;
	public List<Result> resultDeltas;
	public String generalInfo;
	public List<ClassifierData> unclassified;
	
	public DetectorExperimentResult() {
		results = new TreeMap<String, List<ClassifierData>>();
		generalInfo = "";
		unclassified = new ArrayList<ClassifierData>();
	}
	
	public void addResult(String key, ClassifierData cd) {
		if (!results.containsKey(key)) {
			results.put(key, new ArrayList<ClassifierData>());
		}
		results.get(key).add(cd);
	}
	
	public void addUnclassified(ClassifierData cd) {
		unclassified.add(cd);
	}
	
	public void addResultSets(TreeMap<String, List<ClassifierData>> generalResults, List<Result> resultDeltas) {
		results = generalResults;
		this.resultDeltas = resultDeltas;
	}
	
	public double[] getPrecisionRecall() {
		// Precision = number of correct answers / number answers reported
		// Recall = number of correct answers / number of instances
		
		// First, count the corrects
		int correct = 0;
		int totalClassified = 0;
		for (String key : results.keySet()) {
			for (ClassifierData cd : results.get(key)) {
				if (cd.getKey().equals(key)) {
					correct++;
				}
				totalClassified++;
			}
		}
		double precision = (correct * 1.0) / totalClassified;
		double recall = (correct * 1.0) / (totalClassified + unclassified.size());
		double[] pr = {precision, recall};
		return pr;
	}
	
	public double getFScore() {
		double[] pr = getPrecisionRecall();
		if (pr[0] == 0 && pr[1] == 0) {
			return 0;
		}
		return (2.0 * pr[0] * pr[1]) / (pr[0] + pr[1]);
	}
	
	public double[] getCategoryPrecisionRecall() {
		int correct = 0;
		int totalClassified = 0;
		for (String key : results.keySet()) {
			for (ClassifierData cd : results.get(key)) {
				if (cd.getKey().equals(key)) {
					correct++;
				}
				totalClassified++;
			}
		}
		double precision = (correct * 1.0) / totalClassified;
		double recall = (correct * 1.0) / (totalClassified + unclassified.size());
		double[] pr = {precision, recall};
		return pr;
	}
	
	public void addPrintInfo(String info) {
		this.generalInfo += info;
	}
	
	public String getPrintInfo() {
		return generalInfo;
	}
	
	public String toString() {
		String s = "";

		s += "[precision, recall]: " + Arrays.toString(getPrecisionRecall()) + "\n";
		s += "fscore: " + getFScore();
		
		return s;
	}
	
	
	/**
	 * Get x examples of errors, where each x was incorrectly classified.
	 * @param x
	 * @param label
	 * @return
	 */
	public String getRandomErrorString(int x) {
		
		List<ClassifierData> errors = new ArrayList<ClassifierData>();
		for (String key : results.keySet()) {
			for (ClassifierData cd : results.get(key)) {
				if (!cd.getKey().equals(key)) {
					errors.add(cd);
				}
			}
		}
		
		Collections.shuffle(errors);
		
		String s = "Errors that were misclassified \n";
		s += "This is a report of (" + x + "/" + errors.size() + ")";
		for (int i = 0; i < x && i < errors.size(); i++) {
			s += errors.get(i).getDetectionString() + "\n";
		}
		return s;
	}


	@Override
	// return -num if other is > this
	// return +num if other is < this
	// return 0 if they are equal
	public int compareTo(DetectorExperimentResult other) {
		double amt = this.getFScore() - other.getFScore();
		return (int) Math.signum(amt);
	}
}
