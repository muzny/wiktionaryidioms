/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This experiment simply uses the detector method passed to get the
 * best prediction for the example, then adds this to the DetectorExperimentResult
 * that it returns.
 */

package detector.experiments;

import java.util.List;

import classifier.model.ClassifierData;

import detector.methods.DetectorMethod;
import detector.model.Example;

public class DummyExperiment implements DetectorExperiment {

	@Override
	/**
	 * In this experiment, we will use the example senses associated with the senses in the database
	 * and see if we can disambiguate them.
	 */
	public DetectorExperimentResult runExperiment(DetectorMethod d, List<ClassifierData> test) {		
		DetectorExperimentResult result = new DetectorExperimentResult();
	
		int count = 0;
		for (ClassifierData cd : test) {
			
			Example ex = (Example) cd;
							
			String best = d.predict(ex);

			if (best != null) {
				result.addResult(best, ex);
			} else {
				result.addUnclassified(ex);
			}
			count++;
			if (count % 100 == 0) {
				System.out.println(count + "/" + test.size());
			}
		}
		return result;
	}

}
