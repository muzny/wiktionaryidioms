/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Interface to run detector experiment through.
 * To add a new experiment, simply list it here.
 */

package detector.experiments;

import java.util.List;
import java.util.TreeMap;

import classifier.model.ClassifierData;

import detector.methods.DetectorMethod;


public interface DetectorExperiment {
	
	// String to pass to RunDetectorExperiment[FromFiles] -> instance of experiment object
	public static final TreeMap<String, DetectorExperiment> EXPERIMENTS = new TreeMap<String, DetectorExperiment>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
	    put("dummy", new DummyExperiment());
	    put("goldenlabels", new GoldenLabelsExperiment());
	}};

	public DetectorExperimentResult runExperiment(DetectorMethod d, List<ClassifierData> test);
}
