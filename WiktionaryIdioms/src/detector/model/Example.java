/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A model for an example. An example has a class label, a title 
 * (phrase that it is listed under), text (the example sentence),
 * and JWKTL key (corresponding to the sense it is listed under).
 * 
 * Examples are ClassifierData. This means that this framework is set up
 * so that if you wanted to try to directly classify examples, you could do
 * that.
 */

package detector.model;

import java.sql.Date;
import java.util.List;
import java.util.TreeMap;

import classifier.model.ClassifierData;
import classifier.model.Sense;

public class Example extends ClassifierData {
	public static final String DELIM = "||||";  // For file reading, the sequence of characters
												// that separate the parts listed before the features

	private String text;
	private TreeMap<Integer, Double> features;
	private Date publication;
	public String key;
	
	public Example(int label, String title, String text, String key) {
		super(key, title, label);
		this.text = text;
	}
	
	public void setNyTimesInfo(Date d) {
		publication = d;
	}

	public Date getNyTimesDate() {
		return publication;
	}
	
	public String getText() {
		return text;
	}
	
	public boolean equals(Object o) {
		if (o instanceof Example) {
			Example other = (Example) o;
			if (this.label == other.label && this.title.equals(other.title)) {
				if (other.key != null && this.key != null) {
					return other.key.equals(this.key);
				}
				return true;
			}
		}
		return false;
	}
	
	public int hashCode(){
		return text.hashCode();
	}

	@Override
	public void printPrettyString(List<Integer> featuresToUse) {
		System.out.println("UNIMPLEMENTED");
		
	}

	@Override
	public String getConfidenceString(List<Integer> featuresToUse) {
		String s = label + "--" + senseKey + " " + DELIM + " " + title + 
				" " + DELIM + " " + text + " ";		
		for (int key : featuresToUse) {
			s += key + ":" + features.get(key);
			s += " ";
		}
		return s;
	}

	@Override
	public String getDetectionString() {
		String s = senseKey + " |||| " + title + " |||| " + text + " |||| " + label;
		return s;
	}
	
 }
