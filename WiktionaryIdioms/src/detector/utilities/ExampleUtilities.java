/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A small utilities class to do things with examples.
 * Currently knows how to get examples out of a database
 * and read examples from a file.
 */

package detector.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import mysql.utilities.MySQLConnection;

import classifier.model.ClassifierData;

import detector.model.Example;
import classifier.features.numeric.Feature;

public class ExampleUtilities {
	public static final int STEP_SIZE = 100000;

	/**
	 * Gets examples from a MySQL database, with the columns "sense_key", "title",
	 * "text", and "labelKind".
	 * @param database - Database to draw from.
	 * @param table - Table to draw from.
	 * @param whereClause - Any limiting parameters you want on the SQL query.
	 * @param labelKind - What column the label is listed under.
	 * @return List of ClassifierData (that are Examples).
	 */
	public static List<ClassifierData> getExamplesFromDb(
			String database, String table, String whereClause, String labelKind) {
		
		int min = 0;
		
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("text");
		whatToSelect.add(labelKind);
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");

		List<ClassifierData> exs = new ArrayList<ClassifierData>();
		
		// 3. fill with data
		int current = min;
		while (true) {
					
			// Go and get the data.
			String finalWhereClause = whereClause + " LIMIT " + current + 
					", " + STEP_SIZE;
			current += STEP_SIZE;

			List<String[]> results = moby.selectQuery(whatToSelect, table, finalWhereClause);
						
			for (String[] result: results) {
				String senseKey = result[whatToSelect.indexOf("sense_key")];

				String title = result[whatToSelect.indexOf("title")];
				String text = result[whatToSelect.indexOf("text")];

				int label = Integer.parseInt(result[whatToSelect.indexOf(labelKind)]);
				

				Example e = new Example(label, title, text, senseKey);
				
				for (int key : Feature.FEATURE_INDEXES.keySet()) {
					String value = result[whatToSelect.indexOf(Feature.FEATURE_INDEXES.get(key).getDBName())];
					e.addFeature(key, Double.parseDouble(value));
				}
				
				exs.add(e);
	
			}
					
			if (results.size() < STEP_SIZE) {
				break;  // We're done.
			}
		}
		
		// 4. Only get the ones that have examples
		return exs;
	}
	
	/**
	 * Currently has a limit of only reading the first 1,000,000 lines of a file.
	 * Depends on the file being in the format:
	 * sense_key |||| title |||| text |||| label |||| feat1num:val1 feat2num:val2 ....
	 * @param f - File to read examples from.
	 * @return List of ClassifierData(Examples) gathered from the file.
	 */
	public static List<ClassifierData> getExamplesFromFile(File f) {
		List<ClassifierData> results = new ArrayList<ClassifierData>();
		Set<String> keys = new HashSet<String>();
		try {
			Scanner scan = new Scanner(f);

			int num = 0;
			while (scan.hasNextLine() && num < 1000000) {
				String line = scan.nextLine().trim();
				if (line.length() > 0) {
					num++;
					// First split on "||||"
					String[] parts = line.split("\\|\\|\\|\\|");
					String senseKey = parts[0].trim();
					String title = parts[1].trim();
					String gloss = parts[2].trim();
					int label = Integer.parseInt(parts[3].trim());
				
					if (!keys.contains(senseKey)) {
						Example dp = new Example(label, title, gloss, senseKey);
				
						if (parts.length == 5) {
							String[] feats = parts[4].trim().split(" ");
							// Now split on ":"
							for (String feat : feats) {
								String[] pieces = feat.split(":");
								int featId = Integer.parseInt(pieces[0]);
								double value = Double.parseDouble(pieces[1]);
								dp.addFeature(featId, value);
							}
							
						}
						results.add(dp);
						keys.add(senseKey);
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Error with file: " + f);
		}
		return results;
	}

}
