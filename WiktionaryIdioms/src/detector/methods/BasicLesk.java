/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Disambiguates based on the basic lesk algorithm, and which
 * sense has the highest computed overlap.
 */

package detector.methods;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import classifier.model.ClassifierData;
import classifier.model.Sense;
import detector.model.Example;
import classifier.utilities.StemUtils;


public class BasicLesk extends DetectorMethod {
	
	public BasicLesk(String stopswordKind) {
		super(stopswordKind);
	}
	
	public BasicLesk(String stopswordKind, String lookupDb, String lookupTable){
		super(stopswordKind, lookupDb, lookupTable);
	}
	
	public String predict(ClassifierData cd) {
		Example ex = (Example) cd;
		String text = ex.getText();
		String phrase = ex.getTitle();
		
		// First, get all the senses that have a title of the phrase in the text
		List<ClassifierData> senses = getSensesByTitle(phrase);
		
		if (senses.isEmpty()) {
			return null;
		}
		if (senses.size() == 1) {
			return senses.get(0).getKey();
		}
		
		ClassifierData best = senses.get(0);
		int bestOverlap = 0;
		
		// Get the stemmed, cleaned version of this text
		String stemmedText = StemUtils.stemAndCleanSentence(text, stops);
		String[] textWords = stemmedText.split(" ");
		
		for (ClassifierData s : senses) {
			
			// get the stemmed, cleaned gloss from this sense
			String stemmedGloss = StemUtils.stemAndCleanSentence(((Sense) s).getGloss(), stops);
			
			// count the overlap between the text and the gloss
			// get the words
			int overlap = 0;
			for (String w : textWords) {
				// See how many times w occurs in the gloss.
				overlap += StringUtils.countMatches(stemmedGloss, w);
			}
			
			if (overlap > bestOverlap) {
				bestOverlap = overlap;
				best = s;
			}
		}
		if (best == null) {
			return null;
		}
		return best.getKey();
	}

}
