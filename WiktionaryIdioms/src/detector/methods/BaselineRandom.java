/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Chooses a sense that corresponds to the phrase in question
 * randomly.
 */

package detector.methods;

import java.util.Collections;
import java.util.List;

import classifier.model.ClassifierData;
import detector.model.Example;

public class BaselineRandom extends DetectorMethod {
	
	public BaselineRandom(String stopwordsKind) {
		super(stopwordsKind);
	}
	
	public BaselineRandom(String stopswordKind, String lookupDb, String lookupTable){
		super(stopswordKind, lookupDb, lookupTable);
	}
	
	public String predict(ClassifierData cd) {
		Example ex = (Example) cd;
		String phrase = ex.getTitle();
		
		// First, get all the senses that have a title of the phrase in the text
		List<ClassifierData> senses = getSensesByTitle(phrase);
		
		if (senses.isEmpty()) {
			return null;
		}
		if (senses.size() == 1) {
			return senses.get(0).getKey();
		}
		Collections.shuffle(senses);
		return senses.get(0).getKey();
	}

}
