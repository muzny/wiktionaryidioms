/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Disambiguates based off of the elesk algorithm.
 * Does this based off of the WordNet graph. This means that the
 * class constant DATABASE_DIR needs to be set properly in WordNetUtils for this
 * to work.
 */

package detector.methods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classifier.features.dependency.Pair;
import classifier.model.ClassifierData;
import classifier.model.Sense;
import classifier.utilities.StemUtils;
import classifier.utilities.WordNetUtils;
import detector.model.Example;
import edu.smu.tspell.wordnet.AdjectiveSynset;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.VerbSynset;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class ExtendedLesk extends DetectorMethod {
	public static Map<SynsetType, Set<Pair<Relation, Relation>>> relations;
	public static int phraseSynsetCount = 0;
	
	
	public ExtendedLesk(String stopwordsKind) {
		super(stopwordsKind);
		setRelations();
	}

	public ExtendedLesk(String stopwordsKind, String lookupDb,
			String lookupTable) {
		super(stopwordsKind, lookupDb, lookupTable);
		setRelations();
	}
	
	private void setRelations() {
		if (relations == null) {
			// What relations are we interested in
			relations = new HashMap<SynsetType, Set<Pair<Relation, Relation>>>();
			relations.put(SynsetType.NOUN, new HashSet<Pair<Relation, Relation>>());
			relations.put(SynsetType.ADJECTIVE, new HashSet<Pair<Relation, Relation>>());
			relations.put(SynsetType.VERB, new HashSet<Pair<Relation, Relation>>());

			relations.get(SynsetType.NOUN).add(new Pair<Relation, Relation>(Relation.GLOSS, Relation.GLOSS));
			relations.get(SynsetType.NOUN).add(new Pair<Relation, Relation>(Relation.HYPO, Relation.MERO));
			relations.get(SynsetType.NOUN).add(new Pair<Relation, Relation>(Relation.HYPO, Relation.HYPO));
			relations.get(SynsetType.NOUN).add(new Pair<Relation, Relation>(Relation.GLOSS, Relation.MERO));
			relations.get(SynsetType.NOUN).add(new Pair<Relation, Relation>(Relation.EXAMPLE, Relation.MERO));

			relations.get(SynsetType.ADJECTIVE).add(new Pair<Relation, Relation>(Relation.GLOSS, Relation.GLOSS));
			relations.get(SynsetType.ADJECTIVE).add(new Pair<Relation, Relation>(Relation.ATTR, Relation.GLOSS));
			relations.get(SynsetType.ADJECTIVE).add(new Pair<Relation, Relation>(Relation.ALSO, Relation.GLOSS));
			relations.get(SynsetType.ADJECTIVE).add(new Pair<Relation, Relation>(Relation.EXAMPLE, Relation.GLOSS));
			//relations.get(SynsetType.ADJECTIVE).add(new Pair<Relation, Relation>(Relation.GLOSS, Relation.HYPER));
			
			relations.get(SynsetType.VERB).add(new Pair<Relation, Relation>(Relation.EXAMPLE, Relation.EXAMPLE));
			relations.get(SynsetType.VERB).add(new Pair<Relation, Relation>(Relation.EXAMPLE, Relation.HYPER));
			relations.get(SynsetType.VERB).add(new Pair<Relation, Relation>(Relation.HYPO, Relation.HYPO));
			relations.get(SynsetType.VERB).add(new Pair<Relation, Relation>(Relation.GLOSS, Relation.HYPO));
			relations.get(SynsetType.VERB).add(new Pair<Relation, Relation>(Relation.EXAMPLE, Relation.GLOSS));
		}
	}

	@Override
	public String predict(ClassifierData cd) {
		Example ex = (Example) cd;
		String text = ex.getText();
		String puncCleanedText = StemUtils.cleanSentence(text);
		String stopRemovedAndPuncCleaned = stops.removeStopWords(puncCleanedText);
		String[] textWords = stopRemovedAndPuncCleaned.split(" ");
		String phrase = ex.getTitle();
		
		// First, get all the senses that have a title of the phrase in the text
		List<ClassifierData> senses = getSensesByTitle(phrase);
		if (senses.size() == 0) {
			return null;
		}
		
		// Make sure to set the wordnet database directory
		WordNetUtils.setDatabaseDirectory();
		WordNetDatabase db = WordNetDatabase.getFileInstance();
		
		double maxScore = 0;
		Sense bestSense = (Sense) senses.get(0);
		// to adjust for phrasal relations, the first relation will always be of type GLOSS
		// we want to check and see if there is a synset in wordnet associated with phrase
		Synset[] phraseSets = db.getSynsets(phrase);
		if (phraseSets.length > 0) {
			phraseSynsetCount++;
		}
		
		for (ClassifierData senseCD : senses) {
			Sense s = (Sense) senseCD;
			String gloss = s.getGloss();
			
			double senseScore = 0;
			// for every word in the sentence compute relatedness
			for (String textWord : textWords) {
				// Get its synsets
				Synset[] sets = db.getSynsets(textWord);
				
				for (Synset set : sets) {
					if (set.getType().equals(SynsetType.ADJECTIVE) ||
							set.getType().equals(SynsetType.NOUN) ||
							set.getType().equals(SynsetType.VERB)) {
						//double glossScore = relatednessGloss(gloss, set, relations.get(set.getType()));
						//senseScore += glossScore;
						
						double nonGlossScore = relatednessGeneral(gloss, phraseSets, set, relations.get(set.getType()));
						senseScore += nonGlossScore;
					}
				}
			}
			
			if (senseScore > maxScore) {
				bestSense = s;
				maxScore = senseScore;
			}
		}
		if (bestSense == null) {
			return null;
		}
		
		return bestSense.getKey();
	}
	
	private double relatednessGeneral(String gloss, Synset[] phraseSets, Synset b, Set<Pair<Relation, Relation>> relations) {
		if (relations.isEmpty()) {
			return 0;
		}
		
		double totalScore = 0;
		for (Pair<Relation, Relation> r : relations) {
			// if the first relation in the pair is GLOSS, we want to use the passed in gloss
			totalScore += getLocalScore(gloss, phraseSets, b, r.getFirst(), r.getSecond());
			if (r.getFirst() != r.getSecond()) {
				totalScore += getLocalScore(gloss, phraseSets, b, r.getSecond(), r.getFirst());
			}

		}
		
		return totalScore;
	}
	
	private int getLocalScore(String gloss, Synset[] phraseSets, Synset b, Relation r1, Relation r2) {
		int totalScore = 0;
		if (r1 == Relation.GLOSS) {
			String[] first = StemUtils.stemAndCleanSentenceToArray(gloss, stops);
			String[] second = getStringArrayForGlossRelation(b, r2);
			totalScore += score(first, second);
		} else {

			for (Synset a : phraseSets) {
				// Get the score of the concatenation of the definitions
				// gotten from each relation function
				String[] first = getStringArrayForGlossRelation(a, r1);
				String[] second = getStringArrayForGlossRelation(b, r2);
				totalScore += score(first, second);
		
			}
		}
		return totalScore;
	}
	
	private String[] getStringArrayForGlossRelation(Synset input, Relation r) {
		List<String> plainDefs = getGlossesForRelation(input, r);
		List<String[]> cleanedAndStemmed = new ArrayList<String[]>();
		int size = 0;
		for (String def : plainDefs) {
			String[] pieced = StemUtils.stemAndCleanSentenceToArray(def, stops);
			cleanedAndStemmed.add(pieced);
			size += pieced.length;
		}
		
		String[] result = new String[size];
		int index = 0;
		for (String[] pieced : cleanedAndStemmed) {
			for (int i = 0; i < pieced.length; i++) {
				result[index] = pieced[i];
				index++;
			}
		}
		return result;
	}
	
	private List<String> getGlossesForRelation(Synset input, Relation r) {
		List<String> glosses = new ArrayList<String>();
		if (r == Relation.GLOSS) {
			glosses.add(input.getDefinition());
			return glosses;
		} else if (r == Relation.EXAMPLE) {
			return WordNetUtils.getExamples(input);
		}
		SynsetType t = input.getType();
		String humanType = "UNKNOWN";
		if (t.equals(SynsetType.NOUN)) {
			humanType = "NOUN";
			NounSynset ns = (NounSynset) input;
			if (r == Relation.HYPO) {
				return WordNetUtils.getHyponymsDefinitions(ns);
			}
			if (r == Relation.HYPER) {
				return WordNetUtils.getHypernymsDefinitions(ns);
			}
			if (r == Relation.MERO) {
				return WordNetUtils.getMeronymsDefinitions(ns);
			}
		} else if (t.equals(SynsetType.VERB)) {
			humanType = "VERB";
			VerbSynset vs = (VerbSynset) input;
			if (r == Relation.HYPER) {
				return WordNetUtils.getHypernymsDefinitions(vs);
			}
			if (r == Relation.HYPO) {
				return WordNetUtils.getHyponymsDefinitions(vs);
			}
		} else if (t.equals(SynsetType.ADJECTIVE)) {
			humanType = "ADJECTIVE";
			AdjectiveSynset as = (AdjectiveSynset) input;
			if (r == Relation.ALSO) {
				return WordNetUtils.getAlsoDefinitions(as);
			}
			if (r == Relation.ATTR) {
				return WordNetUtils.getAttributeDefinitions(as);
			}
		} else {
		// Otherwise, you passed me a type of relation that I can't get, 
		// give you a warning.
		System.err.println("WARNING: you passed me a invalid synset/relation " +
				"type combo: " + humanType + "/" + r);
		}
		return glosses;
	}

	/**
	 * Scoring function for elesk. Each overlap of size n gets a score of n^2.
	 * @param one
	 * @param two
	 * @return
	 */
	private double score(String[] one, String[] two) {
		List<Integer> overlaps = new ArrayList<Integer>();
		
		boolean found = true;
		while (found) {
			// find the next longest overlap
			Pair<Integer, Integer> oneLongest = new Pair<Integer, Integer>(-1, -1);
			Pair<Integer, Integer> twoLongest = new Pair<Integer, Integer>(-1, -1);
			int maxLen = 0;
			int[][] table = new int[one.length][two.length];
			for (int i = 0; i < one.length; i++) {
				for (int j = 0; j < two.length; j++) {
					if (one[i].equals(two[j])) {
						if (i == 0 || j == 0) {
							table[i][j] = 1;
						} else {
							table[i][j] = table[i - 1][j - 1] + 1;
						} if (table[i][j] > maxLen) {
							maxLen = table[i][j];
							oneLongest.setFirst((i + 1) - maxLen);
							oneLongest.setSecond(i);
							twoLongest.setFirst((j + 1) - maxLen);
							twoLongest.setSecond(j);
						}
					}
				}
			}
			// We found an overlap!
			if (maxLen != 0) {
				overlaps.add(maxLen);
				// now, replace the appropriate places 
				// in the strings so that they are no longer matching
				for (int i = oneLongest.getFirst(); i <= oneLongest.getSecond(); i++) {
					one[i] = one[i] + "1";
				}
				for (int i = twoLongest.getFirst(); i <= twoLongest.getSecond(); i++) {
					two[i] = two[i] + "2";
				}
			} else {
				found = false;
			}
		}
		double score = 0;
		for (int overlap : overlaps) {
			score += Math.pow(overlap, 2);
		}
		return score;
	}

}
