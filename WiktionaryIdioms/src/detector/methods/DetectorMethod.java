/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This class provides a common interface for all the disambiguation
 * methods that the detector uses. Requires detector config file to be
 * present, and requires stop words file.
 */

package detector.methods;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import mysql.utilities.MySQLConnection;

import classifier.config.GeneralConfigs;
import classifier.model.ClassifierData;

import classifier.utilities.ClassifierDataUtils;
import classifier.utilities.StopWords;

public abstract class DetectorMethod {
	
	public static final TreeMap<String, DetectorMethod> METHODS = new TreeMap<String, DetectorMethod>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		put("baseline", new Baseline(
	    		StopWords.FULL, GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.lookupDb"),
	    		GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.dbTable")));
		put("baselinefirst", new BaselineFirst(
	    		StopWords.FULL, GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.lookupDb"),
	    		GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.dbTable")));
		put("baselinerandom", new BaselineRandom(
	    		StopWords.FULL, GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.lookupDb"),
	    		GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.dbTable")));
	    put("lesk", new BasicLesk(
	    		StopWords.FULL, GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.lookupDb"),
	    		GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.dbTable")));
	    put("elesk", new ExtendedLesk(
	    		StopWords.FULL, GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.lookupDb"),
	    		GeneralConfigs.getString(GeneralConfigs.DB_CONFIGS, "detector.dbTable")));
	}};
	
	public static final TreeMap<String, DetectorMethod> METHODS_BY_FILE = new TreeMap<String, DetectorMethod>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		put("baseline", new Baseline(StopWords.FULL));
		put("baselinefirst", new BaselineFirst(StopWords.FULL));
		put("baselinerandom", new BaselineRandom(StopWords.FULL));
	    put("lesk", new BasicLesk(StopWords.FULL));
	    put("elesk", new ExtendedLesk(StopWords.FULL));
	}};
	
	protected String lookupDb;
	protected String lookupTable;
	protected StopWords stops;
	protected int iters;
	protected double errorBound;
	protected double learningRate;
	protected double[] weights;
	protected Map<String, List<ClassifierData>> sensesByTitle;
	
	public DetectorMethod(String stopwordsKind) {
		this.stops = StopWords.getInstance(stopwordsKind);
	}

	public DetectorMethod(String stopwordsKind, String lookupDb, String lookupTable) {
		this.stops = StopWords.getInstance(stopwordsKind);
	  	this.lookupDb = lookupDb;
	  	this.lookupTable = lookupTable;
	}
	
	public void setSensesByTitleMap(Map<String, List<ClassifierData>> sensesByTitle) {
		this.sensesByTitle = sensesByTitle;
	}
	
	public String getLookupDb() {
		return lookupDb;
	}
	
	public String getLookupTable() {
		return lookupTable;
	}
	
	public List<ClassifierData> getSensesByTitle(String title) {
		if (sensesByTitle != null) {
			return sensesByTitle.get(title);
		}
		MySQLConnection moby = MySQLConnection.getInstance("localhost", lookupDb, "root", "");
		return ClassifierDataUtils.getSensesWithTitle(title, moby, lookupTable);
	}
		
	/**
	 * The predict method, given a piece of ClassifierData, will give the
	 * String that corresponds to the sense it predicts to be most likely
	 * associated with that ClassifierData.
	 * @param cd
	 * @return String that is the Wiktionary (JWKTL) sense key, returns null if no
	 * sense found.
	 */
	public abstract String predict(ClassifierData cd);

}
