/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Reads a corrected annotation file back into a MySQL table.
 * Updates the "comments" and "corrected_label" columns.
 */

package fileio;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import mysql.utilities.MySQLConnection;




public class ReadCorrectedLabels {
	
	public static void main(String[] args) {
		
		String database = args[1];
		String table = args[2];
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");

		File f = new File(args[0]);
		
		try {
			Scanner scan = new Scanner(f);
			Map<String, String> commentUps = new HashMap<String, String>();
			Map<String, Integer> labelUps = new HashMap<String, Integer>();

			
			while (scan.hasNextLine()) {
				String line = scan.nextLine().trim();
				
				// go and get all the lines associated with this
				if (line.startsWith("sense key:")) {
					String senseKey = line.split("key:")[1].trim();
					
					String correctedLine = scan.nextLine().split(":")[1].trim();
					if (correctedLine.length() != 0) {
						int corrected = Integer.parseInt(correctedLine);
						String comments = scan.nextLine().split("comments:")[1].trim();
						commentUps.put(senseKey, comments);
						labelUps.put(senseKey, corrected);
					}
				}
			}
			
			System.out.print("Updating...");
			moby.updateCaseColumnString(table, "comments", "sense_key", commentUps);
			moby.updateCaseColumnInteger(table, "corrected_label", "sense_key", labelUps);
			System.out.println("done.");


		} catch (IOException e) {
			System.out.println("Error with file: " + f);
		}
	}

}
