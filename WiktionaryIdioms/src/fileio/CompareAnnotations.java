/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A main class for comparing and analyzing different annotations files.
 * These are files that are produced by the EditableFile class and that have
 * been fully annotated. Will throw an error if any examples in the annotations
 * files lack a corrected label.
 */

package fileio;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import classifier.features.dependency.Pair;
import classifier.model.Sense;

public class CompareAnnotations {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length < 1) {
			System.err.println("Usage: <full_corrections_path> [<sample_path_1> ... <sample_path_n>]");
		}
		Map<String, Pair<Integer, Sense>> fullAnnotations = readAnnotationFile(args[0]);
		
		
		List<Map<String, Pair<Integer, Sense>>> annoList = new ArrayList<Map<String, Pair<Integer, Sense>>>();
		for (int i = 1; i < args.length; i++) {
			String filename = args[i];
			Map<String, Pair<Integer, Sense>> annotations = readAnnotationFile(filename);
			annoList.add(annotations);
		}
		System.out.println("There are " + fullAnnotations.size() + " annotations in the full list");
		
		for (int i = 0; i < annoList.size(); i++) {
			System.out.println("There are " + annoList.get(i).size() + " annotations in list " + i);
		}
		
		int contained = 0;
		for (Map<String, Pair<Integer, Sense>> l : annoList) {
			Set<String> toBeRemoved = new HashSet<String>();
			for (String key : l.keySet()) {
				if (fullAnnotations.containsKey(key)) {
					contained++;
				} else {
					toBeRemoved.add(key);
				}
			}
			System.out.println("Number contained in both: " + contained);
			System.out.println("Removing extras....(" + toBeRemoved.size() + ")");
			for (String removeKey : toBeRemoved) {
				l.remove(removeKey);
			}
			contained = 0;
		}
		
		System.out.println("Checking for un-annotated data points....");
		Set<String> toBeRemoved = new HashSet<String>();
		for (int i = 0; i < annoList.size(); i++) {
			Map<String, Pair<Integer, Sense>> l = annoList.get(i);
			for (String key : l.keySet()) {
				Sense s = l.get(key).getSecond();
				if (s.getLabel() == -2) {
					System.out.println("List " + i + " has unlabeled data point: " + s.getKey());
					toBeRemoved.add(key);
				}
			}
		}
		
		for (int i = 0; i < annoList.size(); i++) {
			Map<String, Pair<Integer, Sense>> l = annoList.get(i);
			for (String removeKey : toBeRemoved) {
				l.remove(removeKey);
			}
			System.out.println("List " + i + " is has " + l.size() + " annotations now");
		}
		
		// All of the key sets will be the same and what we want
		Set<String> keys = annoList.get(0).keySet(); 
		
		// Make a third list that is from my annotations
		Map<String, Pair<Integer, Sense>> fromFull = new HashMap<String, Pair<Integer, Sense>>();
		for (String key : keys) {
			fromFull.put(key, fullAnnotations.get(key));
		}
		System.out.println("From full list has " + fromFull.size() + " annotations");
		annoList.add(fromFull);
		
		System.out.println();
		System.out.println();

		int literals = 0;
		int idioms = 0;
		int unknowns = 0;
		for (Map<String, Pair<Integer, Sense>> l : annoList) {
			
			for (String key : l.keySet()) {
				Sense s = l.get(key).getSecond();
				if (s.getLabel() == 0) {
					literals++;
				} else if (s.getLabel() == 1) {
					idioms++;
				} else if (s.getLabel() == -1) {
					unknowns++;
				}
			}
			System.out.println("Number literals: " + literals);
			System.out.println("Number idioms: " + idioms);
			System.out.println("Number unknowns: " + unknowns);
			System.out.println("Sum: " + (literals + idioms + unknowns));
			System.out.println();
			
			literals = 0;
			idioms = 0;
			unknowns = 0;
		}
		
		
		// num similar -> list of sense keys in this category
		Map<Integer, List<String>> agreement = new HashMap<Integer, List<String>>();
		for (String key : keys) {
			
			// get all the labels from this data point in each list
			List<Integer> labels = new ArrayList<Integer>();
			for (Map<String, Pair<Integer, Sense>> l : annoList) {
				labels.add(l.get(key).getSecond().getLabel());
			}
			
			int first = labels.get(0);  // should always have at least one entry
			int numSame = 0;
			for (int label : labels) {
				if (first == label) {
					numSame++;
				}
			}
			
			if (!agreement.containsKey(numSame)) {
				agreement.put(numSame, new ArrayList<String>());
			}
			agreement.get(numSame).add(key);
		}
		
		int sum = 0;
		for (int num : agreement.keySet()) {
			System.out.println("Number with " + num + " agreeing: " + agreement.get(num).size());
			sum += agreement.get(num).size();
		}
		System.out.println("Sum: " + sum);
		System.out.println();
		
		/*System.out.println("Cases with disagreement:");
		
		for (int num : agreement.keySet()) {
			if (num == annoList.size()) {
				continue;
			}
			
			for (String key : agreement.get(num)) {
				
				for (int i = 0; i < annoList.size(); i++) {
					Map<String, Pair<Integer, Sense>> l = annoList.get(i);
					System.out.println("List " + i + ":");
					System.out.println("Original label: " + l.get(key).getFirst());
					System.out.println(((Sense) l.get(key).getSecond()).getAnnotationString());
					System.out.println();
				}
			}
		}*/
		
	}
	
	public static Map<String, Pair<Integer, Sense>> readAnnotationFile(String filename) {
		// sense key -> <original, sense>
		Map<String, Pair<Integer, Sense>> annotations = new HashMap<String, Pair<Integer, Sense>>();
		Scanner scan = null;
		try {
			scan = new Scanner(new File(filename));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		while (scan.hasNextLine()) {
			String line = scan.nextLine().trim();
			
			// go and get all the lines associated with this
			if (line.startsWith("sense key:")) {
				String senseKey = line.split("key:")[1].trim();
				String title = scan.nextLine().split("title:")[1].trim();
				String gloss = scan.nextLine().split("gloss:")[1].trim();
				String uncleaned = scan.nextLine().split("gloss:")[1].trim();
				int label = Integer.parseInt(scan.nextLine().split(":")[1].trim());
				
				String correctedLine = scan.nextLine().split(":")[1].trim();
				int corrected = -2;
				if (correctedLine.length() != 0) {
					corrected = Integer.parseInt(correctedLine);
				}
					String comments = scan.nextLine().split("comments:")[1].trim();
					Sense s = new Sense(senseKey, title, gloss, uncleaned, corrected);
					annotations.put(senseKey, new Pair<Integer, Sense>(label, s));
					
					if (comments.length() > 0) {
						s.comments = comments;
					}
				
			}
		}
		return annotations;
	}

}
