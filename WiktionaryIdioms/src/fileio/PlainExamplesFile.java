/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package fileio;

import java.util.ArrayList;
import java.util.List;

import mysql.utilities.MySQLConnection;

import detector.model.Example;


public class PlainExamplesFile implements ProducesFile {
	public static final int STEP_SIZE = 100000;

	@Override
	/**
	 * Produces a string that represents a file that contains the specified examples.
	 * Specifically designed to produce a file to be read by RunDetectorExperimentFromFiles.
	 */
	public String produce(ProduceFileSettings settings) {
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("text");
		whatToSelect.add(settings.labelType);
		whatToSelect.add("data_set_classifier");
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", settings.database, "root", "");
		
		// 3. fill with data
		List<Example> points = new ArrayList<Example>();
		
		// 3. fill with data
		int current = settings.min;
					
		// Go and get the data.
		String whereClause = settings.whereClause + " LIMIT " + current + 
					", " + ProduceFile.STEP_SIZE;

		List<String[]> results = moby.selectQuery(whatToSelect, settings.table, whereClause);
						
		for (String[] result: results) {
			String senseKey = result[whatToSelect.indexOf("sense_key")];

			String title = result[whatToSelect.indexOf("title")];
			int label = Integer.parseInt(result[whatToSelect.indexOf(settings.labelType)]);
			String text = result[whatToSelect.indexOf("text")];

			Example e = new Example(label, title, text, senseKey);
				
			points.add(e);
		}
					

		System.out.println("Done gathering.");
		System.out.println(results.size());
		
		// 4. Output points to String
		String strResult = "";
		if (points.size() > 0) {
			System.out.println("Appending to string...");
			for (Example dp : points) {
				strResult += dp.getDetectionString();
				strResult += "\n";
			}
		}
		System.out.println("Done appending.");
		
		return strResult;
	}
}
