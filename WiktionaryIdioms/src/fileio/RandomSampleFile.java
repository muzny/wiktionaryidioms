/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * WARNING: this file has not been updated since the last changes were made to ProduceFile,
 * and may have issues stopping.
 */


package fileio;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import mysql.utilities.MySQLConnection;



public class RandomSampleFile implements ProducesFile {
	public static final int STEP_SIZE = 100000;

	@Override
	public String produce(ProduceFileSettings settings) {
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("gloss");
		whatToSelect.add("uncleaned_gloss");
		whatToSelect.add("label");
		whatToSelect.add("data_set");
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", settings.database, "root", "");
		
		int number = settings.num;

		String strResult = "";
		// 3. fill with data
		int current = 0;
		while (true) {
					
			// Go and get the data.
			String whereClause = settings.whereClause + " LIMIT " + current + 
					", " + STEP_SIZE;
			current += STEP_SIZE;

			List<String[]> results = moby.selectQuery(whatToSelect, settings.table, whereClause);
			
			// Select random sense keys that we want from dev and test
			List<String> devKeys = new ArrayList<String>();
			List<String> testKeys = new ArrayList<String>();
			
			for (String[] result : results) {
				String dataSet = result[whatToSelect.indexOf("data_set")];
				String senseKey = result[whatToSelect.indexOf("sense_key")];
				if (dataSet.equals("dev")) {
					devKeys.add(senseKey);
				} else if (dataSet.equals("test")) {
					testKeys.add(senseKey);
				}
			}
			
			int numDev = number;//(int) Math.ceil((1.0 / 3.0) * (number));
			int numTest = 0; //(int) Math.floor((2.0 / 3.0) * (number));

			Collections.shuffle(devKeys);
			Collections.shuffle(testKeys);
			
			System.out.println("Picked " + numDev + " results from dev");
			System.out.println("Picked " + numTest + " results from test");

			
			Set<String> keys = new HashSet<String>();
			for (int i = 0; i < numDev; i++) {
				keys.add(devKeys.get(i));
			}
			for (int i = 0; i < numTest; i++) {
				keys.add(testKeys.get(i));
			}
						
			for (String[] result: results) {
				String senseKey = result[whatToSelect.indexOf("sense_key")];

				String title = result[whatToSelect.indexOf("title")];
				int label = Integer.parseInt(result[whatToSelect.indexOf("label")]);
				String gloss = result[whatToSelect.indexOf("gloss")];
				String uncleaned = result[whatToSelect.indexOf("uncleaned_gloss")];
				
				if (keys.contains(senseKey)) {
					strResult += "sense key: " + senseKey + "\n";
					strResult += "title: " + title + "\n";
					strResult += "gloss: " + gloss + "\n";
					strResult += "uncleaned gloss: " + uncleaned + "\n";
					strResult += "label: " + label + "\n";
					strResult += "corrected label: \n" ;
					strResult += "comments: \n";
					strResult += "\n";
					strResult += "\n";
				}

			}
					
			if (results.size() < STEP_SIZE) {
				System.out.println(results.size());
				break;  // We're done.
			}
		}
		return strResult;
	}
}
