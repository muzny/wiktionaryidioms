/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * An implementation of ProducesFile that produces a file for annotating
 * senses.
 */


package fileio;

import java.util.ArrayList;
import java.util.List;

import mysql.utilities.MySQLConnection;


public class EditableFile implements ProducesFile {

	@Override
	/**
	 * An implementation of ProducesFile that produces a file for annotating
	 * senses.
	 */
	public String produce(ProduceFileSettings settings) {
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("gloss");
		whatToSelect.add("uncleaned_gloss");
		whatToSelect.add("label");
		whatToSelect.add("corrected_label");
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", settings.database, "root", "");

		String strResult = "";
		// 3. fill with data
		int current = settings.min;
					
		// Go and get the data.
		String whereClause = settings.whereClause + " LIMIT " + current + 
				", " + ProduceFile.STEP_SIZE;

		List<String[]> results = moby.selectQuery(whatToSelect, settings.table, whereClause);
						
		for (String[] result: results) {
			String senseKey = result[whatToSelect.indexOf("sense_key")];

			String title = result[whatToSelect.indexOf("title")];
			int label = Integer.parseInt(result[whatToSelect.indexOf("label")]);
			String gloss = result[whatToSelect.indexOf("gloss")];
			String uncleaned = result[whatToSelect.indexOf("uncleaned_gloss")];
			int corrected = Integer.parseInt(result[whatToSelect.indexOf("corrected_label")]);

				
			strResult += "sense key: " + senseKey + "\n";
			strResult += "title: " + title + "\n";
			strResult += "gloss: " + gloss + "\n";
			strResult += "uncleaned gloss: " + uncleaned + "\n";
			strResult += "label: " + label + "\n";
			strResult += "corrected label: " + corrected + "\n" ;
			strResult += "comments: \n";
			strResult += "\n";
			strResult += "\n";

		}
					
		
		System.out.println(results.size());
		
		return strResult;
	}
}
