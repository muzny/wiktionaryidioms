/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package fileio;

import java.util.ArrayList;

import java.util.List;

import mysql.utilities.MySQLConnection;

import classifier.features.numeric.Feature;
import classifier.model.Sense;


public class PlainSensesFile implements ProducesFile {

	@Override
	/**
	 * Produces a string that represents a file that contains the specified senses.
	 * Specifically designed to produce a file to be read by RunClassifierExperimentFromFiles.
	 * Also works for producing the necessary all senses file for RunDetectorExperimentFromFiles.
	 */
	public String produce(ProduceFileSettings settings) {
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("gloss");
		whatToSelect.add("uncleaned_gloss");
		whatToSelect.add(settings.labelType);
		whatToSelect.add("data_set");
		
		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			if (settings.features.contains(key)) {
				whatToSelect.add(Feature.FEATURE_INDEXES.get(key).getDBName());
			}
		}
		
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", settings.database, "root", "");
		
		// 3. fill with data
		List<Sense> points = new ArrayList<Sense>();
		
		// 3. fill with data
		int current = settings.min;
					
		// Go and get the data.
		String whereClause = settings.whereClause + " LIMIT " + current + 
					", " + ProduceFile.STEP_SIZE;

		List<String[]> results = moby.selectQuery(whatToSelect, settings.table, whereClause);
						
		for (String[] result: results) {
			String senseKey = result[whatToSelect.indexOf("sense_key")];

			String title = result[whatToSelect.indexOf("title")];
			int label = Integer.parseInt(result[whatToSelect.indexOf(settings.labelType)]);
			String gloss = result[whatToSelect.indexOf("gloss")];
			String uncleaned = result[whatToSelect.indexOf("uncleaned_gloss")];
				
			Sense dp = new Sense(senseKey, title, gloss, uncleaned, label);
			for (int key : Feature.FEATURE_INDEXES.keySet()) {
				if (settings.features.contains(key)) {
					dp.addFeature(key, Double.parseDouble(
						result[whatToSelect.indexOf(Feature.FEATURE_INDEXES.get(key).getDBName())]));
				}
			}
			points.add(dp);
		}
					
			
		System.out.println("Done gathering.");
		System.out.println(points.size());
		
		// 4. Output points to String
		String strResult = "";
		if (points.size() > 0) {
			System.out.println("Appending to string...");
			for (Sense dp : points) {
				strResult += dp.getDataString();
				strResult += "\n";
			}
		}
		System.out.println("Done appending.");
		
		return strResult;
	}
}
