/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The main class to access all ProducesFiles implementors from.
 * 
 * Provides an interface to the database to produce text files from it.
 * Parameters are as follows:
 * -outfile: the path the the output file
 * -kind: what kind of file you would like to produce, 
 *	"edit", "nytimes", "random", "plainsense", "plainexample"
 * -db: the mysql database to connect to (assumes hosted at "root" with "" password)
 * -table: the mysql table to draw from
 * -num: The raw number of things that you would like output, used in "edit", "nytimes", and "random"
 * -numexamples: The max number of examples associated with each title you would like, used in "nytimes"
 * -glosstable: the table where the glosses are kept, used in "nytimes"
 * -labeltype: the column heading for the label you you like output, used in "plainsense"
 * -features: the features that you would like output, used in "plainsense", formated as "#, #, #",
 * 	where all numbers are associated with a feature in the features.numeric package
 */


package fileio;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;

import classifier.features.numeric.Feature;
import classifier.utilities.CommandLineUtils;




public class ProduceFile {
	public static final String OUTFILE = "-outfile";
	public static final String KIND = "-kind";
	public static final String WHERE = "-where";
	public static final String DB = "-db";
	public static final String TABLE = "-table";
	public static final String NUM = "-num";
	public static final String NUM_EXAMPLES = "-numexamples";
	public static final String GLOSS_TABLE = "-glosstable";
	public static final String LABEL_TYPE = "-labeltype";
	public static final String FEATURES = "-features";
	
	public static final int STEP_SIZE = 1000;

	
	public static void main(String[] args) {
		
		Map<String, String> argMap = CommandLineUtils.simpleCommandLineParser(args);

		String outfile = argMap.get(OUTFILE);
		
		ProduceFileSettings settings = new ProduceFileSettings();
		if (argMap.containsKey(WHERE)) {
			settings.whereClause = argMap.get(WHERE);
		}
		settings.database = argMap.get(DB);
		settings.table = argMap.get(TABLE);
		
		if (argMap.containsKey(GLOSS_TABLE)) {
			settings.glossTable = argMap.get(GLOSS_TABLE);
		}
		if (argMap.containsKey(NUM)) {
			settings.num = Integer.parseInt(argMap.get(NUM));
		}
		if (argMap.containsKey(NUM_EXAMPLES)) {
			settings.numExamples = Integer.parseInt(argMap.get(NUM_EXAMPLES));
		}
		
		if (argMap.containsKey(LABEL_TYPE)) {
			settings.labelType = argMap.get(LABEL_TYPE);
		}
		
		settings.features = new HashSet<Integer>();
		if (argMap.containsKey(FEATURES)) {
			String list = argMap.get(FEATURES);
			String[] nums = list.split(",");
			for (String num : nums) {
				settings.features.add(Integer.parseInt(num.trim()));
			}
		} else {
			for (int i : Feature.FEATURE_INDEXES.keySet()) {
				settings.features.add(i);
			}
		}
		System.out.println("The produced file will have the following features: " + settings.features);
		
		System.out.print("Writing to file...");
		try {
			BufferedWriter bw = new BufferedWriter(
					new FileWriter(outfile));
			
			settings.min = 0;
			String toWrite = ProducesFile.INDEXES.get(argMap.get(KIND)).produce(settings);
			
			while (toWrite.length() != 0) {
				bw.write(toWrite);
				
				// if we are doing random, we don't want this
				if (argMap.get(KIND).equals("random")) {
					break;
				}
				settings.min += STEP_SIZE;
				toWrite = ProducesFile.INDEXES.get(argMap.get(KIND)).produce(settings);
				System.out.println("Fetched next batch: " + settings.min);
				System.out.println("Current string was: " + toWrite.length());
			}
			
			bw.close();
		} catch (IOException e) {
			System.out.println("Error with file: " + outfile);
		}
		System.out.println("done");
	
	}
}
