/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Interface to ProducesFiles objects, that know how to read the settings
 * and correspondingly produce a string to be output to a file.
 */

package fileio;

import java.util.TreeMap;

public interface ProducesFile {
	public static final TreeMap<String , ProducesFile> INDEXES = new TreeMap<String , ProducesFile>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
	    put("edit", new EditableFile());
	    put("random", new RandomSampleFile());
	    put("plainsense", new PlainSensesFile());
	    put("plainexample", new PlainExamplesFile());


	}};
	
	public String produce(ProduceFileSettings settings);

}
