/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Some scripts for analyzing databases and feature statistics upon initial
 * entry into the database.
 */

package classifier.miscellaneous;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mysql.utilities.MySQLConnection;

import classifier.features.numeric.Feature;



public class BasicStats {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Connect to the database where the idioms are stored.
		if (args.length != 3) {
			throw new IllegalArgumentException("Usage: <database> <sense_data_table> <def_column>");
		}
		String database = args[0];
		String senseTable = args[1];
		String def = args[2];
		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");
		
		
		// We will be analyzing for classes 1 and "not 1"
		// For every feature, get its average, standard deviation,
		// and the number of examples <, > within each class
		String count = "COUNT(*)";
		
		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			System.out.println("*******************");
			System.out.println("Feature: " + Feature.FEATURE_INDEXES.get(key).getDBName());

			String[] ends = {" AND " + Feature.FEATURE_INDEXES.get(key).getDBName() + " > 0 ", ""};
			for (String end : ends) {
				String[] classStmts = {" WHERE label != 1 AND " + def + " != \"\" " + end, " WHERE label = 1 AND " + def + " != \"\" " + end};

				System.out.println("-+-+-+-+-+-+-+-+-+-+-+-+-");
				System.out.println("Totals: ");
				Map<String, Double> totals = new HashMap<String, Double>();
				List<String> nClassStmts = new ArrayList<String>();
				for (String cStmt : classStmts) {
					double result = Double.parseDouble(moby.selectQuerySingle(count, senseTable, cStmt));
					prettyPrint("For " + cStmt, result, 0);
					if (result != 0) {
						// we want to issue the other queries for this thing
						nClassStmts.add(cStmt);
					}
					totals.put(cStmt, result);
				}
			
				String avg = "AVG(" + Feature.FEATURE_INDEXES.get(key).getDBName() + ")";
				Map<String, Double> avgVals = new HashMap<String, Double>();
				for (String cStmt : nClassStmts) {
					double result = Double.parseDouble(moby.selectQuerySingle(avg, senseTable, cStmt));
					prettyPrint("Average " + cStmt, result, 0);
					avgVals.put(cStmt, result);
				}
			

				for (String cStmt : nClassStmts) {
					for (String avgValKey : avgVals.keySet()) {
						String modifiedWhere = cStmt + " AND " + Feature.FEATURE_INDEXES.get(key).getDBName() +
								" < " + avgVals.get(avgValKey);
						double result = Double.parseDouble(moby.selectQuerySingle(count, senseTable, modifiedWhere));
						prettyPrint(modifiedWhere, result, result / totals.get(cStmt));
						modifiedWhere = cStmt + " AND " + Feature.FEATURE_INDEXES.get(key).getDBName() +
								" >= " + avgVals.get(avgValKey);
						result = Double.parseDouble(moby.selectQuerySingle(count, senseTable, modifiedWhere));
						prettyPrint(modifiedWhere, result, result / totals.get(cStmt));
					}
				}
			
			
			
				String stddev = "STDDEV(" + Feature.FEATURE_INDEXES.get(key).getDBName() + ")";
				for (String cStmt : nClassStmts) {
					double result = Double.parseDouble(moby.selectQuerySingle(stddev, senseTable, cStmt));
					prettyPrint("Standard deviation " + cStmt, result, 0);
				}
			
				String max = "MAX(" + Feature.FEATURE_INDEXES.get(key).getDBName() + ")";
				for (String cStmt : nClassStmts) {
					double result = Double.parseDouble(moby.selectQuerySingle(max, senseTable, cStmt));
					prettyPrint("Maximum " + cStmt, result, 0);
				}
			
				String min = "MIN(" + Feature.FEATURE_INDEXES.get(key).getDBName() + ")";
				for (String cStmt : nClassStmts) {
					double result = Double.parseDouble(moby.selectQuerySingle(min, senseTable, cStmt));
					prettyPrint("Minimum " + cStmt, result, 0);
				}
			}
		}
	}
	
	private static void prettyPrint(String kind, double result, double percent) {
		kind = makePretty(kind);
		String percentString = "";
		if ( percent > 0) {
			double nPercent = (double) Math.round(percent * 10000) / 10000;
			percentString = "(" + nPercent + ")";
		}
		System.out.printf("%-60s : %16f %s\n", kind, result, percentString);
	}
	
	private static String makePretty(String s) {
		String[] parts = s.split(" ");
		String result = "";
		for (String p : parts) {
			if (!p.contains("_")) {
				result += p + " "; 
			}
		}
		return result.trim();
	}
}
