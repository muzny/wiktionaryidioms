/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.miscellaneous;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import classifier.utilities.CommandLineUtils;


public class AnalyzeResults {
	public static final String FILES = "-files";
	public static final String OUTPUT = "-output";


	public static void main(String[] args) {
		Map<String, String> argMap = CommandLineUtils.simpleCommandLineParser(args);
		if (argMap.size() < 1  ||
				!argMap.containsKey(FILES)) {
			System.err.println("required arguments: -files paths");
		}
		
		// parse the file names
		List<String> files = getFileNames(argMap.get(FILES));

		List<List<String>> strs = getStringsFromFiles(files);
		
		Map<String, Map<Integer, Integer>> cts = countStrings(strs);
		
		if (argMap.containsKey(OUTPUT)) {
			String outfile = argMap.get(OUTPUT);
			System.out.println("Writing to file: " + outfile);
			try {
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(outfile));
				bw.write("Analyzing: \n");

				int i = 0;
				for (String file : files) {
					bw.write(i + ":" + file + "\n");
					i++;
				}
				bw.write("-------------\n");
				bw.write(getPrettyString(cts));
				bw.close();
			} catch (IOException e) {
				System.out.println("Error with file: " + outfile);
			}
		}
	}
	
	public static String getPrettyString(Map<String, Map<Integer, Integer>> counts) {
		// Find the largest count
		int max = 0;
		for (String key : counts.keySet()) {
			// sum over all entries in the list
			int sum = 0;
			for (int i : counts.get(key).keySet()) {
				sum += counts.get(key).get(i);
			}
			if (sum > max) {
				max = sum;
			}
		}

		String s = "";
		// Now, print things starting from the most common to the least common
		for (int num = max; num > 0; num--) {
			for (String key : counts.keySet()) {
				int sum = 0;
				for (int i : counts.get(key).keySet()) {
					sum += counts.get(key).get(i);
				}
				if (sum == num) {
					String subCounts = "";
					for (int k : counts.get(key).keySet()) {
						subCounts += k + ":" + counts.get(key).get(k) + " ";
					}
					s += num + " [" + subCounts.trim() + "]" + " = " + key + "\n";
				}
			}
		}
		return s;
	}
	
	public static Map<String, Map<Integer, Integer>> countStrings(List<List<String>> strs) {
		Map<String, Map<Integer, Integer>> counts = new HashMap<String, Map<Integer, Integer>>();
		for (int i = 0; i < strs.size(); i++) {
			List<String> file = strs.get(i);
			Map<String, Integer> subCounts = new HashMap<String, Integer>();

			for (String result : file) {
				// get the true key
				String[] pieces = result.split("\\|\\|");
				String title = pieces[0].trim() + " || " + pieces[1].trim() + " || " + pieces[2].trim();

				// this has occurred before
				if (!subCounts.containsKey(title)) {
					subCounts.put(title, 0);
				}
				subCounts.put(title, subCounts.get(title) + 1);
			}
			
			// Add them to the overall counts map
			for (String key : subCounts.keySet()) {
				if (!counts.containsKey(key)) {
					counts.put(key, new HashMap<Integer, Integer>());
				}
				counts.get(key).put(i, subCounts.get(key));
			}
		}
		return counts;
	}
	
	public static List<List<String>> getStringsFromFiles(List<String> fNames) {
		List<List<String>> contents = new ArrayList<List<String>>();
		for (int i = 0; i < fNames.size(); i++) {
			contents.add(new ArrayList<String>());
			String name = fNames.get(i);
			try {
				Scanner scan = new Scanner(new File(name));
				while (scan.hasNextLine()) {
					String line = scan.nextLine().trim();
					if (line.length() > 0) {
						contents.get(i).add(line);
					}
				}
			} catch (IOException e) {
				System.out.println("Error with file: " + name);
			}
		}
		return contents;
	}
	
	
	public static List<String> getFileNames(String unsplit) {
		List<String> files = new ArrayList<String>();
		String[] names = unsplit.split(",");
		for (String name : names) {
			files.add(name);
		}
		return files;
	}
	
}
