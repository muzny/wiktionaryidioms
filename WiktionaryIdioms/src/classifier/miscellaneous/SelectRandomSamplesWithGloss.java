/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.miscellaneous;

import java.util.ArrayList;
import java.util.List;

import mysql.utilities.MySQLConnection;

public class SelectRandomSamplesWithGloss {
	
	public static void main(String[] args) {
		if (args.length != 4) {
			throw new IllegalArgumentException("Usage: <data_database>" +
					" <data_table> <sense_table> <where_clause>");
		}
		String dataDatabase = args[0];
		String dataTable = args[1];
		String senseTable = args[2];
		String whereClause = args[3];
		System.out.println("Where clause: " + whereClause);
		System.out.println("Database: " + dataDatabase);
		System.out.println("Data table: " + dataTable);
		System.out.println("Sense table: " + senseTable);		
		
		// Reset select list for inserts.
		List<String> select = new ArrayList<String>();
		select.add("sense_key");
		select.add("title");
		select.add("language");
		select.add("gloss");
		select.add("uncleaned_gloss");

		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", dataDatabase, "root", "");
		String orderClause = whereClause + " ORDER BY RAND() LIMIT 500 ";
		List<String[]> results = moby.selectQuery(select, dataTable, orderClause);
		
		for (String[] r: results) {
			// print it out in a tab-separated fashion
			System.out.println(r[0] + "\t" + r[1] + "\t" + r[3] + "\t" + r[4]);
		}
	}
}
