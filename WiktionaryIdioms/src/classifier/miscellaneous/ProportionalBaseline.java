/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.miscellaneous;
/**
 * A baseline that classifies proportional to what it observes during "training".
 * That is, if it observe 60% class 0 and 40% class 1, it will classify 60% of the
 * test data as 0 and 40% as 1.
 */


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import classifier.config.GeneralConfigs;
import classifier.model.ClassifierData;
import classifier.model.ClassifierEvaluationUtils;
import classifier.utilities.ClassifierDataUtils;


public class ProportionalBaseline {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Loading data...");

		List<ClassifierData> trainData = null;
		List<ClassifierData> testData = null;
		boolean cleanTest = false;
		boolean testCorrectedLabel = false;

		String database = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.database");
		String table = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.table");
		String column = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.column");
		String testName = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.testData");
		String where = " WHERE " + column + " = \"train\" ";
		trainData = ClassifierDataUtils.getSensesFromDb(
				database, table, where, "label");
		String labelStr = "label";
		if (testCorrectedLabel) {
			labelStr = "corrected_label";
		}
		System.out.println("Using test label column: " + labelStr);
		where = " WHERE " + column + " = \"" + testName +
				"\" AND " + labelStr + " IS NOT NULL " +
				"AND " + labelStr + " != -1 ";
			
		if (cleanTest) {
			System.out.println("Omitting specifics from test data");
			where += "AND specific_definition = \"\"";
		}

		testData = ClassifierDataUtils.getSensesFromDb(
				database, table, where, labelStr);
		
		TreeMap<Integer, List<ClassifierData>> results = classify(trainData, testData);
		
		System.out.println("[p, r]: " + Arrays.toString(ClassifierEvaluationUtils.getPrecisionRecall(results, 1)));
		System.out.println("f: " + ClassifierEvaluationUtils.getFMeasure(results, 1));
	}
	
	public static TreeMap<Integer, List<ClassifierData>> classify(List<ClassifierData> train, List<ClassifierData> test) {
		// learn the proportions
		Map<Integer, Integer> counts = new HashMap<Integer, Integer>();
		for (ClassifierData cd : train) {
			if (!counts.containsKey(cd.getLabel())) {
				counts.put(cd.getLabel(), 0);
			}
			counts.put(cd.getLabel(), counts.get(cd.getLabel()) + 1);
		}
		
		Map<Integer, Double> proportions = new HashMap<Integer, Double>();
		int total = counts.get(0) + counts.get(1);
		proportions.put(0, (1.0 * counts.get(0)) / total);
		proportions.put(1, (1.0 * counts.get(1)) / total);

		// classify based on these proportions
		TreeMap<Integer, List<ClassifierData>> results = new TreeMap<Integer, List<ClassifierData>>();
		Random r = new Random();
		for (ClassifierData cd : test) {
			double num = r.nextDouble();
			
			int classification = 1;
			if (num <= proportions.get(0)) {
				classification = 0;
			}
			
			if (!results.containsKey(classification)) {
				results.put(classification, new ArrayList<ClassifierData>());
			}
			results.get(classification).add(cd);
		}
		return results;
	}

}
