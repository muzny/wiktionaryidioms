/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A classifier that implements the same training algorithm as the Perceptron,
 * except that it attempts to bootstrap itself by "trusting" its classifications
 * after each iteration of training, therefore pretending that the points that
 * it most strongly believed were idiomatic will be counted as idiomatic, even if
 * their official label is literal.
 * 
 * Warning: this classifier is less ready to do more than two-dimensional classification
 * than the regular Perceptron class.
 */

package classifier.classifiers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.model.Result;


public class TrustingPerceptron extends Classifier {
	// This field controls what percentage of the training examples the classifier
	// will pretend are positive (idiomatic) examples.
	// .8 means that the top 20% of "most positively idiomatic" examples will be regarded
	// as idiomatic the next iteration no matter their "true" label.
	public static final double POS_CUTOFF = .8;

	private int numIterations;
	private double errorBound;
	private double learningRate;
	private double[] weights;
	private List<Integer> featureTypes;
	
	public TrustingPerceptron(int numIterations, double errorBound, 
			double learningRate, List<Integer> featureTypes) {
		super(numIterations, errorBound, learningRate, featureTypes);
	}
	
	public TrustingPerceptron(ClassifierModel model) {
		super(model);
	}
	
	public ClassifierModel train(List<ClassifierData> data) {
		if (data.size() == 0) {
			return null;
		}
		Set<ClassifierData> incognito = new HashSet<ClassifierData>();
		Map<Integer, List<ClassifierData>> incogPreds = new HashMap<Integer, List<ClassifierData>>();
		// Start with 0 weights
		weights = new double[(featureTypes.size() + 1) * ClassifierData.getClasses().size()];
		
		int iters = 0;
		double iterationError = errorBound;
		while (iters < numIterations && iterationError >= errorBound) {
			iters++;
			
			iterationError = 0;
			
			// accumulate update
			double[] totalUpdate = new double[weights.length];
			// Visit the training instances one by one.
			for (int i = 0; i < data.size(); i++) {
				// Make a prediction
				int prediction = predict(data.get(i));
		
				int correctLabel = data.get(i).getLabel();
				// If it is an incognito positive, make sure that it is still an error
				if (incognito.contains(data.get(i))) {
					// the label should actually be 1
					correctLabel = 1;
					
					// count what it was predicted as
					if (!incogPreds.containsKey(prediction)) {
						incogPreds.put(prediction, new ArrayList<ClassifierData>());
					}
					incogPreds.get(prediction).add(data.get(i));
				}
				
				// If wrong, adjust weights
				if (prediction != correctLabel) {
					
					update(totalUpdate, data.get(i), prediction, correctLabel);
					// Accumulate iteration error.

					iterationError += 1;
				}
			}
			// Update the weights by adding the averaged update
			averagedUpdateWeights(totalUpdate, data.size());
			
			iterationError = (iterationError / data.size());
			
			// Update the incognito set
			incognito = getUpdatedIncognito(incogPreds, data);
			incogPreds = new HashMap<Integer, List<ClassifierData>>();
			
			// Uncomment this to see how many examples the classifier is pretending are
			// idiomatic but actually aren't.
			//System.out.println("The incognito set size is: " + incognito.size());
		}
		return new ClassifierModel(this);
	}
	
	private Set<ClassifierData> getUpdatedIncognito(
			Map<Integer, List<ClassifierData>> incogPreds, List<ClassifierData> data) {
		Set<ClassifierData> incognito = new HashSet<ClassifierData>();
		
		// If it classified the incognito sense correctly previously, keep it in the incognito set
		if (incogPreds.containsKey(1)) {
			incognito.addAll(incogPreds.get(1));
		}
		
		// For the rest of the data, if it is a "negative" example, see if it actually looks positive.
		List<Result> testResult = testGetSortedDeltas(data);
		
		double positiveCutoff = getPositiveCutoff(testResult);
		for (Result r : testResult) {
			if (r.getDifference() >= positiveCutoff) {
				incognito.add(r.getClassifierData());
			}
		}
		
		return incognito;
	}
	
	private double getPositiveCutoff(List<Result> results) {
		// We want to get the number that POS_CUTOFF percent of positive examples are above
		List<Result> positives = new ArrayList<Result>();
		for (Result r : results) {
			if (r.getClassifierData().getLabel() == 1) {
				positives.add(r);
			}
		}
		
		int numBelowCutoff = (int) Math.floor(positives.size() * POS_CUTOFF);
		
		// Cutoff is the difference value at the numBelowCutoff - 1th element
		return positives.get(numBelowCutoff - 1).getDifference();
	}
	
	
	private void averagedUpdateWeights(double[] totalUpdate, int numExamples) {
		for (int i = 0; i < weights.length; i++) {
			weights[i] += (totalUpdate[i] / (numExamples * 1.0));
		}
	}

	private void update(double[] totalUpdate, ClassifierData point, int pred, int correct) {
		double[] correctPhis = ClassifierData.phi(point, correct, featureTypes);
		double[] wrongPhis = ClassifierData.phi(point, pred, featureTypes);
		for (int i = 0; i < totalUpdate.length; i++) {
			totalUpdate[i] = totalUpdate[i] + (learningRate * (correctPhis[i] - wrongPhis[i]));	
		}
	}
}
