/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A classifier that implements the Perceptron algorithm, performing averaged
 * updates. This means that it does one update every iteration through the entire
 * set of training data, and that this update is an averaged update.
 */

package classifier.classifiers;

import java.util.List;

import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;

public class Perceptron extends Classifier {
	
	public Perceptron(int numIterations, double errorBound, double learningRate, List<Integer> featureTypes) {
		super(numIterations, errorBound, learningRate, featureTypes);
	}
	
	public Perceptron(ClassifierModel model) {
		super(model);
	}
	
	public String toString() {
		String s = "Perceptron: \n";
		s += "iterations: " + numIterations + "\n";
		s += "errorBound: " + errorBound + "\n";
		s += "learningRate: " + learningRate + "\n";
		s += "features: " + featureTypes + "\n";
		return s;
	}
	
	
	public ClassifierModel train(List<ClassifierData> data) {
		if (data.size() == 0) {
			return null;
		}
		// Start with 0 weights
		weights = new double[(featureTypes.size() + 1) * ClassifierData.getClasses().size()];
		
		int iters = 0;
		double iterationError = errorBound;
		while (iters < numIterations && iterationError >= errorBound) {
			iters++;
			
			iterationError = 0;
			
			// accumulate update
			double[] totalUpdate = new double[weights.length];
			// Visit the training instances one by one.
			for (int i = 0; i < data.size(); i++) {
				// Make a prediction
				int prediction = predict(data.get(i));
		
				// If wrong, adjust weights
				if (prediction != data.get(i).getLabel()) {
					update(totalUpdate, data.get(i), prediction);
					// Accumulate iteration error.

					iterationError += 1;
				}
			}
			// Update the weights by adding the averaged update
			averagedUpdateWeights(totalUpdate, data.size());
			
			iterationError = (iterationError / data.size());
		}
		
		return new ClassifierModel(this);
	}
	
	private void averagedUpdateWeights(double[] totalUpdate, int numExamples) {
		for (int i = 0; i < weights.length; i++) {
			weights[i] += (totalUpdate[i] / (numExamples * 1.0));
		}
	}

	private void update(double[] totalUpdate, ClassifierData point, int pred) {
		double[] correctPhis = ClassifierData.phi(point, point.getLabel(), featureTypes);
		double[] wrongPhis = ClassifierData.phi(point, pred, featureTypes);
		for (int i = 0; i < totalUpdate.length; i++) {
			totalUpdate[i] = totalUpdate[i] + (learningRate * (correctPhis[i] - wrongPhis[i]));	
		}
	}
}
