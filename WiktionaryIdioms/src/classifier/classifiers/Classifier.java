/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The classifier is the class that can be extended and work with all of the classifier
 * experiments to produce a classification model. It is based off of the Perceptron
 * algorithm, and is therefore most easily extendible to Perceptron-based learning
 * algorithms.
 */


package classifier.classifiers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.model.Result;
import classifier.model.Sense;


public abstract class Classifier {
	
	protected int numIterations;
	protected double errorBound;
	protected double learningRate;
	protected double[] weights;
	protected List<Integer> featureTypes;
	
	/**
	 * A classifier is defined by the parameters that this constructor takes in. The initially
	 * constructed classifier has null weights, which become set by calling train.
	 * @param numIterations - The number of times to fully iterate through the entire set
	 * of data points.
	 * @param errorBound - The error level under which the classifier should stop iterating
	 * even if it hasn't reached numIterations yet. Typically set to a small number.
	 * @param learningRate - The learning rate, or discount factor that helps determine how
	 * much a classifier learns from each update it performs.
	 * @param featureTypes - Which features the classifier uses. Should be a list of the
	 * integers corresponding to the features in classifier.features.numeric.Feature.
	 */
	public Classifier(int numIterations, double errorBound,
						double learningRate, List<Integer> featureTypes) {
		this.numIterations = numIterations;
		this.errorBound = errorBound;
		this.learningRate = learningRate;
		this.featureTypes = featureTypes;
	}
	
	/**
	 * Load a classifier from a saved classifier model. This classifier typically
	 * does not need to learn new weights, because it will have loaded them from the
	 * ClassifierModel, meaning that you should not run the train method.
	 * @param model - ClassifierModel to load
	 */
	public Classifier(ClassifierModel model) {
		this.numIterations = model.numIterations;
		this.errorBound = model.errorBound;
		this.learningRate = model.learningRate;
		this.featureTypes = model.features;
		this.weights = model.weights;
	}
		
	/**
	 * Method for training the classifier, i.e. learning the weights.
	 * @param data - training data for the classifier - each ClassifierData
	 * should have the features passed into the Classifier constructor set.
	 * @return The resulting classifier model, containing the number of iterations,
	 * error bound, learning rate, feature types, and weights of the classifier
	 * after it has finished training.
	 */
	public abstract ClassifierModel train(List<ClassifierData> data);
	
	/**
	 * Tests the List of Senses. For each data point, it predicts the class
	 * based on the weights learned from the training step.
	 * @param testPoints - the list of points to be classified
	 * @return A map from label to list of DataPoint. These are organized such that all DataPoints
	 *  with predicted class label are in label's list.
	 */
	public TreeMap<Integer, List<ClassifierData>> test(List<ClassifierData> testPoints) {
		TreeMap<Integer, List<ClassifierData>> predictions = new TreeMap<Integer, List<ClassifierData>>();
		// First go through and put a mapping for every class in
		for (int label : Sense.getClasses()) {
			predictions.put(label, new ArrayList<ClassifierData>());
		}
		for (int i = 0; i < testPoints.size(); i++) {
			int pred = predict(testPoints.get(i));
			if (!predictions.containsKey(pred)) {
				predictions.put(pred, new ArrayList<ClassifierData>());
			}
			predictions.get(pred).add(testPoints.get(i));
		}
		return predictions;
	}
	
	/**
	 * Tests the List of Senses. For each data point, it predicts the class
	 * based on the weights learned from the training step.
	 * @param testPoints - the list of points to be classified
	 * @return A map from label to list of DataPoint. These are organized such that all DataPoints
	 *  with predicted class label are in label's list.
	 */
	public List<Result> testGetSortedDeltas(List<ClassifierData> testPoints) {
		List<Result> predictions = new ArrayList<Result>();
		// First go through and put a mapping for every class in

		for (int i = 0; i < testPoints.size(); i++) {
			Result pred = predictResult(testPoints.get(i));
			predictions.add(pred);
		}
		Collections.sort(predictions);
		return predictions;
	}
	
	/**
	 * Accessor method for the weights learned during training.
	 * @return a copy of the weights array. This should be an array of length
	 * ((number of features + 1) * number of classes)
	 */
	public double[] getWeights() {
		return Arrays.copyOf(weights, weights.length);
	}
	
	
	/**
	 * Predict the label of a single ClassifierData based on the weights learned
	 * during training.
	 * @param point - ClassifierData to be classified.
	 * @return - int label that had the highest score according to the learned weights.
	 */
	public int predict(ClassifierData point) {

		double maxPred = 0;
		int bestClass = Integer.MIN_VALUE;
		for (int label : ClassifierData.getClasses()) {
			double pred = 0;
			double[] phiVals = ClassifierData.phi(point, label, featureTypes);
			for (int i = 0; i < weights.length; i++) {
				pred += (weights[i] * phiVals[i]);
			}
			if(pred > maxPred || bestClass == Integer.MIN_VALUE) {
				maxPred = pred;
				bestClass = label;
			}
		}
		return bestClass;
	}
	
	/**
	 * Predict the classification result for a single ClassifierData point 
	 * according to the model learned during training.
	 * @param point - ClassifierData to be classified.
	 * @return - The Result for the data point. The Result contains information
	 * about what values the classifier assigned to each possible class.
	 */
	public Result predictResultGeneral(ClassifierData point) {

		Result r = new Result(point);
		for (int label : Sense.getClasses()) {
			double pred = 0;
			double[] phiVals = ClassifierData.phi(point, label, featureTypes);
			for (int i = 0; i < weights.length; i++) {
				pred += (weights[i] * phiVals[i]);
			}
			r.addPrediction(label, pred);
		}
		return r;
	}	
	
	/**
	 * Predict the classification result for a single ClassifierData point 
	 * according to the model learned during training.
	 * @param point - ClassifierData to be classified.
	 * @return - The Result for the data point. The Result contains information
	 * about what values the classifier assigned to the idiomatic vs. literal label.
	 */
	public Result predictResult(ClassifierData point) {

		double litPred = 0;
		double idiomPred = 0;
		for (int label : ClassifierData.getClasses()) {
			double pred = 0;
			double[] phiVals = ClassifierData.phi(point, label, featureTypes);
			for (int i = 0; i < weights.length; i++) {
				pred += (weights[i] * phiVals[i]);
			}
			if (label == 0) {
				litPred = pred;
			} else if (label == 1) {
				idiomPred = pred;
			}
		}
		return new Result(point, litPred, idiomPred);
	}
	
	/**
	 * Set the number of iterations for the classifier to perform during training.
	 * @param iters - number of iterations (should not be 0 or negative)
	 */
	public void setIterations(int iters) {
		this.numIterations = iters;
	}
	
	/**
	 * Set the error bound for the classifier to honor during training.
	 * @param error - new error bound
	 */
	public void setErrorBound(double error) {
		this.errorBound = error;
	}
	
	/**
	 * Set the learning rate for the classifier during training.
	 * @param learn - new error rate <0, 1]
	 */
	public void setLearningRate(double learn) {
		this.learningRate = learn;
	}
	
	/**
	 * Accessor method for the number of iterations.
	 * @return - number of iterations for the current model.
	 */
	public int getIterations() {
		return this.numIterations;
	}
	
	/**
	 * Accessor method for the error bound.
	 * @return - Error bound for the current model.
	 */
	public double getErrorBound() {
		return errorBound;
	}
	
	/**
	 * Accessor method for the learning rate.
	 * @return - Learning rate for the current model.
	 */
	public double getLearningRate() {
		return learningRate;
	}
	
	/**
	 * Accessor method for the feature types.
	 * @return - Feature types for the current model.
	 */
	public List<Integer> getFeatureTypes() {
		List<Integer> copy = new ArrayList<Integer>(featureTypes);
		return copy;
	}

}
