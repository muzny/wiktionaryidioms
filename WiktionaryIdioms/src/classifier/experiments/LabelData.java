/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * An experiment that labels data according to the precision boundary specified.
 * This means that we determine what percentage of results to classify as 1 by determining
 * what percentage of the "most 1" data points can be classified as 1 while maintaining a
 * precision above the specified level. Determines the percentage on the dev set, and does
 * the final reporting on the test set. Current configurations require mysql databases.
 */

package classifier.experiments;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;


import classifier.classifiers.Classifier;
import classifier.config.ClassifierConfigs;
import classifier.config.GeneralConfigs;
import classifier.model.ClassifierData;
import classifier.model.ClassifierEvaluationUtils;
import classifier.model.ClassifierModel;
import classifier.model.Result;
import classifier.utilities.ClassifierDataUtils;

public class LabelData {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassifierConfigs configs = ClassifierConfigs.getInstance();
		configs.setSection("LabelData");
		
		System.out.print("Loading data...");

		List<ClassifierData> data = null;
		
		String database = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.database");
		String table = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.table");
		String column = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.column");
		String where = " WHERE " + column + " = \"train\" ";
		data = ClassifierDataUtils.getSensesFromDb(
				database, table, where, "label");
		
		// Get the dev set
		where = " WHERE " + column + " = \"dev\"";
		
		data.addAll(ClassifierDataUtils.getSensesFromDb(
				database, table, where, "label"));
		
		// Get the test set
		where = " WHERE " + column + " = \"test\"";
		
		data.addAll(ClassifierDataUtils.getSensesFromDb(
				database, table, where, "label"));
		
		// Get the test set to produce the cutoff
		where = " WHERE " + column + " = \"test\" AND corrected_label != -1 ";
		List<ClassifierData> test = ClassifierDataUtils.getSensesFromDb(
				database, table, where, "corrected_label");
		
		System.out.println("We have " + data.size() + " senses");
		System.out.println(ClassifierDataUtils.getGeneralDataAnalysis(data));
		
		String modelFile = configs.getSString("modelFile");
		double precisionBoundary = configs.getSDouble("precisionBoundary");

		System.out.println("Loading from model file: " + modelFile);
		ClassifierModel	resultModel = new ClassifierModel(new File(modelFile));
		Classifier classy = configs.getClassifier(resultModel);
		
		List<Result> testDeltas = classy.testGetSortedDeltas(test);

		TreeMap<Integer, List<ClassifierData>> best = null;
		double bestCutoff = 0;
		for (double cutoff = .001; cutoff < 1; cutoff += .001) {
			TreeMap<Integer, List<ClassifierData>> classifications = 
					ExperimentResult.produceClassificationsFromCutoff(testDeltas, cutoff);
			double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(classifications, 1);
			if (pr[0] > precisionBoundary) {
				best = classifications;
				bestCutoff = cutoff;
			}
		}
		
		System.out.println();
		double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(best, 1);

		System.out.println("We end with [p, r]: " + Arrays.toString(pr));
		System.out.println("And F-measure: " + ClassifierEvaluationUtils.getFMeasure(best, 1));
		
		System.out.println();
		System.out.println("Labeling all data:");
		List<Result> dataDeltas = classy.testGetSortedDeltas(data);
		TreeMap<Integer, List<ClassifierData>> classifications = 
				ExperimentResult.produceClassificationsFromCutoff(dataDeltas, bestCutoff);
		int lits = 0;
		int ids = 0;
		for (int label: classifications.keySet()) {
			for (ClassifierData cd : classifications.get(label)) {
				if (label == 1 || cd.getLabel() == 1) {
					ids++;
				} else {
					lits++;
				}
			}
		}
		
		Set<String> litKeys = new HashSet<String>();
		Set<String> idKeys = new HashSet<String>();
		for (int label: classifications.keySet()) {
			for (ClassifierData cd : classifications.get(label)) {
				if (cd.getLabel() == 1) {
					idKeys.add(cd.getTitle());
				} else {
					litKeys.add(cd.getTitle());
				}
			}
		}
		
		System.out.println("entries with label 0: " + classifications.get(0).size());
		System.out.println("entries with label 1: " + classifications.get(1).size());

		//System.out.println("augmented entries with label 0: " + lits);
		//System.out.println("augmented entries with label 1: " + ids);
	}

}
