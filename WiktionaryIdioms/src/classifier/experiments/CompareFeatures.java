/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * An experiment that compares feature sets. This means that if you give it 
 * the features 1,2,3,5, it will test each of these features individually. If you
 * set buildUp to true, it will also test 1,2 1,2,3 and 1,2,3,5. If you set buildByBest
 * to true, it will build up to all features by adding the ones that performed the best
 * in first.
 */


package classifier.experiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classifier.classifiers.Classifier;
import classifier.config.ClassifierConfigs;
import classifier.model.ClassifierData;


public class CompareFeatures implements Experiment {
	public static final String CONFIG_STR = "CompareFeatures";

	@Override
	public ExperimentResult runExperiment(ClassifierConfigs configs, List<ClassifierData> train,
			List<ClassifierData> test) {
		configs.setSection(CONFIG_STR);
				
		boolean verbose = configs.getSBool(ClassifierConfigs.VERBOSE);
				
		System.out.println("verbose: " + verbose);
		
		double errorBound = configs.getSDouble(ClassifierConfigs.ERROR_BOUND);
		double learningDelta = configs.getSDouble("learningDelta");
		int iterationMax = configs.getSInt("iterationMax");
		
		System.out.println("learning delta: " + learningDelta);
		System.out.println("iteration max: " + iterationMax);
				
		List<List<Integer>> listOfFeatures = new ArrayList<List<Integer>>();
		String[] arr = configs.getSStringArray(ClassifierConfigs.FEATURES);
		// add each feature in its own list to the list of features
		for (String s : arr) {
			List<Integer> feat = new ArrayList<Integer>();
			feat.add(Integer.parseInt(s));
			listOfFeatures.add(feat);
			break;
		}

		if (configs.getSBool("buildUp")) {
			for (int i = 1; i < arr.length; i++) {
				List<Integer> feat = new ArrayList<Integer>();
				for (int j = 0; j <= i; j++) {
					feat.add(Integer.parseInt(arr[j]));
				}
				listOfFeatures.add(feat);
			}
		}
		
		boolean buildBest = configs.getSBool("buildByBest");
		
		System.out.println("Feature combos to be tested: " + listOfFeatures);
		System.out.println("Later, we will determine the build up by best: " + buildBest);
		System.out.println();

		ExperimentResult maxBest = null;
		String completeInfo = "features\trecall\tprecision\tfscore\titerations\tweights\n";
		String graphInfo = "features & recall & precision & fscore & iterations\n";
		
		Map<Integer, Double> bestSingles = new HashMap<Integer, Double>();
		while (true) {
			for (List<Integer> feats : listOfFeatures) {
				// set the features for the grid search
			
				Classifier classy = configs.getClassifier(0, errorBound, 0, feats);
				// Do a grid search for each feature setting
				List<ExperimentResult> result = 
						GridSearch.gridSearch(classy, learningDelta, iterationMax, train, test);
			
				ExperimentResult best = GridSearch.getBestResult(result);
			
				if (feats.size() == 1) {
					bestSingles.put(feats.get(0), best.getFScore());
				}
			
				if (maxBest == null || maxBest.getFScore() < best.getFScore()) {
					maxBest = best;
				}
			
				completeInfo += best.model.features + "\t";
				completeInfo += best.getRecall() + "\t";
				completeInfo += best.getPrecision() + "\t";
				completeInfo += best.getFScore() + "\t";
				completeInfo += best.model.numIterations + "\t";
				completeInfo += Arrays.toString(best.weights) + "\n";
				
				graphInfo += best.model.features + " & ";
				graphInfo += best.getRecall() + " & ";
				graphInfo += best.getPrecision() + " & ";
				graphInfo += best.getFScore() + " & ";
				graphInfo += best.model.numIterations + "\n";
				if (verbose) {
					System.out.println(best);
				}
			}
		
		
			if (buildBest) {
				// We're done with the singles, so add the builded ones to the end
				int[] ordered = getOrderedList(bestSingles);
				listOfFeatures.clear();
				for (int i = 1; i < ordered.length; i++) {
					List<Integer> feat = new ArrayList<Integer>();
					for (int j = 0; j <= i; j++) {
						feat.add(ordered[j]);
					}
					Collections.sort(feat);
					listOfFeatures.add(feat);
				}
				buildBest = false;
			} else {
				break;
			}
		}
		
		maxBest.addPrintInfo(completeInfo);
		maxBest.addGraphFormatInfo(graphInfo);

		return maxBest;
	}
	
	public int[] getOrderedList(Map<Integer, Double> vals) {
		int[] l = new int[vals.size()];
		int count = 0;
		for (int i : vals.keySet()) {
			l[count] = i;
			count++;
		}
		selectionSort(l, vals); 
		return l;
	}
	
	public static void selectionSort(int[] x, Map<Integer, Double> vals) {
	    for (int i=0; i<x.length-1; i++) {
	        for (int j=i+1; j<x.length; j++) {
	            if (vals.get(x[i]) < vals.get(x[j])) {
	                //... Exchange elements
	                int temp = x[i];
	                x[i] = x[j];
	                x[j] = temp;
	            }
	        }
	    }
	}
}
