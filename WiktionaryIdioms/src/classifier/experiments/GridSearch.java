/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * An experiment that does a grid search over numbers of
 * iterations that the classifier should perform. Determines the "best"
 * settings by choosing the ones that maximize FScore.
 */

package classifier.experiments;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import classifier.classifiers.Classifier;
import classifier.config.ClassifierConfigs;
import classifier.features.numeric.Feature;
import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.model.Result;
import classifier.model.Sense;

public class GridSearch implements Experiment {
	public static final String CONFIG_STR = "GridSearch";
	
	@Override
	public ExperimentResult runExperiment(ClassifierConfigs configs,
			List<ClassifierData> train, List<ClassifierData> test) {
		configs.setSection(CONFIG_STR);
		
		boolean verbose = configs.getSBool(ClassifierConfigs.VERBOSE);
		
		System.out.println("verbose: " + verbose);
		
		double errorBound = configs.getSDouble(ClassifierConfigs.ERROR_BOUND);
		double learningDelta = configs.getSDouble("learningDelta");
		int iterationMax = configs.getSInt("iterationMax");
		
		System.out.println("learning delta: " + learningDelta);
		System.out.println("iteration max: " + iterationMax);
		
		String group = configs.getSString(ClassifierConfigs.GROUP);
		List<Integer> featuresToUse = new ArrayList<Integer>();
		if (group.length() > 0) {
			System.out.println("Using group of features: " + group);
			if (group.equalsIgnoreCase("lexical")) {
				featuresToUse = CompareGroups.getList(Feature.LEXICAL);
			} else if (group.equalsIgnoreCase("graph")) {
				featuresToUse = CompareGroups.getList(Feature.GRAPH);
			} else if (group.equalsIgnoreCase("groups")) {
				for (int[] g : Feature.GROUPS) {
					for (int num : g) {
						featuresToUse.add(num);
					}
				}
			}
			
		} else {
			featuresToUse = configs.getSFeatures(ClassifierConfigs.FEATURES);
		}
		System.out.println("Testing features: " + featuresToUse);
		

		// The iterations and learning rate will get reset in the gridsearch
		Classifier percy = configs.getClassifier(0, errorBound, 0, featuresToUse);
		System.out.println(percy);
		
		List<ExperimentResult> result = 
				gridSearch(percy, learningDelta, iterationMax, train, test);
		
		ExperimentResult best = getBestResult(result);

		// Print out the results
		if (verbose) {
			printExperimentResults(result);
			System.out.println("Best: ");
			System.out.println(best);
		}
		return best;
	}
	
	public static ExperimentResult getBestResult(List<ExperimentResult> results) {
		ExperimentResult best = null;
		for (ExperimentResult cr : results) {
			if (!cr.isUnaryClassifier() && (best == null || 
					best.getFScore() < cr.getFScore())) {
				best = cr;
			}
		}
		if (best == null) {
			return results.get(0);
		}
		return best;
	}
	
	
	public static void printExperimentResults(List<ExperimentResult> result) {
		for (ExperimentResult cr : result) {
			System.out.println(cr);
		}
	}
	
	public static List<ExperimentResult> gridSearch(Classifier classy, double learningDelta, int itersMax,
			List<ClassifierData> train, List<ClassifierData> test) {

		// Do the grid search
		// we also want to find the best ones of these for the case in which the classifier
		// doesn't classify everything as all of one thing.
		List<ExperimentResult> crs = new ArrayList<ExperimentResult>();
		for (double learningRate = 1; learningRate > .1; learningRate -= learningDelta) {
			for (int iters = 1 ; iters <= itersMax; iters++) {
				
				classy.setIterations(iters);
				classy.setLearningRate(learningRate);
				
				ClassifierModel resultModel = classy.train(train);
				ExperimentResult result = new ExperimentResult(resultModel);
				
				TreeMap<Integer, List<ClassifierData>> overallResults = classy.test(test);
				List<Result> deltas = classy.testGetSortedDeltas(test);
				result.addResultSets(overallResults, deltas);
				result.setClassifierInfo(classy.toString());
				result.weights = classy.getWeights();
				
				crs.add(result);
			}
		}
		return crs;
	}
}
