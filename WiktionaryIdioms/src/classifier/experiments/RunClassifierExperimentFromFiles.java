/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The main class to run experiments directly from files (no databases).
 * 
 * Not all experiments have been tested with this experiment running framework.
 */

package classifier.experiments;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import classifier.config.ClassifierConfigs;
import classifier.model.ClassifierData;
import classifier.utilities.ClassifierDataUtils;


public class RunClassifierExperimentFromFiles {

	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("Usage: RunClassifierExperimentFromFiles <type> <config_path>");
			System.exit(1);
		}
		
		String configPath = args[1];
		String type = args[0];
		ClassifierConfigs configs = ClassifierConfigs.getInstance(configPath);
		String trainPath = configs.getSString(ClassifierConfigs.TRAIN_PATH);
		String testPath = configs.getSString(ClassifierConfigs.TEST_PATH);
		
		Experiment e = null;
		if (type.equals("basic")) {
			e = new BasicApply();
		} else if (type.equals("grid")) {
			e = new GridSearch();
		} else if (type.equals("compare")) {
			e = new CompareFeatures();
		} else if (type.equals("comparegroups")) {
			e = new CompareGroups();
		} else {
			System.err.println("Invalid experiment type, please choose \"basic\", \"grid\", "
					+ "\"compare\", \"comparegroups\", or \"bootstrapping\"");
			System.exit(1);
		}
		
		System.out.println("Using an experiment of type: " + e);
		System.out.println();
		
		
		String dataInfo = "";
		System.out.print("Loading data...");

		List<ClassifierData> trainData = null;
		List<ClassifierData> testData = null;

		System.out.print("train (" + trainPath + ")...");
		trainData = ClassifierDataUtils.getSensesFromFile(new File(trainPath));

		System.out.print("test (" + testPath + ")...");
		testData = ClassifierDataUtils.getSensesFromFile(new File(testPath));
		
		
		System.out.println("done.");
		System.out.println();
		
		dataInfo += "Number in train: " + trainData.size() + "\n";
		dataInfo += "Number in test: " + testData.size() + "\n";

		System.out.println("Number in train: " + trainData.size());
		System.out.println("Number in test: " + testData.size());
		
		boolean equalDist = false;
		String[] percents = configs.getSStringArray(ClassifierConfigs.PERCENTS);
		System.out.println("equalDist: " + equalDist);
		System.out.println("percents: " + Arrays.toString(percents));
		
		List<ClassifierData> trainMod = ((List<ClassifierData> ) ClassifierDataUtils.getData(equalDist, percents, trainData));
		String dataAnalysis = ClassifierDataUtils.getGeneralDataAnalysis(trainMod);
		dataInfo += dataAnalysis;
		String dataAnalysisTest = ClassifierDataUtils.getGeneralDataAnalysis(testData);
		dataInfo += dataAnalysisTest;
		System.out.println(dataAnalysisTest);
		
		ExperimentResult result = e.runExperiment(configs, trainMod, testData);
		
		System.out.println("Experimental result: ");
		System.out.println(result);
		
		if (configs.getSBool("shouldOutputGeneral")) {
			String outfile = configs.getSString(ClassifierConfigs.OUTFILE);

			System.out.print("Outputting to : " + outfile + " ... ");
		
			try {
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(outfile));
				bw.write("Data Info:\n" + dataInfo + "\n");
				bw.write("Classifier info:\n" + result.classifierInfo + "\n");
				bw.write("Classifier [p,r]:\n" + Arrays.toString(result.getPrecisionRecall()) + "\n");
				bw.write("Classifier fmeasure:\n" + result.getFScore() + "\n");

				bw.write(e.toString() + "\n\n");
				if (configs.getSBool("output.generalInfo")) {
					bw.write("\n");
					bw.write("General Info:\n");
					bw.write(result.getPrintInfo());
					
					bw.write("\n");
					bw.write("Graph Info:\n");
					bw.write(result.getGraphInfo());
				}
				if (configs.getSBool("output.confidenceList")) {
					bw.write("\n");
					bw.write("Confidence List:\n");
					bw.write(result.getConfidenceString());
				}
				if (configs.getSBool("output.precisionRecallPoints")) {
					bw.write("\n");
					bw.write("Precision/recall points:\n");
					bw.write(result.getTestCutoffsString(configs.getSDouble("precisionRecallGranularity")));
					
					bw.write("\n");
					bw.write("Precision/recall coords:\n");
					bw.write(result.getRecallPrecisionPoints(configs.getSDouble("precisionRecallGranularity")));
				}
				if (configs.getSInt("output.randomErrorAnalysis") != null) {
					int x = configs.getSInt("output.randomErrorAnalysis");
					bw.write("\n");
					bw.write("Random Errors:\n");
					bw.write(result.getRandomErrorString(x));
				}
				bw.close();
			} catch (IOException ioe) {
				System.out.println("Error with file: " + outfile);
			}
			System.out.println("done.");
		}
		
		if (configs.getSBool("shouldOutputModel")) {
			String[] outfilePieces = configs.getSString(ClassifierConfigs.OUTFILE).split("/");
			String outfile = outfilePieces[outfilePieces.length - 1];

			String modelOut = configs.getSString("outputModel.file") + outfile + ".model";

			System.out.print("Outputting to : " + modelOut + " ... ");
		
			try {
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(modelOut));
				bw.write(result.model.toString());
				bw.close();
			} catch (IOException ioe) {
				System.out.println("Error with file: " + modelOut);
			}
			System.out.println("done.");
		}
	}
}
