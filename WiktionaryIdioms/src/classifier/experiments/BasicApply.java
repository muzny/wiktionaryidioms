/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A basic experiment for the classifier. This means that it runs the classifier
 * once with the specified settings.
 */

package classifier.experiments;

import java.io.File;
import java.util.List;
import java.util.TreeMap;

import classifier.classifiers.Classifier;
import classifier.config.ClassifierConfigs;
import classifier.model.ClassifierData;
import classifier.model.ClassifierModel;
import classifier.model.Result;

public class BasicApply implements Experiment {
	public static final String CONFIG_STR = "BasicApply";  // specifies what section to look in in the config file

	@Override
	public ExperimentResult runExperiment(ClassifierConfigs configs, List<ClassifierData> train, List<ClassifierData> test) {
		configs.setSection(CONFIG_STR);
		
		boolean verbose = configs.getSBool(ClassifierConfigs.VERBOSE);
		
		System.out.println("verbose: " + verbose);
		
		String modelFile = configs.getSString("modelFile");
		System.out.println("Model file: " + modelFile);
		Classifier classy = null;
		ClassifierModel resultModel = null;
		if (modelFile != null && modelFile.length() > 0) {
			System.out.println("Loading from model file: " + modelFile);
			resultModel = new ClassifierModel(new File(modelFile));
			classy = configs.getClassifier(resultModel);
		} else {
			classy = configs.getSClassifier();
			resultModel = classy.train(train);
		}
		
		System.out.println(classy);

		TreeMap<Integer, List<ClassifierData>> results = classy.test(test);
		List<Result> deltas = classy.testGetSortedDeltas(test);
		
		ExperimentResult exResult = new ExperimentResult(resultModel);

		exResult.addResultSets(results, deltas);
		
		exResult.setClassifierInfo(classy.toString());
		
		exResult.weights = classy.getWeights();
		
		return exResult;
	}

}
