/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This is the file to run experiments that draw from databases.
 */

package classifier.experiments;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import classifier.config.ClassifierConfigs;
import classifier.config.GeneralConfigs;
import classifier.model.ClassifierData;
import classifier.utilities.ClassifierDataUtils;


public class RunExperiment {
	public static final String SENSE_TRAIN_WHERE = " WHERE data_set = \"train\" ";
	public static final String SENSE_DEV_WHERE = 
			" WHERE data_set=\"dev\" AND corrected_label IS NOT NULL " +
			"AND corrected_label != -1 AND specific_definition = \"\"";
	public static final String SENSE_TEST_WHERE = 
			" WHERE data_set=\"test\" AND corrected_label IS NOT NULL " +
			"AND corrected_label != -1 AND specific_definition = \"\"";


	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.err.println("Usage: RunExperiment <category> <type>");
			System.exit(1);
		}
		String category = args[0];
		if (!(category.equals("detect") || category.equals("classify"))) {
			System.err.println("Invalid category of experiment, please choose \"detect\" or \"classify\"");
		}
		
		String type = args[1];
		
		Experiment e = null;
		if (type.equals("basic")) {
			e = new BasicApply();
		} else if (type.equals("grid")) {
			e = new GridSearch();
		} else if (type.equals("compare")) {
			e = new CompareFeatures();
		} else if (type.equals("comparegroups")) {
			e = new CompareGroups();
		} else {
			System.err.println("Error in specifying experiment type, must be \"basic\"" +
					", \"grid\", \"compare\", or \"comparegroups\"");
		}
		
		System.out.println("Using an experiment of type: " + e);
		System.out.println();
		
		ClassifierConfigs configs = ClassifierConfigs.getInstance();
		
		String dataInfo = "";
		System.out.print("Loading data...");


		List<ClassifierData> trainData = null;
		List<ClassifierData> testData = null;
		boolean cleanTest = configs.getSBool("cleanTest");
		boolean testCorrectedLabel = configs.getSBool("testCorrectedLabel");


		if (category.equals("classify")) {
			String database = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.database");
			String table = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.table");
			String column = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.column");
			String testName = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "MySQL.classify.testData");
			String where = " WHERE " + column + " = \"train\" AND gloss != \"\" ";
			trainData = ClassifierDataUtils.getSensesFromDb(
					database, table, where, "label");
			String labelStr = "label";
			if (testCorrectedLabel) {
				labelStr = "corrected_label";
			}
			System.out.println("Using test label column: " + labelStr);
			where = " WHERE gloss != \"\" AND " + column + " = \"" + testName +
					"\" AND " + labelStr + " IS NOT NULL " +
					"AND " + labelStr + " != -1 ";
			
			if (cleanTest) {
				System.out.println("Omitting specifics from test data");
				where += "AND specific_definition = \"\"";
			}

			testData = ClassifierDataUtils.getSensesFromDb(
					database, table, where, labelStr);
		}
		
		
		System.out.println("done.");
		System.out.println();
		
		dataInfo += "Number in train: " + trainData.size() + "\n";
		dataInfo += "Number in test: " + testData.size() + "\n";

		System.out.println("Number in train: " + trainData.size());
		System.out.println("Number in test: " + testData.size());
		
		boolean equalDist = false;
		String[] percents = configs.getSStringArray(ClassifierConfigs.PERCENTS);
		System.out.println("equalDist: " + equalDist);
		System.out.println("percents: " + Arrays.toString(percents));
		
		List<ClassifierData> trainMod = ((List<ClassifierData> ) ClassifierDataUtils.getData(equalDist, percents, trainData));
		String dataAnalysis = ClassifierDataUtils.getGeneralDataAnalysis(trainMod);
		dataInfo += dataAnalysis;
		String dataAnalysisTest = ClassifierDataUtils.getGeneralDataAnalysis(testData);
		dataInfo += dataAnalysisTest;
		System.out.println(dataAnalysisTest);
		
		ExperimentResult result = e.runExperiment(configs, trainMod, testData);
		
		System.out.println("Experimental result: ");
		System.out.println(result);
		
		if (configs.getSBool("shouldOutputGeneral")) {
			String outfile = configs.getSString(ClassifierConfigs.OUTFILE);

			System.out.print("Outputting to : " + outfile + " ... ");
		
			try {
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(outfile));
				bw.write("Data Info:\n" + dataInfo + "\n");
				bw.write("Classifier info:\n" + result.classifierInfo + "\n");
				bw.write("Classifier [p,r]:\n" + Arrays.toString(result.getPrecisionRecall()) + "\n");
				bw.write("Classifier fmeasure:\n" + result.getFScore() + "\n");

				bw.write(e.toString() + "\n\n");
				if (configs.getSBool("output.generalInfo")) {
					bw.write("\n");
					bw.write("General Info:\n");
					bw.write(result.getPrintInfo());
					
					bw.write("\n");
					bw.write("Graph Info:\n");
					bw.write(result.getGraphInfo());
				}
				if (configs.getSBool("output.confidenceList")) {
					bw.write("\n");
					bw.write("Confidence List:\n");
					bw.write(result.getConfidenceString());
				}
				if (configs.getSBool("output.precisionRecallPoints")) {
					bw.write("\n");
					bw.write("Precision/recall points:\n");
					bw.write(result.getTestCutoffsString(configs.getSDouble("precisionRecallGranularity")));
					
					bw.write("\n");
					bw.write("Precision/recall coords:\n");
					bw.write(result.getRecallPrecisionPoints(configs.getSDouble("precisionRecallGranularity")));
				}
				if (configs.getSInt("output.randomErrorAnalysis") != null) {
					int x = configs.getSInt("output.randomErrorAnalysis");
					bw.write("\n");
					bw.write("Random Errors:\n");
					bw.write(result.getRandomErrorString(x));
				}
				bw.close();
			} catch (IOException ioe) {
				System.out.println("Error with file: " + outfile);
			}
			System.out.println("done.");
		}
		
		if (configs.getSBool("shouldOutputModel")) {
			String[] outfilePieces = configs.getSString(ClassifierConfigs.OUTFILE).split("/");
			String outfile = outfilePieces[outfilePieces.length - 1];

			String modelOut = configs.getSString("outputModel.file") + outfile + ".model";

			System.out.print("Outputting to : " + modelOut + " ... ");
		
			try {
				BufferedWriter bw = new BufferedWriter(
						new FileWriter(modelOut));
				bw.write(result.model.toString());
				bw.close();
			} catch (IOException ioe) {
				System.out.println("Error with file: " + modelOut);
			}
			System.out.println("done.");
		}
	}
}
