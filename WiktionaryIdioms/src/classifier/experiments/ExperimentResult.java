/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A wrapper for the results of an experiment so that all experiments
 * can return the same thing and fill in the fields as necessary for that specific
 * experiment.
 */

package classifier.experiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;


import classifier.model.ClassifierData;
import classifier.model.ClassifierEvaluationUtils;
import classifier.model.ClassifierModel;
import classifier.model.Result;

public class ExperimentResult implements Comparable<ExperimentResult>{
	public ClassifierModel model;
	public TreeMap<Integer, List<ClassifierData>> results;
	public List<Result> resultDeltas;
	public String generalInfo;
	public String graphInfo;
	public String classifierInfo;
	public double[] weights;
	public List<ClassifierData> unclassified;
	
	/**
	 * An empty ExperimentResult doesn't know anything 
	 * about what happened in the experiment, or about the classifier.
	 */
	public ExperimentResult() {
		results = new TreeMap<Integer, List<ClassifierData>>();
		generalInfo = "";
		classifierInfo = "";
		graphInfo = "";
		unclassified = new ArrayList<ClassifierData>();
	}
	
	/**
	 * Knows what model this ExperimentResult is associated with.
	 * @param clm
	 */
	public ExperimentResult(ClassifierModel clm) {
		this();
		model = clm;
	}
	
	/**
	 * Add one result to this experiment result. This means that you
	 * are adding one predicted classification associated with on ClassifierData.
	 * @param label - The label predicted by the classifier.
	 * @param cd - The ClassifierData that was classified.
	 */
	public void addResult(int label, ClassifierData cd) {
		if (!results.containsKey(label)) {
			results.put(label, new ArrayList<ClassifierData>());
		}
		results.get(label).add(cd);
	}
	
	/**
	 * Add sets of results to this ExperimentResult.
	 * @param generalResults - The map that corresponds predicted class to ClassifierDatas
	 * @param resultDeltas - The List of Results that contains information about
	 * how each ClassifierData was scored for each available class. 
	 */
	public void addResultSets(TreeMap<Integer, List<ClassifierData>> generalResults, List<Result> resultDeltas) {
		results = generalResults;
		this.resultDeltas = resultDeltas;
	}
	
	/**
	 * Get the FScore for this ExperimentResult, from the perspective of the idiom or the
	 * classification label of "1".
	 * @return - The double FScore (Harmonic mean of precision and recall)
	 */
	public double getFScore() {
		return ClassifierEvaluationUtils.getFMeasure(results, 1);
	}
	
	/**
	 * Get an array of doubles that contains [precision, recall] associated with the
	 * results in this ExperimentResult.
	 * @return - Double array of [precision, recall]
	 */
	public double[] getPrecisionRecall() {
		return ClassifierEvaluationUtils.getPrecisionRecall(results, 1);
	}
	
	/**
	 * Get the precision of this classifier.
	 * @return - The double precision.
	 */
	public double getPrecision() {
		return getPrecisionRecall()[0];
	}
	
	/**
	 * Get the recall of this classifier.
	 * @return - The double recall.
	 */
	public double getRecall() {
		return getPrecisionRecall()[1];
	}
	
	/**
	 * Returns true if the classifier has classified every data point
	 * as the same label.
	 * @return True if it classified everything as the same thing.
	 */
	public boolean isUnaryClassifier() {
		List<double[]> prList = new ArrayList<double[]>();
		prList.add(getPrecisionRecall());
		return ClassifierEvaluationUtils.getAllOneClassifier(prList);
	}
	
	/**
	 * Set some printable info (about the classifier) so that when an experiment is run,
	 * the important information is communicated. Does not print in default toString().
	 * @param info - String that the info of this ExperimentResult should be (does
	 * NOT append)
	 */
	public void setClassifierInfo(String info) {
		classifierInfo = info;
	}
	
	/**
	 * Add some info that is easily accessible to be printed later (does not print in default
	 * toString()).
	 * @param info - APPENDS info to the generalInfo field.
	 */
	public void addPrintInfo(String info) {
		this.generalInfo += info;
	}
	
	/**
	 * Add info about precision/recall in a format such that it is easy to put in spreadsheets.
	 * @param info
	 */
	public void addGraphFormatInfo(String info) {
		this.graphInfo = info;
	}
	
	public String getGraphInfo() {
		return graphInfo;
	}
	
	public String getPrintInfo() {
		return generalInfo;
	}
	
	/**
	 * The "golden measures" are the measures that you would get if you
	 * used your classifier but also counted the examples that are marked
	 * as label 1 as label 1 no matter what your classifier said they were.
	 * @return - double that is precision - your recall for this will always
	 * be 1.
	 */
	public double getGoldenMeasures() {
		// count the true positives
		int tP = 0;
		int fP = 0;
		for (ClassifierData cd : results.get(1)) {
			if (cd.getLabel() == 1) {
				tP++;
			} else {
				fP++;
			}
		}
		// also count the ones we missed
		for (ClassifierData cd : results.get(0)) {
			if (cd.getLabel() == 1) {
				tP++;
			}
		}
		
		return (1.0 * tP) / (tP + fP);
	}
	
	public String toString() {
		String s = "Features: " + model.features + "\n";
		s += "iterations: " + model.numIterations + "\n";
		s += "learning rate: " + model.learningRate + "\n";
		s += "error bound: " + model.errorBound + "\n";
		s += "weights: " + Arrays.toString(weights) + "\n";

		double[] pr = getPrecisionRecall();
		s += "p: " + pr[0] + "; ";
		s += "r: " + pr[1] + "; ";
		s += "f: " + ClassifierEvaluationUtils.getFMeasure(pr) + "\n";
		
		//s += "With golden labels:" + getGoldenMeasures() + "\n";
		
		s += getFScore() + "\n";
		return s;
	}
	
	/**
	 * Shows the difference between each ClassifierData being classified as class 1
	 * versus class 0.
	 * @return
	 */
	public String getConfidenceString() {
		String s = "";
		for (Result r : resultDeltas) {
			s += r.getDifference() + " = " + r.getClassifierData().getConfidenceString(model.features) + "\n";
		}
		return s;
	}
	
	
	
	/**
	 * Get x examples of errors, where each x was incorrectly classified as label.
	 * @param x
	 * @param label
	 * @return
	 */
	public String getRandomErrorString(int x, int label) {
		List<ClassifierData> target = results.get(label);
		
		List<ClassifierData> errors = new ArrayList<ClassifierData>();
		for (ClassifierData s : target) {
			if (s.getLabel() != label) {
				errors.add(s);
			}
		}
		
		Collections.shuffle(errors);
		
		String s = "Errors that were misclassified as: " + label + "\n";
		s += "This is a report of (" + x + "/" + errors.size() + ")";
		for (int i = 0; i < x && i < errors.size(); i++) {
			s += errors.get(i).getConfidenceString(model.features) + "\n";
		}
		return s;
	}
	
	/**
	 * Get x examples of errors, where each x was incorrectly classified.
	 * @param x
	 * @param label
	 * @return
	 */
	public String getRandomErrorString(int x) {
		
		
		List<Result> errors = new ArrayList<Result>();
		
		int litErrors = 0;
		int idiomErrors = 0;
		for (Result r : resultDeltas){
			if ((r.getDifference() < 0 && r.getClassifierData().getLabel() != 0)) {
				idiomErrors++;
				errors.add(r);
			}
			if ((r.getDifference() > 0 && r.getClassifierData().getLabel() != 1)) {
				litErrors++;
				errors.add(r);
			}
		}
		
		Collections.shuffle(errors);
		
		String s = "Errors that were misclassified \n";
		s += "This is a report of (" + x + "/" + errors.size() + ")\n";
		s += "There were " + idiomErrors + " miss-classified idioms and " + litErrors + " miss-classified literals \n";

		for (int i = 0; i < x && i < errors.size(); i++) {
			s += errors.get(i).getDifference() + " = " + 
					errors.get(i).getClassifierData().getConfidenceString(model.features) + "\n";
		}
		return s;
	}
	
	/**
	 * Returns a string in easily spreadsheet insertable format that
	 * contains the percent classified as label 1, the number labelled as 1
	 * at that percent, the recall, precision, and fscore
	 * @param granularity - the granularity to advance the percent by
	 * until it becomes 100% (<0,1]).
	 * @return - The String result.
	 */
	public String getTestCutoffsString(double granularity) {
		List<Double> percents = new ArrayList<Double>();
		List<Double> precisions = new ArrayList<Double>();
		List<Double> recalls = new ArrayList<Double>();
		List<Double> fscores = new ArrayList<Double>();
		List<Integer> nums = new ArrayList<Integer>();

		for (double i = granularity; i < 1; i += granularity) {
			TreeMap<Integer, List<ClassifierData>> total = produceClassificationsFromCutoff(resultDeltas, i);	
			percents.add(i);
			nums.add(total.get(1).size());
			double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(total, 1);
			precisions.add(pr[0]);
			recalls.add(pr[1]);
			fscores.add(ClassifierEvaluationUtils.getFMeasure(total, 1));
		}
		
		// do it for when the percent is 1.0
		TreeMap<Integer, List<ClassifierData>> total = produceClassificationsFromCutoff(resultDeltas, 1.0);	
		percents.add(1.0);
		nums.add(total.get(1).size());
		double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(total, 1);
		precisions.add(pr[0]);
		recalls.add(pr[1]);
		fscores.add(ClassifierEvaluationUtils.getFMeasure(total, 1));
		
		// Do the heading line
		String result = "percent\tnumber\trecall\tprecision\tfscore\n";
		for (int i = 0; i < percents.size(); i++) {
			result += percents.get(i) + "\t";
			result += nums.get(i) + "\t";
			result += recalls.get(i) + "\t";
			result += precisions.get(i) + "\t";
			result += fscores.get(i) + "\n";
		}
		
		return result;
	}
	
	public String getRecallPrecisionPoints(double granularity) {
		List<Double> precisions = new ArrayList<Double>();
		List<Double> recalls = new ArrayList<Double>();

		for (double i = granularity; i < 1; i += granularity) {
			TreeMap<Integer, List<ClassifierData>> total = produceClassificationsFromCutoff(resultDeltas, i);	
			double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(total, 1);
			precisions.add(pr[0]);
			recalls.add(pr[1]);
		}
		
		// do it for when the percent is 1.0
		TreeMap<Integer, List<ClassifierData>> total = produceClassificationsFromCutoff(resultDeltas, 1.0);	
		double[] pr = ClassifierEvaluationUtils.getPrecisionRecall(total, 1);
		precisions.add(pr[0]);
		recalls.add(pr[1]);
		
		// Do the heading line
		String result = "(recall, precision)\n";
		for (int i = 0; i < precisions.size(); i++) {
			result += "(" + recalls.get(i) + ", " + precisions.get(i) + ")\n";
		}
		
		return result;
	}
	
	public static TreeMap<Integer, List<ClassifierData>> produceClassificationsFromCutoff(List<Result> l, double cutoff) {
		// the last cutoff% of l we will classify as idiomatic
		int num = (int) Math.floor(l.size() * cutoff);
		TreeMap<Integer, List<ClassifierData>> results = new TreeMap<Integer, List<ClassifierData>>();
		results.put(0, new ArrayList<ClassifierData>());
		results.put(1, new ArrayList<ClassifierData>());
		// put the last num in the bucket for 1
		for (int i = l.size() - 1; i >= l.size() - num; i--) {
			results.get(1).add(l.get(i).getClassifierData());
		}
		// put the rest in the bucket for 0
		for (int i = l.size() - num - 1; i >= 0; i--) {
			results.get(0).add(l.get(i).getClassifierData());
		}
		return results;
	}

	
	@Override
	// return -num if other is > this
	// return +num if other is < this
	// return 0 if they are equal
	public int compareTo(ExperimentResult other) {
		return 0;
	}
}
