/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This experiment compares the LEXICAL versus GRAPH features (and any other group
 * that you define) in classifer.features.numeric.Feature.
 */

package classifier.experiments;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import classifier.classifiers.Classifier;
import classifier.config.ClassifierConfigs;
import classifier.features.numeric.Feature;
import classifier.model.ClassifierData;


public class CompareGroups implements Experiment {
	public static final String CONFIG_STR = "CompareGroups";

	@Override
	public ExperimentResult runExperiment(ClassifierConfigs configs, List<ClassifierData> train,
			List<ClassifierData> test) {
		configs.setSection(CONFIG_STR);
				
		boolean verbose = configs.getSBool(ClassifierConfigs.VERBOSE);
				
		System.out.println("verbose: " + verbose);
		
		double errorBound = configs.getSDouble(ClassifierConfigs.ERROR_BOUND);
		double learningDelta = configs.getSDouble("learningDelta");
		int iterationMax = configs.getSInt("iterationMax");
		
		System.out.println("learning delta: " + learningDelta);
		System.out.println("iteration max: " + iterationMax);
				
		List<List<Integer>> listOfFeatures = new ArrayList<List<Integer>>();
		
		System.out.println("Feature groups to be tested: ");
		listOfFeatures.add(getList(Feature.LEXICAL));
		listOfFeatures.add(getList(Feature.GRAPH));

		List<Integer> part12 = getList(Feature.LEXICAL);
		part12.addAll(getList(Feature.GRAPH));
		listOfFeatures.add(part12);

		
		System.out.println(listOfFeatures);

		ExperimentResult maxBest = null;
		String completeInfo = "features\trecall\tprecision\tfscore\titerations\tweights\n";
		String graphInfo = "features & recall & precision & fscore & iterations\n";
				
		for (List<Integer> feats : listOfFeatures) {
			// set the features for the grid search
			
			Classifier classy = configs.getClassifier(0, errorBound, 0, feats);
			// Do a grid search for each feature setting
			List<ExperimentResult> result = 
					GridSearch.gridSearch(classy, learningDelta, iterationMax, train, test);
			
			ExperimentResult best = GridSearch.getBestResult(result);
			
			if (maxBest == null || maxBest.getFScore() < best.getFScore()) {
				maxBest = best;
			}
			
			completeInfo += best.model.features + "\t";
			completeInfo += best.getRecall() + "\t";
			completeInfo += best.getPrecision() + "\t";
			completeInfo += best.getFScore() + "\t";
			completeInfo += best.model.numIterations + "\t";
			completeInfo += Arrays.toString(best.weights) + "\n";
				
			graphInfo += best.model.features + " & ";
			graphInfo += best.getRecall() + " & ";
			graphInfo += best.getPrecision() + " & ";
			graphInfo += best.getFScore() + " & ";
			graphInfo += best.model.numIterations + "\n";
			if (verbose) {
				System.out.println(best);
			}
		}
		
		maxBest.addPrintInfo(completeInfo);
		maxBest.addGraphFormatInfo(graphInfo);

		return maxBest;
	}
	
	public static List<Integer> getList(int[] input) {
		List<Integer> l = new ArrayList<Integer>();
		for (int i : input) {
			l.add(i);
		}
		return l;
	}
}
