/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.config;
/**
 * Basic config wrapper that reads the value for a 
 * field from a specified config file.
 */

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

public class GeneralConfigs {
	public static final String ESA_CONFIGS = "./config/esaconfig.xml";
	public static final String DB_CONFIGS = "./config/dbconfig.xml";
	public static final String CLASSIFIER_CONFIGS = "./config/classifierconfig.xml";
	public static final String FEATURE_CONFIGS = "./config/featureconfig.xml";
	public static final String DETECTOR_CONFIGS = "./config/detectorconfig.xml";

	
	private static Map<String, XMLConfiguration> xmlInsts;
	
	private static void initialize(String path) {
		if (xmlInsts == null) {
			xmlInsts = new HashMap<String, XMLConfiguration>();
		}
		if (!xmlInsts.containsKey(path)) {
			try {
				xmlInsts.put(path, new XMLConfiguration(path));
			} catch (ConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Read and return the value at field name from file at path as
	 * a String.
	 * @param path - Location of config file to read from.
	 * @param name - The name of the field to get the value from.
	 * @return String value at name in file at path.
	 */
	public static String getString(String path, String name) {
		initialize(path);
		return xmlInsts.get(path).getString(name);
	}
	
}
