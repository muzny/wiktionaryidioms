/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * A configuration class specifically designed to live in harmony with the
 * classifier and read the corresponding .xml files in a way that makes everyone happy.
 */

package classifier.config;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.XMLConfiguration;

import classifier.classifiers.Classifier;
import classifier.classifiers.Perceptron;
import classifier.classifiers.TrustingPerceptron;
import classifier.model.ClassifierModel;

public class ClassifierConfigs {

	
	public static final String DEFAULT = "default";
	public static final String EQUAL_DIST = "equalDist";
	public static final String PERCENTS = "percents";
	public static final String FOLDS = "folds";
	public static final String LEARNING_RATE = "learningRate";
	public static final String ITERATIONS = "iterations";
	public static final String VERBOSE = "verbose";
	public static final String FEATURES = "features";
	public static final String ERROR_BOUND = "errorBound";
	public static final String STRATIFIED = "stratified";
	public static final String SENSES_PATH = "trainPath";
	public static final String TRAIN_PATH = "trainPath";
	public static final String TEST_PATH = "testPath";
	public static final String CLASSIFIER_TYPE = "classifierType";
	public static final String OUTFILE = "output.file";
	public static final String GROUP = "group";

	
	private static ClassifierConfigs instance;
	
	private XMLConfiguration xmlInst;
	
	private String section;
	
	private ClassifierConfigs() {
		try {
			xmlInst = new XMLConfiguration(GeneralConfigs.CLASSIFIER_CONFIGS);
			section = DEFAULT;
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private ClassifierConfigs(String path) {
		try {
			xmlInst = new XMLConfiguration(path);
			section = DEFAULT;
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Get an instance of ClassifierConfigs, using GeneralConfigs.CLASSIFIER_CONFIGS
	 * as the default path.
	 * @return a ClassifierConfigs object that reads from the config file located at
	 * GeneralConfigs.CLASSIFIER_CONFIGS
	 */
	public static ClassifierConfigs getInstance() {
		if (instance == null) {
			instance = new ClassifierConfigs();
		}
		return instance;
	}
	
	
	/**
	 * Get an instance of ClassifierConfigs, from the specified path.
	 * @param path - The path to the config file.
	 * @return a ClassifierConfigs object that reads from the config file located at path.
	 */
	public static ClassifierConfigs getInstance(String path) {
		if (instance == null) {
			instance = new ClassifierConfigs(path);
		}
		return instance;
	}
	
	/**
	 * Get the string associated with the field specified in the section that you
	 * are in, defaulting to the default section.
	 * @param name - field name you are interested in
	 * @return The String that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public String getSString(String name) {
		String result = xmlInst.getString(section + "." + name);
		if (result == null || result.length() == 0) {
			result = xmlInst.getString(DEFAULT + "." + name);
		}
		return result;
	}
	
	/**
	 * Get the string array associated with the field specified in the section that you
	 * are in, defaulting to the default section.
	 * @param name - field name you are interested in
	 * @return The String array that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public String[] getSStringArray(String name) {
		String[] result = xmlInst.getStringArray(section + "." + name);
		if (result == null || result.length == 0) {
			result = xmlInst.getStringArray(DEFAULT + "." + name);
		}
		return result;
	}
	
	
	/**
	 * Get the int associated with the field specified in the section that you
	 * are in, defaulting to the default section.
	 * @param name - field name you are interested in
	 * @return The int that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public Integer getSInt(String name) {
		int result;
		try {
			result = xmlInst.getInt(section + "." + name);
		} catch (Exception e) {
			try {
				result = xmlInst.getInt(DEFAULT + "." + name);
			} catch (Exception f) {
				return null;
			}
		}
		return result;
	}
	
	
	/**
	 * Get the boolean associated with the field specified in the section that you
	 * are in, defaulting to the default section.
	 * @param name - field name you are interested in
	 * @return The boolean that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public boolean getSBool(String name) {
		boolean result;
		try {
			result = xmlInst.getBoolean(section + "." + name);
		} catch (NoSuchElementException e) {
			result = xmlInst.getBoolean(DEFAULT + "." + name, false);
		}
		return result;
	}
	
	/**
	 * Get the double associated with the field specified in the section that you
	 * are in, defaulting to the default section.
	 * @param name - field name you are interested in
	 * @return The double that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public double getSDouble(String name) {
		double result;
		try {
			result = xmlInst.getDouble(section + "." + name);
		} catch (NoSuchElementException e) {
			result = xmlInst.getDouble(DEFAULT + "." + name);
		}
		return result;
	}
	
	/**
	 * Set the specific section to look in before defaulting to the default section.
	 * @param name - The name of the section in the xml config file.
	 */
	public void setSection(String name) {
		section = name;
	}
	
	/**
	 * Get the list of ints associated with the field specified in the section that you
	 * are in, defaulting to the default section. Specifically designed to get the
	 * list of features that you want to use.
	 * @param name - field name you are interested in
	 * @return The list of ints that corresponds to field, first from the section you
	 * are in, then from the default section.
	 */
	public List<Integer> getSFeatures(String name) {
		// Now analyze which features we should be using
		String[] feats = getSStringArray(name);
		List<Integer> featuresToUse = new ArrayList<Integer>();
		for (String num : feats) {
			featuresToUse.add(Integer.parseInt(num));
		}
		return featuresToUse;
	}
	
	/**
	 * Loads the classifier that is specified in the config file, meaning that it
	 * loads all section fields associated with iterations, error bound, learning rate,
	 * and features, as specified by the constants in this file. Calls the methods
	 * that first read from the specific section, then default to the default section.
	 * Reads the type from the config file too - "trusting" or "perceptron".
	 * @return - The Classifier that this section of the config file specifies.
	 */
	public Classifier getSClassifier() {		
		int iters = getSInt(ITERATIONS);
		double errorBound = getSDouble(ERROR_BOUND);
		double learningRate = getSInt(LEARNING_RATE);
		List<Integer> feats = getSFeatures(FEATURES);

		return getClassifier(iters, errorBound, learningRate, feats);
	}
	
	/**
	 * Gets the classifier specified by the parameters, reading the type that it ought to
	 * be from the config file. "trusting" maps to a TrustingPerception, "perceptron" maps
	 * to a Perceptron.
	 * @param iters - numIterations for the Classifier constructor
	 * @param errorBound - as in the Classifier constructor
	 * @param learningRate - as in the Classifier constructor
	 * @param feats -  as in the Classifier constructor
	 * @return Your newly constructed classifier of the type specified in the config file.
	 */
	public Classifier getClassifier(int iters, double errorBound, double learningRate, List<Integer> feats) {
		String type = getSString(CLASSIFIER_TYPE);
		if (type.equals("trusting")) {
			return new TrustingPerceptron(iters, errorBound, learningRate, feats);
		} else if (type.equals("perceptron")) {
			// type.equals("perceptron")
			return new Perceptron(iters, errorBound, learningRate, feats);
		}
		return null;
	}
	
	/**
	 * Takes in a classifier model and returns the proper classifier according to the type
	 * specified in the config file - "trusting" or "perceptron".
	 * @param clm - ClassifierModel to build the classifier from.
	 * @return Your newly constructed classifier according to the given model and the
	 * type denoted in the config file.
	 */
	public Classifier getClassifier(ClassifierModel clm) {
		String type = getSString(CLASSIFIER_TYPE);
		if (type.equals("untrusting")) {
			return new TrustingPerceptron(clm);
		} else if (type.equals("perceptron")) {
			return new Perceptron(clm);
		}
		return null;
	}

}
