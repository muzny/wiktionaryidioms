/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * Currently the way to model Wiktionary entries upon initial entry into
 * the database. Should probably be redesigned.
 */

package classifier.model;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;


public class DataPoint {
	private static List<Integer> classes;
	
	private String title;
	private long pageId;
	private int label;
	private TreeMap<Integer, Double> features;
	
	/**
	 * Creates a new DataPoint.
	 * @param pageId
	 * @param title
	 * @param label
	 */
	public DataPoint(long pageId, String title, int label) {
		if (classes == null) {
			classes = new ArrayList<Integer>();
		}
		if (!classes.contains(label)) {
			classes.add(label);
		}
		this.pageId = pageId;
		this.title = title;
		this.label = label;
		this.features = new TreeMap<Integer, Double>();
	}
	
	public boolean addFeature(int key, double value) {
		if ((features.size() == 0 && key != 0) || 
				(features.size() > 0 && key != features.lastKey() + 1)) {
			return false;
		}
		features.put(key, value);
		
		return true;
	}
	
	public long getId() {
		return this.pageId;
	}
	
	public int getLabel() {
		return this.label;
	}

	public String getTitle() {
		return this.title;
	}
	
	
	public double getFeature(int key) {
		return features.get(key);
	}
	
	public String getFeatureString() {
		String s = "This is defunct";
		/*for (int key : features.keySet()) {
			s += key + ":" + Feature.FEATURE_INDEXES.get(key) + "\n";
		}*/
		return s;
	}
	
	public int numFeatures() {
		return features.size();
	}
	
	public double[] getFeatures() {
		// flatten the feature structure.
		double[] feats = new double[features.size()];			
		for (int i = 0; i < feats.length; i++) {
			feats[i] = this.getFeature(i);
		}
		return feats;
	}
	
	public String getDataString() {
		String s = pageId + " || " + title + " || " + label + " || ";
		for (int key : features.keySet()) {
			s += key + ":" + features.get(key);
			s += " ";
		}
		return s;
	}
	
	public static double[] phi(DataPoint point, int label) {
		int labelIndex = classes.indexOf(label);
		int numFeats = point.numFeatures();
		double[] feats = new double[numFeats * classes.size()];
		for (int i = 0; i < feats.length; i++) {
			if ( i / numFeats == labelIndex) {
				feats[i] = point.getFeature(i % numFeats);
			}
		}
		return feats;
	}
	
	public static List<Integer> getClasses() {
		return classes;
	}
}
