/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.model;

import java.util.List;

import classifier.features.numeric.Feature;


public class Sense extends ClassifierData {
	public static final String DELIM = "||||";

	private String gloss;
	private String uncleaned;
	public String comments;
		
	
	/**
	 * Create a new sense to represent a single <phrase, definition> pair.
	 * @param senseKey - the sense key associated with this sense in wkt
	 * @param title - the phrase/title it is associated with
	 * @param gloss - definition w/o wiktionary markup
	 * @param uncleaned - definition w/ wiktionary markup
	 * @param label - classification
	 */
	public Sense (String senseKey, String title, String gloss, String uncleaned, int label) {
		super(senseKey, title, label);
		
		this.gloss = gloss;
		this.uncleaned = uncleaned;
	}
	
	public String getAnnotationString() {
		/*sense key: 2972726:0:1
title: tu-whit tu-whoo
gloss: The cry of an owl
uncleaned gloss: The cry of an [[owl]]
label: 0
corrected label: 0
comments: */
		String s = "sense key: " + senseKey + "\n";
		s += "title: " + title + "\n";
		s += "gloss: " + gloss + "\n";
		s += "uncleaned: " + uncleaned + "\n";
		s += "label: " + label + "\n";

		return s;
	}
	
	public String getGloss() {
		return gloss;
	}
	
	public String getUncleanedGloss() {
		return uncleaned;
	}
	
	public String getFeatureString() {
		String s = "";
		for (int key : features.keySet()) {
			s += key + ":" + Feature.FEATURE_INDEXES.get(key) + "\n";
		}
		return s;
	}
	
	public String getDataString() {
		String s = getInitialStr();
		for (int key : features.keySet()) {
			s += key + ":" + features.get(key);
			s += " ";
		}
		return s;
	}
	
	private String getInitialStr() {
		String s = senseKey + " " + DELIM + " " + title + " " + DELIM + " " + gloss + " " + DELIM + " " 
				+ uncleaned + " " + DELIM + " " + label + " " + DELIM + " ";
		return s;
	}
	
	public void printPrettyString(List<Integer> featuresToUse) {
		System.out.printf("%-12s %s %-50s %s %-50s %s %-50s %s %d %s", 
				senseKey, DELIM, title, DELIM, gloss, DELIM, uncleaned, DELIM, label, DELIM);
		for (int key : featuresToUse) {
			System.out.printf(" %d:%f ", key, features.get(key));
		}
		System.out.println();
	}
	
	public String getPrettyString(List<Integer> featuresToUse) {
		String s = getInitialStr();
		for (int key : featuresToUse) {
			s += key + ":" + features.get(key);
			s += " ";
		}
		return s;
	}
	
	public String getConfidenceString(List<Integer> featuresToUse) {
		String s = label + "--" + senseKey + " " + DELIM + " " + title + 
				" " + DELIM + " " + gloss + " " + DELIM + " " + uncleaned + " " + DELIM + " ";		
		for (int key : featuresToUse) {
			s += key + ":" + features.get(key);
			s += " ";
		}
		return s;
	}
	
	public void printPrettyString() {
		System.out.printf("%-12s %s %-50s %s %-50s %s %-50s %s %d %s", 
				senseKey, DELIM, title, DELIM, gloss, DELIM, uncleaned, DELIM, label, DELIM);
		for (int key : features.keySet()) {
			System.out.printf(" %d:%f ", key, features.get(key));
		}
		System.out.println();
	}
	
	public String toString() {
		return getDataString();
	}
	
	public boolean equals(Object o) {
		if (o instanceof Sense) {
			Sense other = (Sense) o;
			return other.getKey().equals(this.getKey());
		}
		return false;
	}
	
	public int hashCode() {
		return senseKey.hashCode();
	}

	@Override
	public String getDetectionString() {
		// TODO Auto-generated method stub
		return null;
	}
}
