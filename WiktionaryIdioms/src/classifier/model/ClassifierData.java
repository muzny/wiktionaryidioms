/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This is the Classifier Data. A Classifier Data is a generalizable wrapper for
 * use with a classifier, such as a perceptron.
 */

package classifier.model;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public abstract class ClassifierData {
	protected static List<Integer> classes;
	
	protected String title;
	protected String senseKey;	
	protected int label;
	protected String parse;
	
	protected TreeMap<Integer, Double> features;
	
	public ClassifierData(String senseKey, String title, int label) {
		this.senseKey = senseKey;
		this.title = title;
		this.label = label;
		
		this.features = new TreeMap<Integer, Double>();
		
		if (classes == null) {
			classes = new ArrayList<Integer>();
		}
		if (!classes.contains(label)) {
			classes.add(label);
		}
	}
	
	public String getParse() {
		return parse;
	}
	
	public void setParse(String parse) {
		this.parse = parse;
	}
	
	public boolean addFeature(int key, double value) {
		features.put(key, value);
		return true;
	}
	
	public String getKey() {
		return senseKey;
	}
	
	public int getLabel() {
		return this.label;
	}
	
	public void setLabel(int newLabel) {
		this.label = newLabel;
	}

	public String getTitle() {
		return this.title;
	}
	
	public double getFeature(int key) {
		return features.get(key);
	}
	
	public static double[] phi(ClassifierData point, int label, List<Integer> featureTypes) {
		int labelIndex = classes.indexOf(label);
		int numFeats = featureTypes.size() + 1;  // add 1 for the bias feature
		// The bias feature will go at the end of the array
		double[] feats = new double[numFeats * classes.size()];
		for (int i = 0; i < feats.length; i++) {
			if ( i / numFeats == labelIndex) {
				int offset = i % numFeats;
				if (offset == featureTypes.size()) {
					// Set the bias feature
					feats[i] = 1;
				} else {
					int featureNumber = featureTypes.get(offset);
					double realValue = point.getFeature(featureNumber);
					feats[i] = realValue;
				}
			}
		}
		return feats;
	}
	
	public static List<Integer> getClasses() {
		return classes;
	}
	
	public abstract void printPrettyString(List<Integer> featuresToUse);
	
	public abstract String getConfidenceString(List<Integer> featuresToUse);
	
	public abstract String getDetectionString();
}
