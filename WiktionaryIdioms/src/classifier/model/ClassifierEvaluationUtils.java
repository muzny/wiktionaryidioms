/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class ClassifierEvaluationUtils {
	public static double getFMeasure(double[] pr) {
		if (pr[0] + pr[1] == 0) {
			return 0;
		}
		return (2.0 * ((pr[0] * pr[1]) / (pr[0] + pr[1])));
	}
	
	public static double getFMeasure(TreeMap<Integer, List<ClassifierData>> data, int label) {
		double[] pr = getPrecisionRecall(data, label);
		return getFMeasure(pr);
	}
	
	
	public static double[] getPrecisionRecall(TreeMap<Integer, List<ClassifierData>> data, int label) {
		double[] pr = new double[2];
		
		Map<Integer, Integer> classIndexes = new HashMap<Integer, Integer>();
		int index = 0;
		for (int l : data.keySet()) {
			classIndexes.put(l, index);
			index++;
		}
		
		int[][] confMatrix = getConfusionMatrix(data, classIndexes);
		
		// Now analyze the precision and recall.
		// On the diagonal.
		int truePositive = confMatrix[classIndexes.get(label)][classIndexes.get(label)];
			
		// The sum of the column associated with this label
		int falsePositive = 0;
		for (int i = 0; i < confMatrix.length; i++) {
			if (i != classIndexes.get(label)) {
				falsePositive += confMatrix[i][classIndexes.get(label)];
			}
		}
			
		// The sum of the row associated with this label
		int falseNegative = 0;
		for (int i = 0; i < confMatrix[classIndexes.get(label)].length; i++) {
			if (i != classIndexes.get(label)) {
				falseNegative += confMatrix[classIndexes.get(label)][i];
			}
		}
			
		//precision
		pr[0] = (1.0 * truePositive) / (truePositive + falsePositive);
		//recall
		pr[1] = (1.0 * truePositive) / (truePositive + falseNegative);
		
		if ((truePositive + falsePositive) == 0) {
			pr[0] = 0;
		}
		if ((truePositive + falseNegative) == 0) {
			pr[1] = 0;
		}
		
		return pr;
	}
	
	// Method considers recall of 1 wrong
	public static boolean getAllOneClassifier(List<double[]> prs) {
		for (double[] pr : prs) {
			if (pr[1] == 1 || pr[1] == 0) {
				return true;
			}
		}
		return false;
	}
	
	public static int[][] getConfusionMatrix(TreeMap<Integer, List<ClassifierData>> data, Map<Integer, Integer> classIndexes) {
		int[][] confMatrix = new int[data.size()][data.size()];

		// Fill in the confusion matrix
		for (int l : data.keySet()) {
			List<ClassifierData> ps = data.get(l);
			for (int i = 0; i < ps.size(); i++) {
				int real = ps.get(i).getLabel();
				// label is what the perceptron guessed.
				confMatrix[classIndexes.get(real)][classIndexes.get(l)] += 1;
			}
		}
		return confMatrix;
	}
	
	public static void printSummary(TreeMap<Integer, List<ClassifierData>> data) {
		int[][] confMatrix = new int[data.size()][data.size()];

		System.out.println("++++++++++++++++++++++++++");
		System.out.println("SUMMARY: \n");
		Map<Integer, Integer> classIndexes = new HashMap<Integer, Integer>();
		int index = 0;
		for (int label : data.keySet()) {
			classIndexes.put(label, index);
			index++;
		}
		
		for (int label : data.keySet()) {
			List<ClassifierData> ps = data.get(label);
			for (int i = 0; i < ps.size(); i++) {
				int real = ps.get(i).getLabel();
				// label is what the perceptron guessed.
				confMatrix[classIndexes.get(real)][classIndexes.get(label)] += 1;
			}
		}

		// Print the results
		System.out.println("Confusion matrix: ");
		for (int[] row : confMatrix) {
			System.out.println(Arrays.toString(row));
		}
		
		// Now analyze the precision and recall.
		for (int label: classIndexes.keySet()) {
			// On the diagonal.
			int truePositive = confMatrix[classIndexes.get(label)][classIndexes.get(label)];
			
			// The sum of the column associated with this label
			int falsePositive = 0;
			for (int i = 0; i < confMatrix.length; i++) {
				if (i != classIndexes.get(label)) {
					falsePositive += confMatrix[i][classIndexes.get(label)];
				}
			}
			
			// The sum of the row associated with this label
			int falseNegative = 0;
			for (int i = 0; i < confMatrix[classIndexes.get(label)].length; i++) {
				if (i != classIndexes.get(label)) {
					falseNegative += confMatrix[classIndexes.get(label)][i];
				}
			}
			
			double[] pr = getPrecisionRecall(data, label);
			System.out.println("\nFor class: " + label);
			System.out.println("Precision: (" + truePositive + ") / (" + truePositive + " + " + falsePositive + ")");
			System.out.println("\t: " + pr[0]);
			System.out.println("Recall: (" + truePositive + ") / (" + truePositive + " + " + falseNegative + ")");
			System.out.println("\t: " + pr[1]);
		}
	}
}
