/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import classifier.classifiers.Classifier;

public class ClassifierModel {
	public int numIterations;
	public double errorBound;
	public double learningRate;
	public List<Integer> features;
	public double[] weights;
	
	public ClassifierModel(Classifier cl) {
		numIterations = cl.getIterations();
		errorBound = cl.getErrorBound();
		learningRate = cl.getLearningRate();
		features = cl.getFeatureTypes();
		weights = cl.getWeights();
	}
	
	public ClassifierModel(File f) {
		try {
			Scanner scan = new Scanner(f);
			// First line should be iterations
			numIterations = Integer.parseInt(scan.nextLine().split(":")[1].trim());
			// Error Bound
			errorBound = Double.parseDouble(scan.nextLine().split(":")[1].trim());
			// Learning rate 
			learningRate = Double.parseDouble(scan.nextLine().split(":")[1].trim());
			// Features
			features = new ArrayList<Integer>();
			String feats = scan.nextLine().split(":")[1].trim();
			String[] featArr = feats.substring(1,feats.length() - 1).split(",");
			for (String s : featArr) {
				features.add(Integer.parseInt(s.trim()));
			}
			// Last, weights!
			String weightStr = scan.nextLine().split(":")[1].trim();
			String[] weightArr = weightStr.substring(1,weightStr.length() - 1).split(",");
			weights = new double[weightArr.length];
			for (int i = 0; i < weightArr.length; i++) {
				weights[i] = Double.parseDouble(weightArr[i].trim());
			}
			
		} catch (FileNotFoundException e) {
			System.err.println("File not found: " + f);
		}
	}
	
	public String toString(){
		String s = "Iterations: " + numIterations + "\n";
		s += "Error bound: " + errorBound + "\n";
		s += "Learning rate: " + learningRate + "\n";
		s += "Features : " + features + "\n";
		s += "Weights: " + Arrays.toString(weights);
		return s;
	}
}
