/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.model;

import java.util.HashMap;
import java.util.Map;

public class Result implements Comparable<Result> {
	private ClassifierData data;
	private double litScore;
	private double idiomScore;
	private Map<Integer, Double> scores;
	
	public Result(ClassifierData s, double litScore, double idiomScore) {
		data = s;
		this.litScore = litScore;
		this.idiomScore = idiomScore;
	}
	
	public Result(ClassifierData s) {
		data = s;
		scores = new HashMap<Integer, Double>();
	}

	public void addPrediction(int label, double score) {
		scores.put(label, score);
	}
	
	public double getDifference() {
		return this.idiomScore - this.litScore;
	}
	
	public ClassifierData getClassifierData() {
		return data;
	}
	
	@Override
	public int compareTo(Result other) {
		return (int) Math.signum(this.getDifference() - other.getDifference());
	}
	
	public String toString() {
		return "l: " + data.getLabel() + " d:" + getDifference();
	}
	
	public static double getValuePrecisionAbove() {
		return 0.0;
	}
		
}
