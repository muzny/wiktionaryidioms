/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

import mysql.utilities.MySQLConnection;

import classifier.features.numeric.Feature;
import classifier.model.ClassifierData;
import classifier.model.Result;
import classifier.model.Sense;



public class ClassifierDataUtils {
	public static final int STEP_SIZE = 100000;
	
	public static List<ClassifierData> getSensesFromDbBasedOnExamples(
			String database, String table, List<ClassifierData> examples) {
		
		int min = 0;
		int stepSize = 100000;
		
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add("gloss");
		whatToSelect.add("uncleaned_gloss");
		whatToSelect.add("label");


		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			whatToSelect.add(Feature.FEATURE_INDEXES.get(key).getDBName());
		}
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");

		List<ClassifierData> senses = new ArrayList<ClassifierData>();
		
		// 2. Build where clause
		String whereClause = " WHERE ";
		for (ClassifierData cd : examples) {
			whereClause += "sense_key = \"" + cd.getKey() + "\" OR ";
		}
		// remove the last "OR"
		whereClause = whereClause.substring(0, whereClause.length() - 3);
		
		// 3. fill with data
		int current = min;
		while (true) {
					
			// Go and get the data.
			String finalWhereClause = whereClause + " LIMIT " + current + 
					", " + stepSize;
			current += stepSize;

			List<String[]> results = moby.selectQuery(whatToSelect, table, finalWhereClause);
						
			for (String[] result: results) {
				String senseKey = result[whatToSelect.indexOf("sense_key")];

				String title = result[whatToSelect.indexOf("title")];
				String gloss = result[whatToSelect.indexOf("gloss")];
				String uncleaned = result[whatToSelect.indexOf("uncleaned_gloss")];
				int label = Integer.parseInt(result[whatToSelect.indexOf("label")]);

				
				Sense s = new Sense(senseKey, title, gloss, uncleaned, label);
				
				for (int key : Feature.FEATURE_INDEXES.keySet()) {
					String value = result[whatToSelect.indexOf(Feature.FEATURE_INDEXES.get(key).getDBName())];
					s.addFeature(key, Double.parseDouble(value));
				}
				senses.add(s);
				
			}	
			if (results.size() < stepSize) {
				break;  // We're done.
			}
		}
		
		// 4. Only get the ones that have examples
		return senses;
	}

	public static List<ClassifierData> getSensesFromDb(
			String database, String table, String whereClause, 
			String label) {
		
		int min = 0;
		int stepSize = 100000;
		
		List<String> whatToSelect = new ArrayList<String>();
		whatToSelect.add("sense_key");
		whatToSelect.add("title");
		whatToSelect.add(label);
		whatToSelect.add("gloss");
		whatToSelect.add("uncleaned_gloss");

		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			whatToSelect.add(Feature.FEATURE_INDEXES.get(key).getDBName());
		}
		
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");

		List<ClassifierData> exs = new ArrayList<ClassifierData>();
		
		// 3. fill with data
		int current = min;
		while (true) {
					
			// Go and get the data.
			String finalWhereClause = whereClause + " LIMIT " + current + 
					", " + stepSize;
			current += stepSize;

			List<String[]> results = moby.selectQuery(whatToSelect, table, finalWhereClause);
						
			for (String[] result: results) {
				String senseKey = result[whatToSelect.indexOf("sense_key")];

				String title = result[whatToSelect.indexOf("title")];
				int sLabel = Integer.parseInt(result[whatToSelect.indexOf(label)]);
				String gloss = result[whatToSelect.indexOf("gloss")];
				String uncleaned = result[whatToSelect.indexOf("uncleaned_gloss")];
				
				Sense s = new Sense(senseKey, title, gloss, uncleaned, sLabel);
				
				for (int key : Feature.FEATURE_INDEXES.keySet()) {
					String value = result[whatToSelect.indexOf(Feature.FEATURE_INDEXES.get(key).getDBName())];
					s.addFeature(key, Double.parseDouble(value));
				}
				exs.add(s);
				
			}	
			if (results.size() < stepSize) {
				break;  // We're done.
			}
		}
		
		// 4. Only get the ones that have examples
		return exs;
	}
	
	/**
	 * Get all senses from the table associated with the title.
	 * @param title
	 * @param conn
	 * @param table
	 * @return List<Sense> of the senses associated with the title.
	 */
	public static List<ClassifierData> getSensesWithTitle(String title, MySQLConnection conn, String table) {
		List<String> feats = new ArrayList<String>();
		feats.add("sense_key");
		feats.add("title");
		feats.add("gloss");
		feats.add("uncleaned_gloss");
		feats.add("label");
		
		for (int key : Feature.FEATURE_INDEXES.keySet()) {
			feats.add(Feature.FEATURE_INDEXES.get(key).getDBName());
		}
		
		List<String[]> results = conn.selectQueryWhereRestricted(feats, table, "title", title);
		
		List<ClassifierData> senses = new ArrayList<ClassifierData>();
		for (String[] r : results) {
			String rTitle = r[feats.indexOf("title")];
			String senseKey = r[feats.indexOf("sense_key")];
			String gloss = r[feats.indexOf("gloss")];
			String uncleaned = r[feats.indexOf("uncleaned_gloss")];
			int label = Integer.parseInt(r[feats.indexOf("label")]);
			
			Sense s = new Sense(senseKey, rTitle, gloss, uncleaned, label);
			
			for (int key : Feature.FEATURE_INDEXES.keySet()) {
				double val = Double.parseDouble(r[feats.indexOf(Feature.FEATURE_INDEXES.get(key).getDBName())]);
				s.addFeature(key, val);
			}
			senses.add(s);
		}
		return senses;
	}
	
	public static List<ClassifierData> getSpecCleanedData(List<Sense> test, String path) {
		Set<String> specs = new HashSet<String>();
		try {
			Scanner scan = new Scanner(new File(path));
			while (scan.hasNextLine()) {
				String line = scan.nextLine();
				specs.add(line.trim());
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error with file: " + path);
		}
		List<ClassifierData> cleaned = new ArrayList<ClassifierData>();
		
		for (Sense s : test) {
			String uncleaned = s.getUncleanedGloss();
			boolean specFound = false;
			if (uncleaned.startsWith("{") && uncleaned.contains("}")) {
				String context = uncleaned.substring(0, uncleaned.indexOf("}"));
				for (String spec : specs) {
					if (context.contains(spec)) {
						specFound = true;
					}
				}
			}
			if (!specFound) {
				cleaned.add(s);
			}
			
		}
		return cleaned;
	}
	
	public static Map<Integer, List<ClassifierData>> getData(boolean equalDist, String percents,
			int numFolds, boolean stratified, List<ClassifierData> l1, List<ClassifierData> l2) {
		
		
		List<List<ClassifierData>> data = null;
		if (equalDist) {
			System.out.println("Using an equal title distribution.");
			data = ClassifierDataUtils.getEqualDistributation(l1, l2);
		} else if (percents != null && percents.length() != 0) {
			String[] ps = percents.split(",");
			double p1 = Double.parseDouble(ps[0]);
			double p2 = Double.parseDouble(ps[1]);
			int total = Integer.parseInt(ps[2]);
			data = ClassifierDataUtils.getPercentageDistribution(l1, l2, p1, p2, total);
		} else {
			return null;
		}
		
		Map<Integer, List<ClassifierData>> folds = null;
		if (stratified) {
			System.out.println("Using stratified folds");
			folds = ClassifierDataUtils.getStratifiedFolds(data, numFolds);
		} else {
			folds = ClassifierDataUtils.getFolds(data, numFolds);
		}
		return folds;
	}
	
	public static List<ClassifierData> getData(boolean equalDist, String[] percents,
			List<ClassifierData> allData) {
				
		// split the data into two sets - one for those with label 0, one for those with label 1
		if (!equalDist && percents.length != 3) {
			return allData;
		}
		List<ClassifierData> labelZero = new ArrayList<ClassifierData>();
		List<ClassifierData> labelOne = new ArrayList<ClassifierData>();
		for (ClassifierData s : allData) {
			if (s.getLabel() == 0) {
				labelZero.add(s);
			} else {
				labelOne.add(s);
			}
		}
		
		List<List<ClassifierData>> data = null;
		if (equalDist) {
			System.out.println("Using an equal title distribution.");
			data = ClassifierDataUtils.getEqualDistributation(labelZero, labelOne);
		} else if (percents.length != 0) {
			double p1 = Double.parseDouble(percents[0]);
			double p2 = Double.parseDouble(percents[1]);
			int total = Integer.parseInt(percents[2]);
			data = ClassifierDataUtils.getPercentageDistribution(labelOne, labelZero, p1, p2, total);
		}
		// put all the data into a new list, then shuffle that list
		List<ClassifierData> full = new ArrayList<ClassifierData>();
		for (List<ClassifierData> l : data) {
			for (ClassifierData s : l) {
				full.add(s);
			}
		}
		Collections.shuffle(full);
		return full;
	}
	
	public static List<List<ClassifierData>> getPercentageDistribution(
			List<ClassifierData> positives, List<ClassifierData> negatives,
			double posPercent, double negPercent, int total) {
		posPercent = (total) * posPercent;
		negPercent = (total) * negPercent;
		
		int i1 = (int) Math.ceil(posPercent);
		int i2 = (int) Math.floor(negPercent);
		
		if (i1 + i2 != total) {
			System.out.println("Error in percentage calculations: " + i1 + " + " + i2 +
					"!= " + total);
			System.exit(1);
		}
		System.out.println("Using a split distribution: idioms=" + i1 + " literals=" + i2);
		positives = ClassifierDataUtils.getRandomSubset(positives, i1);
		negatives = ClassifierDataUtils.getRandomSubset(negatives, i2);
		List<List<ClassifierData>> data = new ArrayList<List<ClassifierData>>();
		data.add(positives);
		data.add(negatives);
		return data;
	}
	
	public static List<List<ClassifierData>> getEqualDistributation(List<ClassifierData> l1, List<ClassifierData> l2) {
		l2 = ClassifierDataUtils.getSimilarTitleWordDistribution(l1, l2);
		// Then combine them
		l1 = ClassifierDataUtils.getSimilarTitleWordDistribution(l2, l1);
		List<List<ClassifierData>> data = new ArrayList<List<ClassifierData>>();
		data.add(l1);
		data.add(l2);
		return data;
	}
	
	/** Tries to get a distribution in terms of numbers of words in a title,
	 *  that matches the model data set's distribution.
	 * @param model - the List<Sense> to use as the distribution to match
	 * @param sampleFrom - the List<Sense> to randomly draw from
	 * @return - List<Sense>, ideally with the same number of instances as the model Instances.
	 */
	public static List<ClassifierData> getSimilarTitleWordDistribution(
			List<ClassifierData> modelFrom, List<ClassifierData> sampleFrom) {		
		// Count the distribution for the model instances
		Map<Integer, Integer> titleWords = new HashMap<Integer, Integer>();
		for (int i = 0; i < modelFrom.size(); i++) {
			String title = modelFrom.get(i).getTitle();
			
			int numWords = title.split(" ").length;
			if (!titleWords.containsKey(numWords)) {
				titleWords.put(numWords, 0);
			}
			titleWords.put(numWords, titleWords.get(numWords) + 1);
		}

		// Randomize the sample from set.
		Collections.shuffle(sampleFrom);

		List<ClassifierData> result = new ArrayList<ClassifierData>();
		for (int i = 0; i < sampleFrom.size(); i++) {
			String title = sampleFrom.get(i).getTitle();
			int numWords = title.split(" ").length;
			if (!titleWords.containsKey(numWords) || 
				(titleWords.get(numWords) == 0)) {
				// Do nothing			
			} else {
				result.add(sampleFrom.get(i));
				titleWords.put(numWords, titleWords.get(numWords) - 1);
			}
		}

		for (int key : titleWords.keySet()) {
			if (titleWords.get(key) != 0) {
				//System.out.println("Warning: mapping: " + key + "->" + titleWords.get(key));
			}
		}
		
		return result;
	}
	
	/** 
	 * Randomly selects numFrom1 from set1 and numFrom2 from set2 and merges
	 * them together. Returns the resultant merged set. Randomizes the given
	 * lists as well.
	 * 
	 * @param set1
	 * @param set2
	 * @param numFrom1
	 * @param numFrom2
	 * @return the merged Instances.
	 */
	public static List<ClassifierData> getRandomMergedInstances(List<ClassifierData> set1, 
													   List<ClassifierData> set2, 
													   int numFrom1, 
													   int numFrom2) {
		List<ClassifierData> one = new ArrayList<ClassifierData>();
		List<ClassifierData> two = new ArrayList<ClassifierData>(); 

		Collections.shuffle(set1);
		Collections.shuffle(set2);
		
		for (int i = 0; i < numFrom1; i++) {
			one.add(set1.get(i));
		}
		
		for (int i = 0; i < numFrom2; i++) {
			two.add(set2.get(i));
		}

		return getMergedInstances(one, two);
	}
	
	public static List<ClassifierData> getRandomSubset(List<ClassifierData> set1, int num) {
		Collections.shuffle(set1);
		List<ClassifierData> newSet = new ArrayList<ClassifierData>();
		for (int i = 0; i < num && i < set1.size(); i++) {
			newSet.add(set1.get(i));
		}
		return newSet;
	}

	public static List<ClassifierData> getMergedInstances(List<ClassifierData> set1, List<ClassifierData> set2) {
		List<ClassifierData> data = new ArrayList<ClassifierData>();
		data.addAll(set1);
		data.addAll(set2);
		return data;
	}
	
	/**
	 * Currently has a limit of only reading the first 1,000,000 lines.
	 * @param f
	 * @return
	 */
	public static List<ClassifierData> getSensesFromFile(File f) {
		List<ClassifierData> results = new ArrayList<ClassifierData>();
		Set<String> keys = new HashSet<String>();
		try {
			Scanner scan = new Scanner(f);

			int num = 0;
			while (scan.hasNextLine() && num < 1000000) {
				String line = scan.nextLine().trim();
				if (line.length() > 0) {
					num++;
					// First split on "||||"
					String[] parts = line.split("\\|\\|\\|\\|");
					String senseKey = parts[0].trim();
					String title = parts[1].trim();
					String gloss = parts[2].trim();
					String uncleaned = parts[3].trim();
					int label = Integer.parseInt(parts[4].trim());
				
					if (!keys.contains(senseKey)) {
						Sense dp = new Sense(senseKey, title, gloss, uncleaned, label);
				
						String[] feats = parts[5].trim().split(" ");
						// Now split on ":"
						for (String feat : feats) {
							String[] pieces = feat.split(":");
							int featId = Integer.parseInt(pieces[0]);
							double value = Double.parseDouble(pieces[1]);
							dp.addFeature(featId, value);
						}
						results.add(dp);
						keys.add(senseKey);
					}
				}
			}
		} catch (IOException e) {
			System.out.println("Error with file: " + f);
		}
		return results;
	}
	

	
	public static Map<Integer, List<ClassifierData>> getFolds(List<List<ClassifierData>> dataTypes, int n){
		Map<Integer, List<ClassifierData>> folds = new HashMap<Integer, List<ClassifierData>>();
		List<ClassifierData> all = new ArrayList<ClassifierData>();
		for (List<ClassifierData> data : dataTypes) {
			all.addAll(data);
		}
		Collections.shuffle(all);
		int size = all.size() / n;
		int remainder = all.size() % n;

		int current = 0;
		for (int i = 0; i < n; i++) {
			folds.put(i, new ArrayList<ClassifierData>());
			folds.get(i).addAll(all.subList(current, current + size));
			current += size;
		}
		for (int i = 0; i < remainder; i++) {
			folds.get(i).add(all.get(current));
			current += 1;
		}
		return folds;
	}
	
	public static Map<Integer, List<ClassifierData>> getStratifiedFolds(
			List<List<ClassifierData>> dataTypes, int n){
		Map<Integer, List<ClassifierData>> folds = new HashMap<Integer, List<ClassifierData>>();
		for (List<ClassifierData> data : dataTypes) {
			Collections.shuffle(data);
			
			int size = data.size() / n;
			int remainder = data.size() % n;
			
			// For each fold, put size of this kind in it
			int current = 0;
			for (int i = 0; i < n; i++) {
				if (!folds.containsKey(i)) {
					folds.put(i, new ArrayList<ClassifierData>());
				}
				folds.get(i).addAll(data.subList(current, current + size));
				current += size;
			}
			
			for (int i = 0; i < remainder; i++) {
				folds.get(i).add(data.get(current));
				current += 1;
			}

		}
		return folds;
	}
	
	public static void printGeneralDataAnalysis(Map<Integer, List<ClassifierData>> folds) {
		Set<String> unique = new HashSet<String>();
		for (int label : folds.keySet()) {
			System.out.println("For fold " + label + ":");
			Map<Integer, Set<ClassifierData>> types = new HashMap<Integer, Set<ClassifierData>>();
			for (ClassifierData s : folds.get(label)) {
				if (unique.contains(s.getKey())) {
					//System.out.println("Duplicate data error.");
				}
				unique.add(s.getKey());
				if (!types.containsKey(s.getLabel())) {
					types.put(s.getLabel(), new HashSet<ClassifierData>());
				}
				types.get(s.getLabel()).add(s);
			}
			for (int l : types.keySet()) {
				System.out.println("Entries with label "+ l + ": " + types.get(l).size());
			}
		}
		System.out.println("Unique entries: " + unique.size());
	}
	
	public static String getGeneralDataAnalysis(List<ClassifierData> data) {
		String str = "";
		Set<String> unique = new HashSet<String>();
		Map<Integer, Set<ClassifierData>> types = new HashMap<Integer, Set<ClassifierData>>();
		for (ClassifierData s : data) {
			if (unique.contains(s.getKey())) {
				//System.out.println("Duplicate data error.");
			}
			unique.add(s.getKey());
			if (!types.containsKey(s.getLabel())) {
				types.put(s.getLabel(), new HashSet<ClassifierData>());
			}
			types.get(s.getLabel()).add(s);
		}
		for (int l : types.keySet()) {
			str += "Entries with label "+ l + ": " + types.get(l).size() + "\n";
		}
		str += "Unique entries: " + unique.size() + "\n";
		return str;
	}
	
	public static int getNumIdioms(List<Result> l) {
		int num = 0;
		for (Result s : l) {
			if (s.getClassifierData().getLabel() == 1) {
				num++;
			}
		}
		return num;
	}
	
	public static void printErrors(List<TreeMap<Integer, List<ClassifierData>>> results,
			List<Integer> featuresToUse) {
		System.out.println("**************************");
		System.out.println("Errors: ");
		int i = 0;
		for (TreeMap<Integer, List<ClassifierData>> fResult : results) {
			System.out.println("Fold: " + i);
			i++;
			for (int pred : fResult.keySet()) {
				System.out.println("Predicted: " + pred);
				for (ClassifierData s : fResult.get(pred)) {
					if (s.getLabel() != pred) {
						s.printPrettyString(featuresToUse);
					}
				}
				System.out.println("###########");
			}
		}
	}
	
	public static void printSuccesses(List<TreeMap<Integer, List<ClassifierData>>> results,
			List<Integer> featuresToUse) {
		System.out.println("**************************");
		System.out.println("Successes: ");
		int i = 0;
		for (TreeMap<Integer, List<ClassifierData>> fResult : results) {
			System.out.println("Fold: " + i);
			i++;
			for (int pred : fResult.keySet()) {
				System.out.println("Predicted: " + pred);
				for (ClassifierData s : fResult.get(pred)) {
					if (s.getLabel() == pred) {
						s.printPrettyString(featuresToUse);
					}
				}
				System.out.println("###########");
			}
		}
	}
	
	public static List<Sense> gatherErrors(List<TreeMap<Integer, List<Sense>>> results) {
		List<Sense> errors = new ArrayList<Sense>();
		for (TreeMap<Integer, List<Sense>> fResult : results) {
			for (int pred : fResult.keySet()) {
				for (Sense s : fResult.get(pred)) {
					if (s.getLabel() != pred) {
						errors.add(s);
					}
				}
			}
		}
		return errors;
	}
	
	public static List<Sense> gatherSuccesses(List<TreeMap<Integer, List<Sense>>> results) {
		List<Sense> successes = new ArrayList<Sense>();
		for (TreeMap<Integer, List<Sense>> fResult : results) {
			for (int pred : fResult.keySet()) {
				for (Sense s : fResult.get(pred)) {
					if (s.getLabel() == pred) {
						successes.add(s);
					}
				}
			}
		}
		return successes;
	}
}
