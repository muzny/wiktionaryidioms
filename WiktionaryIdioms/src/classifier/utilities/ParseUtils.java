/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;

public class ParseUtils {

	
	public static List<List<HasWord>> parseText(String text) {

		Reader reader = new StringReader(text);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);

		Iterator<List<HasWord>> it = dp.iterator();
		List<List<HasWord>> parsed = new ArrayList<List<HasWord>>();
		while (it.hasNext()) {
		   List<HasWord> sentence = it.next();
		   parsed.add(sentence);
		}
		return parsed;
	}
	
	public static String[] parseTextToArray(String text) {

		Reader reader = new StringReader(text);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);

		Iterator<List<HasWord>> it = dp.iterator();
		List<List<HasWord>> parsed = new ArrayList<List<HasWord>>();
		while (it.hasNext()) {
		   List<HasWord> sentence = it.next();
		   parsed.add(sentence);
		}
		
		int size = 0;
		for (List<HasWord> sent : parsed) {
			size += sent.size();
		}
		
		String[] result = new String[size];
		int index = 0;
		for (List<HasWord> sent : parsed) {
			for (int i = 0; i < sent.size(); i++) {
				result[index] = sent.get(i).toString();
				index++;
			}
		}
		return result;
	}
	
}
