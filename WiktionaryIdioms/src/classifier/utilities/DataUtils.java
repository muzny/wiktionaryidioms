/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.util.List;

import mysql.utilities.MySQLConnection;

import classifier.model.DataPoint;

public class DataUtils {
	
	public static void insertData(String database, String table, List<String> whatToInsert, List<DataPoint> data) {
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");
		moby.insertDataPointsInto(whatToInsert, data, table);
	}
	
	public static void insertDataCodes(String database, String table,
									   List<String> whatToInsert, List<String[]> data, boolean verbose) {
		MySQLConnection moby = MySQLConnection.getInstance("localhost", database, "root", "");
		moby.insertDataCodesInto(whatToInsert, data, table, verbose);
	}
}
