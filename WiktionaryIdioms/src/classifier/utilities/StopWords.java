/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class StopWords {
	public static final String FULL = "./stopwords/full.txt";
	public static final String PARTIAL = "./stopwords/partial.txt";

	
	private static Map<String, StopWords> instances;

	private Set<String> stopwords;
	
	
	private StopWords(File stopFile) {
		try {
			Scanner lines = new Scanner(stopFile);
			stopwords = new HashSet<String>();
			while (lines.hasNextLine()) {
				stopwords.add(lines.nextLine().trim().toLowerCase());
			}
		} catch (FileNotFoundException e) {
			System.out.println("Unable to find file: " + stopFile);
		}
	}
	
	public static StopWords getInstance(String kind) {
		if (instances == null) {
			instances = new HashMap<String, StopWords>();
		}
		if (!instances.containsKey(kind)) {
			instances.put(kind, new StopWords(new File(kind)));
		}
		return instances.get(kind);
	}
	
	// Ignores case
	public boolean isStopWord(String word) {
		return stopwords.contains(word.toLowerCase());
	}
	
	public String removeStopWords(String sentence) {
		String[] words = sentence.split(" ");
		List<String> nonStopWords = new ArrayList<String>();
		for (String word: words) {
			String w = word.trim();
			if (!isStopWord(w) && w.length() > 0) {
				nonStopWords.add(w);
			}
		}
		return StringUtils.join(nonStopWords, " ");
	}
}
