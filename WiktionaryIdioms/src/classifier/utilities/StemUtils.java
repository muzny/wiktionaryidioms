/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.util.ArrayList;
import java.util.List;

import de.tudarmstadt.ukp.wiktionary.api.IWikiString;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import edu.stanford.nlp.ling.HasWord;

public class StemUtils {
	public static final String[] PUNCTUATION = {"\"", "'", "?", "!", ".", ";", ",", "(", ")", "{", "}", 
		"[", "]", ":", "`", "-LRB-", "-RRB-", "-LCB-", "-RCB-"};
	
	private static final Stemmer stem = new Stemmer();
	
	public static String cleanSentence(String sentence) {
		sentence = sentence.toLowerCase();
		
		// Remove any punctuation, replace them with spaces.
		for (String punc : PUNCTUATION) {
			// but don't clean out the apostrophes yet
			if (!punc.equals("'")) {
				sentence = sentence.replace(punc, " ");
			}
		}
		
		return sentence;
	}
	
	public static List<List<String>> stemSentences(List<List<HasWord>> sentences, boolean clean, StopWords stops) {
		List<List<String>> cleaned = new ArrayList<List<String>>();
		for (int sent = 0; sent < sentences.size(); sent++) {
			List<HasWord> sentence = sentences.get(sent);
			cleaned.add(new ArrayList<String>());
			for (int i = 0; i < sentence.size(); i++) {
				String word = sentence.get(i).toString();
				
				if (clean) {
					// Remove any punctuation, replace them with spaces.
					for (String punc : PUNCTUATION) {
						word = word.replace(punc, " ");
					}
					
					// Remove all stop words
					word = stops.removeStopWords(word);
				}
				
				if (word.trim().length() > 0) {
					stem.add(word.toCharArray(), word.length());
					stem.stem();
					String result = stem.toString();
					cleaned.get(sent).add(result);
				}
			}
		}
		return cleaned;
	}

	
	public static String stemAndCleanSentence(String sentence, StopWords stops) {
		sentence = sentence.toLowerCase();
		
		// Remove any punctuation, replace them with spaces.
		for (String punc : PUNCTUATION) {
			sentence = sentence.replace(punc, " ");
		}
		
		
		// Remove all stop words
		sentence = stops.removeStopWords(sentence);
		
		String[] words = sentence.split(" ");
		
		// Deal with colons
		for (int i = 0; i < words.length; i++) {
			if (words[i].contains(":")) {
				// get the substring after the :
				words[i] = words[i].substring(words[i].indexOf(":") + 1);
			}
		}
		
		String result = "";
		for (String word: words) {
			stem.add(word.toCharArray(), word.length());
			stem.stem();
			result += " " + stem.toString();
		}
		return result.trim();
	}
	
	public static String[] stemAndCleanSentenceToArray(String sentence, StopWords stops) {
		sentence = sentence.toLowerCase();
		
		// Remove any punctuation, replace them with spaces.
		for (String punc : PUNCTUATION) {
			sentence = sentence.replace(punc, " ");
		}
		
		
		// Remove all stop words
		sentence = stops.removeStopWords(sentence);
		
		String[] words = sentence.split(" ");
		
		// Deal with colons
		for (int i = 0; i < words.length; i++) {
			if (words[i].contains(":")) {
				// get the substring after the :
				words[i] = words[i].substring(words[i].indexOf(":") + 1);
			}
		}
		
		String[] result = new String[words.length];
		for (int i = 0; i < words.length; i++) {
			String word = words[i];
			stem.add(word.toCharArray(), word.length());
			stem.stem();
			result[i] = stem.toString();
		}
		return result;
	}
	
	public static String stemWord(String word) {
		stem.add(word.toCharArray(), word.length());
		stem.stem();
		return stem.toString();
	}

	public static List<String> getStemmedGlosses(IWiktionaryPage page, StopWords stops) {
		List<IWiktionaryEntry> entries = page.getEntries();

		// For each entry, go and get the definitions and stem them.
		List<String> stemmedGlosses = new ArrayList<String>();
		for (IWiktionaryEntry entry : entries) {
			List<IWikiString> glosses = entry.getGlosses();
				
			// For each gloss, get the plain text and stem it.
			for (IWikiString gloss: glosses) {
				String text = gloss.getPlainText();
				stemmedGlosses.add(StemUtils.stemAndCleanSentence(text, stops));
			}
		}
		return stemmedGlosses;
	}
	
	
}
