/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.util.HashMap;
import java.util.Map;

public class CommandLineUtils {
	  /**
	   * Simple method which turns an array of command line arguments into a map,
	   * where each token starting with a '-' is a key and the following non '-'
	   * initial token, if there is one, is the value.  For example, '-size 5
	   * -verbose' will produce keys (-size,5) and (-verbose,null).
	   * 
	   * @author Dan Klein
	   */
	  public static Map<String, String> simpleCommandLineParser(String[] args) {
	    Map<String, String> map = new HashMap<String, String>();
	    for (int i = 0; i <= args.length; i++) {
	      String key = (i > 0 ? args[i - 1] : null);
	      String value = (i < args.length ? args[i] : null);
	      if (key == null || key.startsWith("-")) {
	        if (value != null && value.startsWith("-"))
	          value = null;
	        if (key != null || value != null)
	          map.put(key, value);
	      }
	    }
	    return map;
	  }
}
