/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.smu.tspell.wordnet.AdjectiveSynset;
import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.VerbSynset;
import edu.smu.tspell.wordnet.WordNetDatabase;
import edu.smu.tspell.wordnet.WordSense;

public class WordNetUtils {
	public static final String DATABASE_DIR = "[PATH TO WORDNET]/WordNet-3.0/dict/";
	
	private static String isSet;
	private static WordNetUtils instance;  // flyweight
		
	public static void setDatabaseDirectory(String directory) {
		if (isSet == null) {
			System.setProperty("wordnet.database.dir", directory);
			isSet = directory;
		}
	}
	
	public static void setDatabaseDirectory() {
		setDatabaseDirectory(DATABASE_DIR);
	}
	
	public static void useWordNet(){
		setDatabaseDirectory();
		WordNetDatabase db = WordNetDatabase.getFileInstance();
		Synset[] synsets = db.getSynsets("FacT");
		for (int i = 0; i < synsets.length; i++) { 
		    Synset s = (synsets[i]);
		    System.out.println(s.getDefinition());
		    System.out.println(Arrays.toString(s.getWordForms()));
		    for (String word : s.getWordForms()) {
		    	WordSense[] ws = s.getAntonyms(word);
		    	for (WordSense w : ws) {
				    System.out.println("Antonym: " + w.getWordForm());
		    	}
		    }
		}
	}
	
	public static List<String> getHyponymsDefinitions(NounSynset ns) {
		List<String> defs = new ArrayList<String>();
		NounSynset[] hypoSets = ns.getHyponyms();
		for (NounSynset hypoNS : hypoSets) {
			defs.add(hypoNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getHypernymsDefinitions(NounSynset ns) {
		List<String> defs = new ArrayList<String>();
		NounSynset[] hyperSets = ns.getHypernyms();
		for (NounSynset hyperNS : hyperSets) {
			defs.add(hyperNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getMeronymsDefinitions(NounSynset ns) {
		List<String> defs = new ArrayList<String>();
		NounSynset[] meroSets = ns.getMemberMeronyms();
		for (NounSynset meroNS : meroSets) {
			defs.add(meroNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getExamples(Synset s) {
		List<String> defs = new ArrayList<String>();
		String[] examples = s.getUsageExamples();
		for (String example : examples) {
			defs.add(example);
		}
		return defs;
	}
	
	public static List<String> getHyponymsDefinitions(VerbSynset vs) {
		List<String> defs = new ArrayList<String>();
		VerbSynset[] hypoSets = vs.getTroponyms();
		for (VerbSynset hypoNS : hypoSets) {
			defs.add(hypoNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getHypernymsDefinitions(VerbSynset vs) {
		List<String> defs = new ArrayList<String>();
		VerbSynset[] hyperSets = vs.getHypernyms();
		for (VerbSynset hyperNS : hyperSets) {
			defs.add(hyperNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getAlsoDefinitions(AdjectiveSynset as) {
		List<String> defs = new ArrayList<String>();
		AdjectiveSynset[] alsoSets = as.getRelated();
		for (AdjectiveSynset alsoNS : alsoSets) {
			defs.add(alsoNS.getDefinition());
		}
		return defs;
	}
	
	public static List<String> getAttributeDefinitions(AdjectiveSynset as) {
		List<String> defs = new ArrayList<String>();
		NounSynset[] attrSets = as.getAttributes();
		for (NounSynset attrNS : attrSets) {
			defs.add(attrNS.getDefinition());
		}
		return defs;
	}
	
	public static Set<String> getAllAntonyms(Synset[] syns) {
		Set<String> ants = new HashSet<String>();
		for (int i = 0; i < syns.length; i++) { 
		    Synset s = (syns[i]);
		    for (String word : s.getWordForms()) {
		    	WordSense[] ws = s.getAntonyms(word);
		    	for (WordSense w : ws) {
		    		ants.add(w.getWordForm());
		    	}
		    }
		}
		return ants;
	}
	
	public static int getShortestSynonymPathDistance(String from, String target, int max) {
		setDatabaseDirectory();
		WordNetDatabase db = WordNetDatabase.getFileInstance();
		Map<String, Integer> distances = new HashMap<String, Integer>();
		Map<String, String> backs = new HashMap<String, String>();
		
		// Initialize by setting distance of from to 1
		distances.put(from, 0);
		
		List<String> queue = new ArrayList<String>();
		queue.add(from);
		int currentDist = 0;
		while (!queue.isEmpty() && currentDist < max) {
			String current = queue.remove(0);
			
			if (!distances.containsKey(current) ||
				current.equals(target)) {
				break;
			}
			currentDist = distances.get(current);
			// The synonyms are the neighbors of current
			Synset[] synsets = db.getSynsets(current);
			
			// build the set of synonyms
			Set<String> synonyms = new HashSet<String>();
			for (Synset synset : synsets) {
				for (String syn : synset.getWordForms()) {
					synonyms.add(syn);
				}
			}
			
			for (String syn : synonyms) {
				int alt = distances.get(current) + 1;
				// If it is not in distances yet or if the distance is
				// shorter than what is already in distances, put it in
				// and store the backpointer.
				if (!distances.containsKey(syn) ||
						distances.get(syn) > alt) {
					distances.put(syn, alt);
					backs.put(syn, current);
					queue.add(syn);
				}
			}
		}
		if (distances.containsKey(target)) {
			return distances.get(target);
		}
		return -1;
	}
	
}
