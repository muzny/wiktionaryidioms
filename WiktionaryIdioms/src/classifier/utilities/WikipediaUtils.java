package classifier.utilities;

/*******************************************************************************
* Copyright (c) 2010 Torsten Zesch.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the GNU Lesser Public License v3
* which accompanies this distribution, and is available at
* http://www.gnu.org/licenses/lgpl.html
*
* Contributors:
*     Torsten Zesch - initial API and implementation
******************************************************************************/

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.tudarmstadt.ukp.wikipedia.api.*;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiInitializationException;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiTitleParsingException;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.util.StringUtils;

/**
* Tutorial 1a
*
* Get the text of a wikipedia article.
* The text will be formatted with MediaWiki markup.
*
* Throws an exception, if no page with the given title exists.
*
* @author zesch
*
*/
public class WikipediaUtils implements WikiConstants {
	
	private DatabaseConfiguration dbConfig;
	private Wikipedia wiki;
	private static WikipediaUtils instance;  // For the singleton design pattern
	
	private WikipediaUtils() {
		 // configure the database connection parameters
	     dbConfig = new DatabaseConfiguration();
	     dbConfig.setHost("127.0.0.1");
	     dbConfig.setDatabase("wikipedia");
	     dbConfig.setUser("root");
	     dbConfig.setPassword("");
	     dbConfig.setLanguage(Language.english);
	      
	     // Create a new english wikipedia.
	     try {
			wiki = new Wikipedia(dbConfig);
		} catch (WikiInitializationException e) {
			System.err.println("Error initializing wikipedia.");
		}
	}
	
	public static WikipediaUtils getInstance() {
		if (instance == null) {
			instance = new WikipediaUtils();
		}
		return instance;
	}
	
	public Iterable getArticles() {
		return wiki.getArticles();
	}
	
	public List<Page> getArticleTitles(int num) {
		Iterable<Page> ps = wiki.getArticles();
		List<Page> titles = new ArrayList<Page>();
		int count = 0;
		for (Page p : ps) {
			
			if (p == null) {
				continue;
			}
			try {
				p.getPlainText();
			} catch (WikiApiException e) {
				continue;
			}
			
			titles.add(p);
			count++;
			
			if (count == num) {
				return titles;
			}
		}
		return titles;
	}
	
	/**
	 * Gets the page from Wikipedia, if there is no page for the given title,
	 * returns null instead. Only looks for article pages and returns the first page
	 * if more than one have the given title.
	 * @param title
	 * @return Page with title title, if not found, null
	 */
	public Page getPage(String title) {
		try {
			PageQuery pq = new PageQuery();
			pq.setOnlyArticlePages(true);
			pq.setTitlePattern(title);
			Iterable<Page> ps = wiki.getPages(pq);
			for (Page p : ps) {
				return p;
			}
		} catch (WikiApiException e) {
			return null;
		}
		return null;
	}
	
	public String getPageText(String title) {
		Page p = getPage(title);

		if (p != null) {
			try {
				return p.getPlainText();
			} catch (WikiApiException e) {
				return null;
			}
		}
		return null;
	}
	
	public String getPageAbstract(String title) {
		String text = getPageText(title);
		if (text != null) {
			return text.split("\n")[0];
		}
		return null;
	}
	
	public String getPageAbstract(Page p) {
		String text;
		try {
			text = p.getPlainText();
		} catch (WikiApiException e) {
			text = null;
		}
		if (text != null) {
			return text.split("\n")[0];
		}
		return null;
	}
	
	public List<List<HasWord>> getSentenceParsedAbstract(Page p) {
		String abs = getPageAbstract(p);
		if (abs != null) {
			return ParseUtils.parseText(abs);
		}
		return null;
	}
	
	public List<List<HasWord>> getSentenceParsedPage(Page p) {
		return getSentenceParsedPage(p, -1);
	}
	
	public List<List<HasWord>> getSentenceParsedAbstract(String title) {
		String abs = getPageAbstract(title);
		if (abs != null) {
			return ParseUtils.parseText(abs);
		}
		return null;
	}
	
	public List<List<HasWord>> getSentenceParsedPage(String title) {
		return getSentenceParsedPage(getPage(title), -1);
	}
	
	public List<List<HasWord>> getSentenceParsedPage(Page p, int n) {
		String result = null;
		try {
			result = p.getPlainText();
		} catch (WikiApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (result == null) {
			return null;
		}
		Reader reader = new StringReader(result);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);

		Iterator<List<HasWord>> it = dp.iterator();
		List<List<HasWord>> parsed = new ArrayList<List<HasWord>>();
		int i = 0;
		while (it.hasNext() && (i < n || n < 0)) {
		   List<HasWord> sentence = it.next();
		   parsed.add(sentence);
		   i++;
		}
		return parsed;
	}
	
	public Iterable<Page> test() {
		PageQuery pq = new PageQuery();
		pq.setOnlyArticlePages(true);
		Iterable<Page> ps = null;
		try {
			ps = wiki.getPages(pq);
		} catch (WikiApiException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ps;
	}

	public static void main(String[] args) {
		WikipediaUtils wu = WikipediaUtils.getInstance();
		Page p = wu.getPage("high_roller");
		try {
			System.out.println(p.getTitle());
		} catch (WikiTitleParsingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println(wu.getPageAbstract(p));
		System.out.println(p.getNumberOfInlinks());
		System.out.println(p.getNumberOfOutlinks());
		/*Iterable<Page> test = wu.test();
		int count = 0;
		for (Page p : test) {
			count++;
			try {
				System.out.println(p.getTitle());
			} catch (WikiTitleParsingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			System.out.println(p.getNumberOfInlinks());
			System.out.println(p.getNumberOfOutlinks());
			if (count == 10) {
				break;
			}
		}*/
	}
}
