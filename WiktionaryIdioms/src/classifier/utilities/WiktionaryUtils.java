/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.tudarmstadt.ukp.wiktionary.api.IWikiString;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryRelation;
import de.tudarmstadt.ukp.wiktionary.api.RelationType;
import de.tudarmstadt.ukp.wiktionary.api.Wiktionary;
import de.tudarmstadt.ukp.wiktionary.api.util.ILanguage;

public class WiktionaryUtils {
	public static final String BASE_REGEX = "\\{\\{[^\\{]*of\\|[^\\}]*\\}\\}";
	public static int count = 0;
	public static int longestAcyclicPath = -1;
	public static final String DEFAULT_DIR = "/home/grace/Research/wiktionary_output/";
	public static Map<String, IWiktionaryEdition> wkts;
	
	public static IWiktionaryEdition getWiktionaryEdition(String dir) {
		if(wkts == null) {
			wkts = new HashMap<String, IWiktionaryEdition>();
		}
		if (!wkts.containsKey(dir)) {
			wkts.put(dir, Wiktionary.open(new File(DEFAULT_DIR)));
		}
		return wkts.get(dir);
	}
	
	public static IWiktionaryEdition getWiktionaryEdition() {
		return getWiktionaryEdition(DEFAULT_DIR);
	}
	
	public static Set<String> getAntonymsForWords(String title, IWiktionaryEdition wkt, int depthLevel) {
		return getRelationsForTitleWords(title, wkt, RelationType.ANTONYM, depthLevel);
	}
	
	public static Set<String> getSynonymsForWords(String title, IWiktionaryEdition wkt, int depthLevel) {
		Set<String> syns = getRelationsForTitleWords(title, wkt, RelationType.SYNONYM, depthLevel);
		return syns;
	}
	
	public static Set<String> getRelationsForTitleWords(String title,
			IWiktionaryEdition wkt, RelationType rel, int depthLevel) {
		Set<String> titleSynonyms = new HashSet<String>();
		Set<String> done = new HashSet<String>();
		for (String word : title.split(" ")) {
			titleSynonyms.add(word);
		}
		for (int i = 0; i < depthLevel; i++){
			Set<String> todo = new HashSet<String>();
			for (String word : titleSynonyms) {
				if (!done.contains(word)) {
					todo.addAll(getRelationsForWord(word, wkt, rel));
					done.add(word);
				}
			}
			titleSynonyms.addAll(todo);
			todo.clear();
		}
		return titleSynonyms;
	}
	
	public static Set<String> getRelationsForWord(String word, IWiktionaryEdition wkt, RelationType rel) {
		Set<String> syns = new HashSet<String>();
		List<IWiktionaryEntry> titleEntries = wkt.getEntriesForWord(word);
			
		for (IWiktionaryEntry entry: titleEntries) {
				
			// Get the relations for this entry.
			// only if the entry is an english entry
			if (entry.getWordLanguage() == ILanguage.ENGLISH) {
				List<IWiktionaryRelation> relations = entry.getRelations(rel);
				for (IWiktionaryRelation relation: relations) {
					//String target = relation.getTarget();
					//target = WiktionaryUtils.getPostColon(target);
					syns.add(relation.getTarget());				
				}
			}
		}
		return syns;
	}
	
	public static String getPostColon(String word) {
		if (word.contains(":")) {
			return word.substring(word.indexOf(":") + 1);
		}
		return word;
	}
	
	public static Set<String> getAllRelationsForWord(String word, IWiktionaryEdition wkt) {
		RelationType[] rels = RelationType.values();
		Set<String> total = new HashSet<String>();
		for (RelationType rel : rels) {
			total.addAll(getRelationsForWord(word, wkt, rel));
		}
		// remove all things that 
		
		return total;
	}
	
	public static IWiktionaryPage getBasePageForWord(String word, IWiktionaryEdition wkt) {
		IWiktionaryPage p = wkt.getPageForWord(word);
		if (p == null) {
			return null;
		}
		List<IWiktionaryEntry> entries = p.getEntries();
	    Pattern pattern = Pattern.compile(BASE_REGEX);


		List<String> possibilities = new ArrayList<String>();
		for (IWiktionaryEntry entry : entries) {
			// Only pay attention to the entries that are in english
			if (entry.getWordLanguage() == ILanguage.ENGLISH) {
				List<IWikiString> glosses = entry.getGlosses();

				// For each gloss, get the plain text and stem it.
				for (IWikiString gloss: glosses) {
					String text = gloss.getText();
					// try to match the form "{{X of|Y}}"
				    Matcher matcher = pattern.matcher(text);
					if (matcher.matches()) {
						// Get the thing that it is a form of, which is after the first |
						String matched = matcher.group(0);

						String referant = getXofYReferant(matched);
						possibilities.add(referant);
					}
				}
			}
		}
		// for now, if the possibilities list is not empty, return the page corresponding with
		// the first entry in the list
		if (possibilities.size() > 0) {
			p = wkt.getPageForWord(possibilities.get(0));
		}
		return p;
	}
	
	public static String getXofYReferant(String text) {
		String piece = text.split("\\{\\{")[1].split("\\}\\}")[0];
		if (piece.endsWith("|") && piece.indexOf("|") == piece.length() - 1) {
			return getXofYReferant("{{" + text.split("\\{\\{")[2]);
		}
		return piece.split("\\|")[1];
	}
	
	public static int getShortestOneLevelAntonymThenSynonymPathDistance(String from, String target,
			IWiktionaryEdition wkt, int max) {
		// Get one level of antonyms from the from word
		Set<String> antonyms = getRelationsForWord(from, wkt, RelationType.ANTONYM);
		
		// Then go and find the smallest distance according to synonyms.
		// Max for the synonym distance should be one less that this because
		// you've effectively already gone one level deep.
		int leastDistance = max + 1;
		for (String antFrom : antonyms) {
			int dist = getShortestSynonymPathDistance(antFrom, target, wkt, max - 1);
			if (dist < leastDistance && dist >= 0) {
				leastDistance = dist;
			} 
		}
		return leastDistance;
	}
	
	public static int getShortestSynonymPathDistance(String from, String target, IWiktionaryEdition wkt, int max) {
		Map<String, Integer> distances = new HashMap<String, Integer>();
		Map<String, String> backs = new HashMap<String, String>();
		
		// Initialize by setting distance of from to 1
		distances.put(from, 0);
		
		List<String> queue = new ArrayList<String>();
		queue.add(from);
		int currentDist = 0;
		while (!queue.isEmpty() && currentDist < max) {
			String current = queue.remove(0);
			
			if (!distances.containsKey(current) ||
				current.equals(target)) {
				break;
			}
			currentDist = distances.get(current);
			// The synonyms are the neighbors of current
			Set<String> syns = getRelationsForWord(current, wkt, RelationType.SYNONYM);
			for (String syn : syns) {
				int alt = distances.get(current) + 1;
				// If it is not in distances yet or if the distance is
				// shorter than what is already in distances, put it in
				// and store the backpointer.
				if (!distances.containsKey(syn) ||
						distances.get(syn) > alt) {
					distances.put(syn, alt);
					backs.put(syn, current);
					queue.add(syn);
				}
			}
		}
		if (distances.containsKey(target)) {
			return distances.get(target);
		}
		return -1;
	}
	
	public static int getShortestAllRelationPathDistance(String from, String target,
			IWiktionaryEdition wkt, int max) {
		Map<String, Integer> distances = new HashMap<String, Integer>();
		Map<String, String> backs = new HashMap<String, String>();
		
		// Initialize by setting distance of from to 0
		distances.put(from, 0);
		
		List<String> queue = new ArrayList<String>();
		queue.add(from);
		int currentDist = 0;
		while (!queue.isEmpty() && currentDist < max) {
			String current = queue.remove(0);
			
			if (!distances.containsKey(current) ||
				current.equals(target)) {
				break;
			}
			currentDist = distances.get(current);
			// The synonyms are the neighbors of current
			Set<String> syns = getAllRelationsForWord(current, wkt);
			for (String syn : syns) {
				int alt = distances.get(current) + 1;
				// If it is not in distances yet or if the distance is
				// shorter than what is already in distances, put it in
				// and store the backpointer.
				if (!distances.containsKey(syn) ||
						distances.get(syn) > alt) {
					distances.put(syn, alt);
					backs.put(syn, current);
					queue.add(syn);
				}
			}
		}
		if (distances.containsKey(target)) {
			return distances.get(target);
		}
		return -1;
	}

}
