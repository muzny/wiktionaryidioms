/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This makes features that compute distances have a common interface for easier
 * feature computation.
 */

package classifier.distances;

import java.util.TreeMap;

public interface Distance {
	// Map from column name in database to class that computes the distance for that column
	public static final TreeMap<String, Distance> TYPES = new TreeMap<String , Distance>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
	    put("wordnet_synonym_distance", new WordNetSynonym());
	    put("wiktionary_synonym_distance", new WiktionarySynonym());

	    put("wiktionary_onelevel_antonym_distance", new WiktionaryOneLevelAntonym());
	    put("wordnet_onelevel_antonym_distance", new WordNetOneLevelAntonym());
	    
	    put("wordnet_hypernym_distance", new WordNetHypernym());
	    put("wordnet_hyponym_distance", new WordNetHyponym());

	    
	}};
	
	
	public int getDistance(String from, String target, int max);
}
