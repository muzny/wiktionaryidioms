/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.distances;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classifier.utilities.WiktionaryUtils;


import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.RelationType;

public class WiktionarySynonym implements Distance {

	@Override
	/**
	 * The distance between from and to, capped at max. 
	 * Follows synonym links.
	 * Returns 0 if no path was found within max distance.
	 */
	public int getDistance(String from, String target, int max) {
		IWiktionaryEdition wkt = WiktionaryUtils.getWiktionaryEdition();
		
		Map<String, Integer> distances = new HashMap<String, Integer>();
		Map<String, String> backs = new HashMap<String, String>();
		
		// Initialize by setting distance of from to 1
		distances.put(from, 0);
		
		List<String> queue = new ArrayList<String>();
		queue.add(from);
		int currentDist = 0;
		while (!queue.isEmpty() && currentDist < max) {
			String current = queue.remove(0);
			
			if (!distances.containsKey(current) ||
				current.equals(target)) {
				break;
			}
			currentDist = distances.get(current);
			// The synonyms are the neighbors of current
			Set<String> syns = WiktionaryUtils.getRelationsForWord(current, wkt, RelationType.SYNONYM);
			for (String syn : syns) {
				int alt = distances.get(current) + 1;
				// If it is not in distances yet or if the distance is
				// shorter than what is already in distances, put it in
				// and store the backpointer.
				if (!distances.containsKey(syn) ||
						distances.get(syn) > alt) {
					distances.put(syn, alt);
					backs.put(syn, current);
					queue.add(syn);
				}
			}
		}
		if (distances.containsKey(target)) {
			return distances.get(target);
		}
		return -1;
	}

}
