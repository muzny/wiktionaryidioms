/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.distances;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import classifier.utilities.WordNetUtils;

import edu.smu.tspell.wordnet.NounSynset;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.SynsetType;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class WordNetHypernym implements Distance {

	@Override
	/**
	 * The distance between from and to, capped at max. 
	 * Follows hypernym links.
	 * Returns 0 if no path was found within max distance.
	 */
	public int getDistance(String from, String target, int max) {
		WordNetUtils.setDatabaseDirectory();
		WordNetDatabase db = WordNetDatabase.getFileInstance();
		Map<String, Integer> distances = new HashMap<String, Integer>();
		Map<String, String> backs = new HashMap<String, String>();
		
		// Initialize by setting distance of from to 1
		distances.put(from, 0);
		
		List<String> queue = new ArrayList<String>();
		queue.add(from);
		int currentDist = 0;
		while (!queue.isEmpty() && currentDist < max) {
			String current = queue.remove(0);
			currentDist = distances.get(current);
			
			if (!distances.containsKey(current) ||
				current.equals(target) ||
				currentDist == max) {
				break;
			}
			// The synonyms are the neighbors of current
			Synset[] synsets = db.getSynsets(current, SynsetType.NOUN);
			
			// build the set of synonyms
			Set<String> hypernyms = new HashSet<String>();
			NounSynset ns;
			for (Synset synset : synsets) {
				ns = (NounSynset) synset;
				NounSynset[] hypers = ns.getHypernyms();
				for (NounSynset hypset : hypers) {
					for (String hyp : hypset.getWordForms()) {
						hypernyms.add(hyp);
					}
				}
			}
			
			for (String syn : hypernyms) {
				int alt = distances.get(current) + 1;
				// If it is not in distances yet or if the distance is
				// shorter than what is already in distances, put it in
				// and store the backpointer.
				if (!distances.containsKey(syn) ||
						distances.get(syn) > alt) {
					distances.put(syn, alt);
					backs.put(syn, current);
					queue.add(syn);
				}
			}
		}
		if (distances.containsKey(target)) {
			return distances.get(target);
		}
		return -1;
	}

}
