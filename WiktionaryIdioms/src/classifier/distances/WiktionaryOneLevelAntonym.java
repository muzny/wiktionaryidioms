/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.distances;

import java.util.Set;

import classifier.utilities.WiktionaryUtils;


import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.RelationType;

public class WiktionaryOneLevelAntonym implements Distance {

	@Override
	/**
	 * The distance between from and to, capped at max. Follows antonym
	 * links for the first step, and then synonym links from then on.
	 * Returns 0 if no path was found within max distance.
	 */
	public int getDistance(String from, String target, int max) {
		IWiktionaryEdition wkt = WiktionaryUtils.getWiktionaryEdition();
		// Get one level of antonyms from the from word
		Set<String> antonyms = WiktionaryUtils.getRelationsForWord(from, wkt, RelationType.ANTONYM);
				
		// Then go and find the smallest distance according to synonyms.
		// Max for the synonym distance should be one less that this because
		// you've effectively already gone one level deep.
		int leastDistance = -1;
		for (String antFrom : antonyms) {
			int dist = Distance.TYPES.get("wiktionary_synonym_distance").getDistance(antFrom, target, max);
			if ((dist < leastDistance && dist >= 0) || (leastDistance == -1)) {
				leastDistance = dist;
			} 
		}
		return leastDistance;
	}

}
