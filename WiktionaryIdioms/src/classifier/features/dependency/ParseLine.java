/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.dependency;

public class ParseLine {
	int tokenNum;
	String token;
	String coarsePOS;
	String POS;
	int head;
	String depRel;
	
	ParseLine dependent;
	
	public ParseLine(int tokenNum, String token, String coarsePOS, 
			String POS, int head, String depRel) {
		this.tokenNum = tokenNum;
		this.token = token;
		this.coarsePOS = coarsePOS;
		this.POS = POS;
		this.head = head;
		this.depRel = depRel;
	}
	
	public String getToken() {
		return token;
	}
	
	public void setDependent(ParseLine other) {
		dependent = other;
	}
	
	public String toString() {
		return tokenNum + " " + token + " " + coarsePOS + " " + POS + " " + head + " " + depRel;
	}
	
	public boolean equals(Object o) {
		if (o instanceof ParseLine) {
			ParseLine pl = (ParseLine) o;
			return this.tokenNum ==  pl.tokenNum && this.token.equals(pl.tokenNum);
		}
		return false;
	}
	
	public int hashCode() {
		return token.hashCode();
	}
}
