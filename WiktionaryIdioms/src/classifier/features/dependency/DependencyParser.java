/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * This class had important input from Jesse Dodge
 */

package classifier.features.dependency;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.parser.lexparser.NoSuchParseException;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.process.DocumentPreprocessor;
import edu.stanford.nlp.trees.EnglishGrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.SemanticHeadFinder;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.Filters;

public class DependencyParser {
	StanfordCoreNLP pipeline;
	GrammaticalStructureFactory gsf;
	
	private static DependencyParser instance = null;
	
	public static DependencyParser getInstance() {
		if (instance == null) {
			instance = new DependencyParser();
		}
		return instance;
	}
	
	private DependencyParser (){
		Properties props = new Properties();
		props.put("annotators", "tokenize,ssplit,parse");

		pipeline = new StanfordCoreNLP(props);
		
	    PennTreebankLanguagePack tlp = new PennTreebankLanguagePack();
	    gsf = tlp.grammaticalStructureFactory(Filters.<String>acceptFilter(), new SemanticHeadFinder(false));
	}

	public String getParse(String l) {
		// to overwrite system.out, so i can store what's printed to it by the parser
		PrintStream out = System.out;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream ps = new PrintStream(baos);
		System.setOut(ps);


		Annotation a = new Annotation(l);

		try {
			pipeline.annotate(a);
		} catch (NoSuchParseException e) {
			System.err.println("Unparsable: " + l);
			return "";
		}
		List<CoreMap> sentences = a
				.get(CoreAnnotations.SentencesAnnotation.class);
		if (sentences != null && sentences.size() > 0) {
			CoreMap sentence = sentences.get(0);
			Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
		    GrammaticalStructure gs = gsf.newGrammaticalStructure(tree);
		    
		    EnglishGrammaticalStructure.printDependencies(gs, gs.typedDependencies(false), tree, true, false);
		    System.out.flush();
		    System.setOut(out);
		}
		
		return baos.toString();
	}
	
	public Map<Integer, ParseLine> getParseMap(String parse) {
		String[] lines = parse.split("\n");
		Map<Integer, ParseLine> lineMap = new HashMap<Integer, ParseLine>();
		for (String line : lines) {
			String[] pieces = line.split("\t");
			int tokenNum = Integer.parseInt(pieces[0]);
			String token = pieces[1];
			String coarsePOS = pieces[3];
			String POS = pieces[4];
			int head = Integer.parseInt(pieces[6]);
			String depRel = pieces[7];
			ParseLine pl = new ParseLine(tokenNum, token, coarsePOS, POS, head, depRel);
			lineMap.put(tokenNum, pl);
		}
		
		// hook up the dependencies
		for (int num : lineMap.keySet()) {
			ParseLine pl = lineMap.get(num);
			if (lineMap.containsKey(pl.head)) {
				pl.setDependent(lineMap.get(pl.head));
			}
		}
				
		return lineMap;
	}
	
	public Pair<Integer, Integer> getBoundsForPhrase(String[] phrase, Map<Integer, ParseLine> lineMap) {
		// Find the bounds of the phrase
		int start = -1;
		int end = -1;
		int i = phrase.length;
		while(lineMap.containsKey(i) && (start == -1 && end == -1)) {
			boolean found = true;
			for (int j = phrase.length - 1; j >= 0; j--) {
				int offset = (phrase.length - 1) - j;  // j = 3, offset = 0, j = 2, offset = 1
				if (!phrase[j].equalsIgnoreCase(lineMap.get(i - offset).token)) {
					if (!((lineMap.get(i - offset).coarsePOS.startsWith("NN") && 
							lineMap.get(i - offset).coarsePOS.endsWith("S")) &&
							lineMap.get(i - offset).token.toLowerCase().startsWith(phrase[j].toLowerCase()))) {
						// move on
						found = false;
				
					}
				}
			}
			i++;
			if (found) {
				start = i - phrase.length;
				end = i - 1;
			}
		}
		return new Pair<Integer, Integer>(start, end);
	}
	
	public ParseLine getFirstGoverningVerb(String[] phrase, String parse) {
		Map<Integer, ParseLine> lineMap = getParseMap(parse);
		
		ParseLine first = getParseLineThatGovernsHead(phrase, parse);
		
		while (first != null) {
			if (first.coarsePOS.startsWith("VB")) {
				return first;
			}
			first = lineMap.get(first.head);
		}
		return first;
	}
	
	public Set<ParseLine> getFirstDependentVerbs(String[] phrase, String parse) {
		
		List<ParseLine> firsts = getParseLinesDependentOnPhrase(phrase, parse);
		
		Set<ParseLine> verbs = new HashSet<ParseLine>();
		while (firsts.size() > 0) {
			List<ParseLine> temp = new ArrayList<ParseLine>();
			for (ParseLine pl : firsts) {
				if (pl.coarsePOS.startsWith("VB")) {
					verbs.add(pl);
				}
				if (pl.dependent != null) {
					temp.add(pl.dependent);
				}
			}
			firsts = temp;
		}
		return verbs;
	}
	
	public List<ParseLine> getParseLinesDependentOnPhrase(String[] phrase, String parse) {
		Map<Integer, ParseLine> lineMap = getParseMap(parse);
		
		Pair<Integer, Integer> bounds = getBoundsForPhrase(phrase, lineMap);
		
		// start is now index of the first word in the phrase 
		// and end is the index of the last word in the phrase
		List<ParseLine> dependents = new ArrayList<ParseLine>();
		for (int key : lineMap.keySet()) {
			ParseLine pl = lineMap.get(key);
			if ((pl.head >= bounds.getFirst() && pl.head <= bounds.getSecond()) && 
					!(pl.tokenNum >= bounds.getFirst() && pl.tokenNum <= bounds.getSecond())) {
				dependents.add(pl);
			}
		}
		return dependents;
	}
	
	public ParseLine getHeadOfBoundedPhrase(Pair<Integer, Integer> bounds, Map<Integer, ParseLine> lineMap) {
		if (bounds.getFirst() < 0) {
			return null;
		}
		ParseLine pl = lineMap.get(bounds.getFirst());
		if (pl.dependent == null) {
			return pl;
		}
		while (pl.head <= bounds.getSecond() && pl.head >= bounds.getFirst()) {
			pl = pl.dependent;
		}
		return pl;
	}
	
	public ParseLine getParseLineThatGovernsHead(String[] phrase, String parse) {
		
		Map<Integer, ParseLine> lineMap = getParseMap(parse);
		
		Pair<Integer, Integer> bounds = getBoundsForPhrase(phrase, lineMap);
		
		if (bounds.getFirst() < 0){
			System.out.println(Arrays.toString(phrase));
			System.out.println(parse);
			return null;
		}
		
		ParseLine head = getHeadOfBoundedPhrase(bounds, lineMap);

		return head.dependent;
	}
	
	public String[] getSplitSentence(String sentence) {
		Reader reader = new StringReader(sentence);
		DocumentPreprocessor dp = new DocumentPreprocessor(reader);

		Iterator<List<HasWord>> it = dp.iterator();
		List<HasWord> split = it.next();
		String[] splitSent = new String[split.size()];
		for (int i = 0; i < split.size(); i++) {
			splitSent[i] = split.get(i).toString();
		}
		
		return splitSent;
	}
	
}
