/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.dependency;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class DependencyTest {
	
	public static void main(String[] args) {
		DependencyParser dp = DependencyParser.getInstance();
		String sentence = "They rode the trade winds going west.";
		//int tokenNum = sentence.indexOf("At gunpoint");
		String parse = dp.getParse(sentence);
		
		String[] phrase = dp.getSplitSentence("trade winds");
		
		System.out.println(Arrays.toString(phrase));
		System.out.println(parse);

		ParseLine govern = dp.getParseLineThatGovernsHead(phrase, parse);
		System.out.println("Govern: " + govern);
		ParseLine verb = dp.getFirstGoverningVerb(phrase, parse);
		System.out.println("verb: " + verb);
		
		List<ParseLine> pls = dp.getParseLinesDependentOnPhrase(phrase, parse);
		
		for (ParseLine pl : pls) {
			System.out.println(pl);
		}
		
		Set<ParseLine> deps = dp.getFirstDependentVerbs(phrase, parse);
		
		for (ParseLine pl : deps) {
			System.out.println("dep:" + pl);
		}
		
	}

}
