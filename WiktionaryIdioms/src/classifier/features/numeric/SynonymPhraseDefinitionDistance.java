/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import classifier.utilities.StemUtils;
import classifier.utilities.WiktionaryUtils;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

/*
 * NOT CURRENTLY IN USE
 */

public class SynonymPhraseDefinitionDistance extends Feature {
	public static final String DB_NAME = "synonym_phrase_definition_distance";
	public static final int MAX_DIST = 7;
	public static final double DIVIDE_CONSTANT = MAX_DIST + 1;

	public SynonymPhraseDefinitionDistance(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
		
		// Get the stemmed, cleaned gloss
		String cleanGloss = StemUtils.cleanSentence(definition);
		
		// now, for each word in the title, calculate its distance between the non stop words
		// in the clean gloss
		String[] titleWords = title.split(" ");
		String[] glossWords = cleanGloss.split(" ");
		
		int totalDistance = 0;
		int distanceAdditions = 0;
		for (String tW : titleWords) {
			for (String gW : glossWords) {
				if (!stops.isStopWord(gW)) {
					distanceAdditions++;
					int dist = WiktionaryUtils.getShortestSynonymPathDistance(tW, gW, wkt, MAX_DIST);
					if (dist >= 0) {
						totalDistance += dist;
					} else {
						// it wasn't found
						totalDistance += (MAX_DIST + 1); 
					}
				}
			}
		}
		double total = (totalDistance * 1.0);
		if (distanceAdditions > 0) {
			total = total / distanceAdditions;
		}
		
		//return total / DIVIDE_CONSTANT;
		return -1;
	}
}
