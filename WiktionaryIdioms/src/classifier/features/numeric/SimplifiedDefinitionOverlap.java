/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import org.apache.commons.lang3.StringUtils;

import classifier.utilities.StemUtils;
import classifier.utilities.WiktionaryUtils;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class SimplifiedDefinitionOverlap extends Feature {
	
	public SimplifiedDefinitionOverlap(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}
	
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
				
		// Get the stemmed, cleaned gloss
		String stemmedGloss = StemUtils.stemAndCleanSentence(definition, stops);
		
		// For each word in the title, go and get the definitions from the base pages that correspond.
		String[] titleWords = title.split(" ");
		Map<String, List<String>> titleGlosses = new HashMap<String, List<String>>();
		for (String word : titleWords) {
			if (!stops.isStopWord(word)) {
				IWiktionaryPage wordPage = WiktionaryUtils.getBasePageForWord(word, wkt);
				if (wordPage != null) {
					titleGlosses.put(word, StemUtils.getStemmedGlosses(wordPage, stops));
				}
			}
		}
		
		// Now calculate the overlap between the glosses and the title
		// glosses (the glosses from the sub-parts of the title)
		double overlap = 0;
		for (String word : titleGlosses.keySet()) {
			List<String> defs = titleGlosses.get(word);
			for (String def : defs) {

				// get the words
				String[] words = def.split(" ");
				for (String w : words) {
					// See how many times w occurs in the gloss.
					overlap += StringUtils.countMatches(stemmedGloss, w);
				}
			}
		}
		
		// normalize by dividing by the number of glosses gotten from the words in the title.
		if (overlap == 0) {
			return 0;
		}
		
		return overlap / titleWords.length;
	}
	
	public static String removeTitleWords(String gloss, String title) {
		String[] tWords = title.split(" ");
		String[] glossWords = gloss.split(" ");
		for (int i = 0; i < glossWords.length; i++) {
			for (int j = 0; j < tWords.length; j++) {
				if (glossWords[i].equals(tWords[j])) {
					glossWords[i] = "";
				}
			}
		}
		return StringUtils.join(glossWords, " ");
	}
}
