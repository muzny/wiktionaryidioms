/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import mysql.utilities.MySQLConnection;
import classifier.distances.Distance;
import classifier.utilities.StemUtils;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class AverageSynonymWordNet extends Feature {
	public static final int MAX_DIST = 5;
	public static final String LOOKUP_TABLE = "wordnet_synonym_distance";
	public static final String LOOKUP_DB = "wiktionary_data_sets";
	
	public AverageSynonymWordNet(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
		
		String gloss = definition;
		
		Distance d = Distance.TYPES.get(LOOKUP_TABLE);
				
		// Get the stemmed, cleaned gloss
		String cleanGloss = StemUtils.cleanSentence(gloss);
		
		// now, for each word in the title, calculate its distance between the non stop words
		// in the clean gloss
		String[] titleWords = StemUtils.cleanSentence(title).split(" ");
		String[] glossWords = cleanGloss.split(" ");
		
		// Connect to the database
		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", LOOKUP_DB, "root", "");
		
		double total = 0;
		double count = 0;
		for (String tW : titleWords) {
			if (tW.trim().length() == 0) {
				continue;
			}
			for (String gW : glossWords) {
				if (!stops.isStopWord(gW) && gW.trim().length() > 0) {
					// first, see if it is in the table
					Double result = moby.selectFromDistanceTable(LOOKUP_TABLE, tW, gW);
					double dist = MAX_DIST + 2;
					if (result != null) {
						dist = result;
					} else {
						dist = d.getDistance(tW, gW, MAX_DIST) + 1;
						dist = (1.0 * dist)/ (MAX_DIST + 1);
						// insert the new entry into the database
					}
					total += dist;
					count++;
				}
			}
		}
		if (count > 0) {
			return total / count;
		}
		return 0;
	}
}
