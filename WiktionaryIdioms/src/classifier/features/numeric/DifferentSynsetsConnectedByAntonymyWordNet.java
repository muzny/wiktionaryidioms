/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import java.util.Set;

import classifier.utilities.StemUtils;
import classifier.utilities.WordNetUtils;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import edu.smu.tspell.wordnet.Synset;
import edu.smu.tspell.wordnet.WordNetDatabase;

public class DifferentSynsetsConnectedByAntonymyWordNet extends Feature {
	
	public DifferentSynsetsConnectedByAntonymyWordNet(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		WordNetUtils.setDatabaseDirectory();
		WordNetDatabase db = WordNetDatabase.getFileInstance();
		
		String title = phrase;
		
		String gloss = definition;
				
		// For each word in the cleaned title
		String[] tWords = StemUtils.cleanSentence(title).split(" ");
		
		// Get the gloss words too
		String[] gWords = StemUtils.cleanSentence(gloss).split(" ");
		for (String tW : tWords) {
			if (tW.trim().length() == 0 || stops.isStopWord(tW)) {
				continue;
			}
			Synset[] titleSyns = db.getSynsets(tW.trim());
			
			// get the set of antonyms
			Set<String> titleAnts = WordNetUtils.getAllAntonyms(titleSyns);
			
			for (String gW : gWords) {
				if (gW.trim().length() == 0 || stops.isStopWord(gW)) {
					continue;
				}
				Synset[] glossSyns = db.getSynsets(gW.trim());
				
				// See if these words are associated with two synsets that are
				// connected through an antonymy relation
				// get the set of antonyms
				Set<String> glossAnts = WordNetUtils.getAllAntonyms(glossSyns);
				
				// if there is something in common between the two sets of 
				// antonyms, then this relation exists
				for (String tAnt : titleAnts) {
					if (glossAnts.contains(tAnt)) {
						return 1;
					}
				}
			}
		}
		return 0;
	}
}
