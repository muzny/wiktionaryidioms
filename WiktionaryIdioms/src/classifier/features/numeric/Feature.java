/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * The interface to all features that can be currently calculated for a given classifier data.
 * Currently, it requires an IWiktionaryEdition as well, from the JWKTL 
 * (http://code.google.com/p/jwktl/) project.
 * 
 * To add another feature, simply put it into the FEATURE_INDEXES dictionary. This will make it
 * accessible everywhere that features are calculated.
 * 
 * To add a feature to a group, or add a group, simply add them to the appropriate arrays.
 */

package classifier.features.numeric;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import classifier.model.ClassifierData;
import classifier.model.Sense;
import classifier.utilities.StopWords;



import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;

public abstract class Feature {
	public static final TreeMap<Integer, Feature> FEATURE_INDEXES = new TreeMap<Integer, Feature>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
		// Features 1 - 14: developed with with the classification task of <phrase, definitions> in mind.
		//
		
		// Lexical Features
	    put(1, new SynonymOverlap(StopWords.FULL, "synonym_overlap"));  
	    put(2, new AntonymOverlap(StopWords.FULL, "antonym_overlap"));  
	    put(3, new SimplifiedDefinitionOverlap(StopWords.FULL, "simple_definition_overlap"));  // Based off the Lesk algorithm
	    put(4, new AverageCapitals(StopWords.FULL,"avg_capitals"));  // doesn't use stop words
	    
	    // Graph-based features
	    put(5, new ClosestSynonymWiktionary(StopWords.FULL, "closest_synonym_phrase_definition_distance"));
	    put(6, new ClosestSynonymWordNet(StopWords.FULL, "closest_synonym_phrase_definition_distance_wordnet"));
	    put(7, new ClosestAntonymWiktionary(StopWords.FULL, "closest_antonym_phrase_definition_distance"));
	    put(8, new ClosestAntonymWordNet(StopWords.FULL, "closest_antonym_phrase_definition_distance_wordnet"));
	    
	    put(9, new AverageSynonymWordNet(StopWords.FULL, "avg_synonym_distance_wordnet"));
	    put(10, new DifferentSynsetsConnectedByAntonymyWordNet(StopWords.FULL, "syns_connected_ant"));

	    put(11, new ClosestHypernymWordNet(StopWords.FULL, "closest_hypernym_wordnet"));
	    put(12, new AverageHypernymWordNet(StopWords.FULL, "avg_hypernym_wordnet"));
	    
	    put(13, new ClosestHyponymWordNet(StopWords.FULL, "closest_hyponym_wordnet"));
	    put(14, new AverageHyponymWordNet(StopWords.FULL, "avg_hyponym_wordnet"));
	}};
	
	public static final int[] LEXICAL = {1, 2, 4};  // The numbers that correspond to lexical features
	public static final int[] GRAPH = {6, 8, 9, 10, 14}; 
	
	public static final int[][] GROUPS = {LEXICAL, GRAPH};
	
	protected StopWords stops;
	private String dbName;
	
	
	public Feature(String stopwordsKind, String databaseName) {
		this.stops = StopWords.getInstance(stopwordsKind);
	  	this.dbName = databaseName;
	}

	public String getDBName() {
		return dbName;
	}
	
	public List<Integer> getClassificationFeatures(){
		List<Integer> feats = new ArrayList<Integer>();
		for (int i = 1; i <= 14; i++) {
			feats.add(i);
		}
		return feats;
	}
	
	public List<Integer> getDetectionFeatures(){
		List<Integer> feats = new ArrayList<Integer>();
		for (int i = 15; i <= 14; i++) {
			feats.add(i);
		}
		return feats;
	}
	
	public static double calculate(ClassifierData cd, int key, IWiktionaryEdition wkt) {
		String phrase = cd.getTitle();
		if (cd instanceof Sense) {
			String definition = ((Sense) cd).getGloss();
			return FEATURE_INDEXES.get(key).calculate(phrase, definition, cd.getParse(), wkt);
		}
		return -1;
	}
	
	public static String cleanExampleText(String phrase, String text) {
		int ind = text.indexOf(phrase) ;

		if (ind >= 0) {
			return text.substring(0, ind) + text.substring(ind + phrase.length());
		}
		
		return text;
	
	}
		
	public abstract double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt);	
}
