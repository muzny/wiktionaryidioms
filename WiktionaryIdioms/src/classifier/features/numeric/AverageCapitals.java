/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class AverageCapitals extends Feature {

	public AverageCapitals(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
		
		// for each non-stop word in the title, count how many capitals it has
		int capitals = 0;
		int words = 0;
		String[] tWords = title.split(" ");
		for (String tw : tWords) {
			for (int i = 0; i < tw.length(); i++) {
				if (Character.isUpperCase(tw.charAt(i))) {
					capitals++;
				}
			}
			if (tw.length() > 0) {
				words++;
			}
		}
		
		if (words > 0) {
			return (1.0 * capitals) / words;
		}
		return 0;
	}

}
