/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mysql.utilities.MySQLConnection;

import org.apache.commons.lang3.StringUtils;

import classifier.distances.Distance;
import classifier.utilities.StemUtils;
import classifier.utilities.WiktionaryUtils;

import de.tudarmstadt.ukp.wiktionary.api.IWikiString;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryPage;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class LeskSynonymDistance extends Feature {
	public static final int MAX_DIST = 5;
	public static final String LOOKUP_TABLE = "wordnet_synonym_distance";
	public static final String LOOKUP_DB = "wiktionary_data_sets";
	
	public LeskSynonymDistance(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}
	
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
				
		// Get the cleaned gloss
		String gloss = StemUtils.cleanSentence(definition);
		
		Distance d = Distance.TYPES.get(LOOKUP_TABLE);
		// Connect to the database
		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", LOOKUP_DB, "root", "");
		
		// For each word in the title, go and get the definitions from the base pages that correspond.
		String[] titleWords = title.split(" ");
		Map<String, List<String>> titleGlosses = new HashMap<String, List<String>>();
		for (String tW : titleWords) {
			IWiktionaryPage page = WiktionaryUtils.getBasePageForWord(tW, wkt);
			if (page == null) {
				continue;
			}
			List<IWiktionaryEntry> entries = page.getEntries();

			// For each entry, go and get the definitions.
			List<String> glosses = new ArrayList<String>();
			for (IWiktionaryEntry entry : entries) {
				List<IWikiString> eGlosses = entry.getGlosses();

				// For each gloss, get the plain text and stem it.
				for (IWikiString eG: eGlosses) {
					String text = eG.getPlainText();
					glosses.add(StemUtils.cleanSentence(text));
				}
			}
			titleGlosses.put(tW, glosses);
		}
		
		// Now calculate the overlap between the glosses and the title
		// glosses (the glosses from the sub-parts of the title)
		int total = 0;
		int count = 0;
		for (String tW : titleGlosses.keySet()) {
			List<String> defs = titleGlosses.get(tW);
			for (String def : defs) {

				// get the words
				String[] words = def.split(" ");
				for (String gW : words) {
					// See how many times w occurs in the gloss.
					Double result = moby.selectFromDistanceTable(LOOKUP_TABLE, tW, gW);
					double dist = MAX_DIST + 2;
					if (result != null) {
						dist = result;
					} else {
						dist = d.getDistance(tW, gW, MAX_DIST) + 1;
						dist = (1.0 * dist)/ (MAX_DIST + 1);
						// insert the new entry into the database
					}
					total += dist;
					count++;
				}
			}
		}
		
		if (count > 0) {
			return total / count;
		}
		return 0;
	}

}
