/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import mysql.utilities.MySQLConnection;
import classifier.distances.Distance;
import classifier.utilities.StemUtils;
import classifier.utilities.StopWords;

public class SenseFeatureUtils {
	
	public static double getClosestWithDatabaseLookup(String title, String gloss, 
			Distance d, int maxDist, String lookupDb, String lookupTable, StopWords stops) {		
		// Get the stemmed, cleaned gloss
		String cleanGloss = StemUtils.cleanSentence(gloss);
		
		// now, for each word in the title, calculate its distance between the non stop words
		// in the clean gloss
		String[] titleWords = StemUtils.cleanSentence(title).split(" ");
		String[] glossWords = cleanGloss.split(" ");

		double leastDistance = maxDist + 2;
		
		// Connect to the database
		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", lookupDb, "root", "");
		
		for (String tW : titleWords) {
			if (tW.trim().length() == 0) {
				continue;
			}
			for (String gW : glossWords) {
				if (!stops.isStopWord(gW) && gW.trim().length() > 0) {
					// first, see if it is in the table
					Double result = moby.selectFromDistanceTable(lookupTable, tW, gW);
					double dist = maxDist + 2;
					if (result != null) {
						dist = result;
					} else {
						dist = d.getDistance(tW, gW, maxDist) + 1;
						dist = (1.0 * dist)/ (maxDist + 1);
						// insert the new entry into the database
					}
					if (dist < leastDistance && dist > 0) {
						leastDistance = dist;
					}
				}
			}
		}
		if (leastDistance == maxDist + 2) {
			return 0;
		}
		return leastDistance;
	}
	
	public static double getAverageWithDatabaseLookup(String title, String gloss, 
			Distance d, int maxDist, String lookupDb, String lookupTable, StopWords stops) {
		// Get the stemmed, cleaned gloss
		String cleanGloss = StemUtils.cleanSentence(gloss);
		
		// now, for each word in the title, calculate its distance between the non stop words
		// in the clean gloss
		String[] titleWords = StemUtils.cleanSentence(title).split(" ");
		String[] glossWords = cleanGloss.split(" ");
		
		// Connect to the database
		mysql.utilities.MySQLConnection moby = MySQLConnection.getInstance("localhost", lookupDb, "root", "");
		
		double total = 0;
		double count = 0;
		for (String tW : titleWords) {
			if (tW.trim().length() == 0) {
				continue;
			}
			for (String gW : glossWords) {
				if (!stops.isStopWord(gW) && gW.trim().length() > 0) {
					// first, see if it is in the table
					Double result = moby.selectFromDistanceTable(lookupTable, tW, gW);
					double dist = maxDist + 2;
					if (result != null) {
						dist = result;
					} else {
						dist = d.getDistance(tW, gW, maxDist) + 1;
						dist = (1.0 * dist)/ (maxDist + 1);
						// insert the new entry into the database
					}
					total += dist;
					count++;
				}
			}
		}
		if (count > 0) {
			return total / count;
		}
		return 0;
	}

}
