/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import java.util.HashSet;
import java.util.Set;

import classifier.utilities.StemUtils;
import classifier.utilities.WiktionaryUtils;


import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class SynonymOverlap extends Feature {
	
	public SynonymOverlap(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
		
		// Get the synonyms for each word in the title.
		// To do this, get all the entries for each word, and then get their synonyms.
		Set<String> titleSynonyms = WiktionaryUtils.getSynonymsForWords(title, wkt, 1);
				
		// Go and get the stemmed definition.
		String stemmedGloss = StemUtils.stemAndCleanSentence(definition, stops);
		
		// Stem the words in the title synonyms set.
		Set<String> stemmedTitleSynonyms = new HashSet<String>();
		for (String word : titleSynonyms) {
			// Remove all "Wikisaurus:" from the title synonyms
			if (word.contains(":")) {
				word = word.substring(word.indexOf(":") + 1);
			}
			if (!stops.isStopWord(word)) {
				stemmedTitleSynonyms.add(StemUtils.stemWord(word));
			}
		}
		
		// also add the words from the stemmed title.
		String[] tWords = StemUtils.stemAndCleanSentence(title, stops).split(" ");
		for (String word : tWords) {
			stemmedTitleSynonyms.add(word);
		}
		
		
		double score = 0;
		// Now calculate the score.
		// For each definition, every time a word in the synonyms set appears
		// in that definition, add 1 to the score.
		for (String syn : stemmedTitleSynonyms) {
			if (stemmedGloss.contains(syn)) {
				score += 1;
			}
		}
		
		// Normalize the score
		score = (1.0 * score) / stemmedTitleSynonyms.size();
		
		return score;
	}

}
