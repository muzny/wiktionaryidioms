/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.numeric;

import classifier.distances.Distance;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class ClosestHyponymWordNet extends Feature {
	public static final int MAX_DIST = 5;
	public static final String LOOKUP_TABLE = "wordnet_hyponym_distance";
	public static final String LOOKUP_DB = "wiktionary_data_sets";
	
	public ClosestHyponymWordNet(String stopwordsKind, String databaseName) {
		super(stopwordsKind, databaseName);
	}

	@Override
	public double calculate(String phrase, String definition, String parse, IWiktionaryEdition wkt) {
		String title = phrase;
		
		String gloss = definition;
		
		Distance d = Distance.TYPES.get(LOOKUP_TABLE);
				
		return SenseFeatureUtils.getClosestWithDatabaseLookup(
				title, gloss, d, MAX_DIST, LOOKUP_DB, LOOKUP_TABLE, stops);
	}
}
