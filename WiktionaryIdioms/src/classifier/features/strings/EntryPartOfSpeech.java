/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.strings;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEntry;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;
import de.tudarmstadt.ukp.wiktionary.api.PartOfSpeech;

public class EntryPartOfSpeech implements StringFeature {
	public static final String DB_NAME = "pos";

	@Override
	public String getDBName() {
		return DB_NAME;
	}

	@Override
	public String calculate(String senseKey, String text, IWiktionaryEdition wkt) {
		IWiktionarySense wikiSense = wkt.getSenseForKey(senseKey);
		
		IWiktionaryEntry entry = wikiSense.getEntry();
		PartOfSpeech pos = entry.getPartOfSpeech();
		return pos.toString();
	}
}
