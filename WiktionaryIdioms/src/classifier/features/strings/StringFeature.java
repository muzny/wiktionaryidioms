/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

/**
 * These are the string features to make inserting them into and 
 * updating the databases easier.
 */

package classifier.features.strings;

import java.util.TreeMap;

import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;

public interface StringFeature {
	public static final TreeMap<Integer, StringFeature> FEATURE_INDEXES = 
			new TreeMap<Integer , StringFeature>() {/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

	{
	    put(0, new Gloss());
	    put(1, new UncleanedGloss());
	    put(2, new Specific());
	    put(3, new EntryPartOfSpeech());
	    put(4, new Parse());
	}};

	public String getDBName();
	
	public String calculate(String senseKey, String text, IWiktionaryEdition wkt);	
}
