/*******************************************************************************
 * WiktionaryIdioms. Copyright (C) 2013 Grace Muzny
 * <p>
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or any later version.
 * <p>
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * <p>
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 51
 * Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 ******************************************************************************/

package classifier.features.strings;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

import classifier.config.GeneralConfigs;


import de.tudarmstadt.ukp.wiktionary.api.IWiktionaryEdition;
import de.tudarmstadt.ukp.wiktionary.api.IWiktionarySense;

public class Specific implements StringFeature {
	public static final String DB_NAME = "specific_definition";
	
	private static Set<String> specs;

	@Override
	public String getDBName() {
		return DB_NAME;
	}
	
	private static void initialize() {
		if (specs == null) {
			specs = new HashSet<String>();
			
			String path = GeneralConfigs.getString(GeneralConfigs.CLASSIFIER_CONFIGS, "default.specificsPath");
			
			try {
				Scanner scan = new Scanner(new File(path));
				while (scan.hasNextLine()) {
					String line = scan.nextLine();
					specs.add(line.trim());
				}
			} catch (FileNotFoundException e) {
				System.err.println("Error with file: " + path);
			}
		}
	}

	@Override
	public String calculate(String senseKey, String text, IWiktionaryEdition wkt) {
		initialize();
		IWiktionarySense wikiSense = wkt.getSenseForKey(senseKey);
		
		String uncleaned = wikiSense.getGloss().getTextIncludingWikiMarkup();
				
		if (uncleaned.startsWith("{") && uncleaned.contains("}")) {
			String context = uncleaned.substring(0, uncleaned.indexOf("}"));
			for (String spec : specs) {
				if (context.contains(spec)) {
					return spec;
				}
			}
		}

		return "";
	}
}
